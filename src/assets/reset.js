(function () {
  if (!('path' in Event.prototype)) {
    /** Polyfill for missing "path" property in IE/Edge Mouse/Pointer Events */
    Object.defineProperty(Event.prototype, 'path', {
      get: function () {
        var path = [];
        var currentElem = this.target;
        while (currentElem) {
          path.push(currentElem);
          currentElem = currentElem.parentElement;
        }
        if (path.indexOf(window) === -1 && path.indexOf(document) === -1) {
          path.push(document);
        }
        if (path.indexOf(window) === -1) {
          path.push(window);
        }
        return path;
      }
    });
  }
})();
