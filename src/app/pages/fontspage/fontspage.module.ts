import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FontspagePageRoutingModule } from './fontspage-routing.module';

import { FontspagePage } from './fontspage.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, FontspagePageRoutingModule],
  declarations: [FontspagePage],
})
export class FontspagePageModule {}
