import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FontspagePage } from './fontspage.page';

describe('FontspagePage', () => {
  let component: FontspagePage;
  let fixture: ComponentFixture<FontspagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FontspagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FontspagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
