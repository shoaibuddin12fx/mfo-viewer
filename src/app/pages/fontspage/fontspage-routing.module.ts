import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FontspagePage } from './fontspage.page';

const routes: Routes = [
  {
    path: '',
    component: FontspagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FontspagePageRoutingModule {}
