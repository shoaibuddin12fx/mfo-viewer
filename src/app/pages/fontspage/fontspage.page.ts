import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { FontService } from '@services/font.service';
import { Utils } from 'src/app/dataformat/util';

@Component({
  selector: 'app-fontspage',
  templateUrl: './fontspage.page.html',
  styleUrls: ['./fontspage.page.scss'],
})
export class FontspagePage implements AfterViewInit {
  /**
   * Canvas reference object
   *
   * @type {ElementRef}
   * @memberof FontspagePage
   */
  @ViewChild('canvas', { read: ElementRef, static: false })
  canvasRef: ElementRef;

  /**
   * canvas reference
   *
   * @type {HTMLCanvasElement}
   * @memberof FontspagePage
   */
  canvas: HTMLCanvasElement;

  /**
   * current font
   *
   * @type {string}
   * @memberof FontspagePage
   */
  public FontFamily: string;

  /**
   * Creates an instance of FontspagePage.
   * @param {FontService} fontService
   * @param {ElementRef} elementRef
   * @memberof FontspagePage
   */
  constructor(
    private fontService: FontService,
    private elementRef: ElementRef
  ) {}

  /**
   * after init lifecycle hook
   *
   * @memberof FontspagePage
   */
  ngAfterViewInit() {
    this.canvas = this.canvasRef.nativeElement as HTMLCanvasElement;
    this.injectFonts();
    this.getPNGs();
  }

  /**
   * inject fonts into stylesheet
   *
   * @private
   * @memberof FontspagePage
   */
  private injectFonts() {
    let fontString = ``;

    this.fontService.FontMapping.forEach((font) => {
      if (!font) {
        return;
      }
      fontString += `
        @font-face {
          font-family: '${font.Family}';
          font-style: normal;
          font-weight: normal;
          src: url(/assets/fonts/${font.File}.woff2) format('woff2');
        }`;
    });

    const node = document.createElement('style');
    node.innerHTML = fontString;
    this.elementRef.nativeElement.appendChild(node);
  }

  /**
   * generate PNGs for all fonts
   *
   * @private
   * @memberof FontspagePage
   */
  private async getPNGs() {
    const context = this.canvas.getContext('2d');
    const link = document.createElement('a');
    let font = '';

    //this.fontService.FontMapping.forEach(async (font) =>
    for (let i = 0; i < this.fontService.FontMapping.length; i++) {
      await new Promise<void>((r) => {
        window.setTimeout(() => {
          Utils.log('TimeOut');
          r();
        }, 1000);
      });
      font = this.fontService.FontMapping[i].Family;
      this.FontFamily = font;
      await new Promise<void>((r) => {
        window.setTimeout(() => {
          Utils.log('TimeOut');
          r();
        }, 500);
      });
      context.clearRect(0, 0, this.canvas.width, this.canvas.height);
      context.font = `36px ${font}`;
      context.fillText(font, 0, 30);
      context.stroke();
      this.canvas.width = context.measureText(font).width;
      context.font = `36px ${font}`;
      context.fillText(font, 0, 30);
      context.stroke();
      link.download = `${font}.png`;
      link.href = this.canvas.toDataURL();
      await new Promise<void>((r) => {
        window.setTimeout(() => {
          Utils.log('TimeOut');
          r();
        }, 1000);
      });
      link.click();
    }
  }
}
