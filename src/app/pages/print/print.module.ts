import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@components/shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { Pdf417BarcodeModule } from 'pdf417-barcode';
import { PrintPageRoutingModule } from './print-routing.module';
import { PrintPage } from './print.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    PrintPageRoutingModule,
    NgxQRCodeModule,
    Pdf417BarcodeModule,
  ],

  declarations: [PrintPage],
})
export class PrintPageModule {}
