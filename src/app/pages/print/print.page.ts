import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Log } from '../../decorators/log.decorator';

import { MfoViewerComponent } from '../../components/mfo-viewer/mfo-viewer.component';
import { ViewMode } from '../../dataformat/enums/viewmode.enum';
import { View } from '../../dataformat/enums/view.enum';
import { ImportService } from '@services/import.service';
import { FontService } from '@services/font.service';
import { Utils } from 'src/app/dataformat/util';
import { MFO } from 'src/app/dataformat/models/mfo.model';

@Component({
  selector: 'app-print',
  templateUrl: 'print.page.html',
  styleUrls: ['print.page.scss'],
})
export class PrintPage implements OnInit {
  @ViewChild(MfoViewerComponent, { read: ElementRef }) viewerRef: ElementRef;
  @ViewChild(MfoViewerComponent) viewerObj: MfoViewerComponent;

  @ViewChild('canvas', { read: ElementRef, static: false })
  canvas: ElementRef;

  /**
   * viewmode
   *
   * @memberof PrintPage
   */
  public Mode = ViewMode;

  /**
   * mfo object
   *
   * @type {MFO}
   * @memberof PrintPage
   */
  public mfo: MFO;

  public FontFamily = '';

  constructor(
    private importService: ImportService,
    private fontService: FontService,
    private elementRef: ElementRef
  ) {
    // this.test();
  }

  injectFonts() {
    let fontString = ``;

    this.fontService.FontMapping.forEach((font) => {
      if (!font) {
        return;
      }
      fontString += `
        @font-face {
          font-family: '${font.Family}';
          font-style: normal;
          font-weight: normal;
          src: url(/assets/fonts/${font.File}.woff2) format('woff2');
        }`;
    });

    const node = document.createElement('style');
    node.innerHTML = fontString;
    this.elementRef.nativeElement.appendChild(node);
  }

  @Log()
  async test() {
    this.injectFonts();
    // await this.delay(5000);
    // const context = this.canvas.nativeElement.getContext('2d');
    // let link = document.getElementById('link');

    // for (let i = 0; i < this.fontService.FontMapping.length; i++) {
    //   this.FontFamily = this.fontService.FontMapping[i].Family;
    //   await this.delay(1000);

    //   context.clearRect(
    //     0,
    //     0,
    //     this.canvas.nativeElement.width,
    //     this.canvas.nativeElement.height
    //   );
    //   context.font = `36px ${this.fontService.FontMapping[i].Family}`;
    //   context.fillText(this.fontService.FontMapping[i].Family, 0, 30);
    //   context.stroke();
    //   this.canvas.nativeElement.width = context.measureText(
    //     this.fontService.FontMapping[i].Family
    //   ).width;
    //   context.font = `36px ${this.fontService.FontMapping[i].Family}`;
    //   context.fillText(this.fontService.FontMapping[i].Family, 0, 30);
    //   context.stroke();
    //   await this.delay(2000);
    //   link.setAttribute(
    //     'download',
    //     this.fontService.FontMapping[i].Family + '.png'
    //   );
    //   link.setAttribute(
    //     'href',
    //     this.canvas.nativeElement
    //       .toDataURL('image/png')
    //       .replace('image/png', 'image/octet-stream')
    //   );
    //   // link.click();
    //   // await this.delay(1000);
    // }

    const response = await fetch('/assets/demo.MFO');
    Utils.log('printpage: test', response);

    this.mfo = await this.importService.from.file(
      response,
      response.url.split('/').pop()
    );

    Utils.log('MFO', this.mfo);

    this.viewerObj.setMfo(this.mfo);

    window.mfo = this.mfo;

    // setInterval(() => {
    //   this.mfo.Pages[0].Children[33].FontStyle.Bold = !this.mfo.Pages[0]
    //     .Children[33].FontStyle.Bold;
    // }, 1000);
  }

  @Log()
  ngOnInit() {
    //this.test();
  }

  delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
}
