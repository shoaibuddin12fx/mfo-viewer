import {
  Component,
  ComponentRef,
  ComponentFactoryResolver,
  ViewContainerRef,
} from '@angular/core';
import { MfoViewerComponent } from '../../components/mfo-viewer/mfo-viewer.component';
import { ImportService } from '@services/import.service';

// dayjs
import * as de from 'dayjs/locale/de';
import * as dayjs from 'dayjs';
import { Utils } from 'src/app/dataformat/util';

/**
 * main page
 *
 * @export
 * @class HomePage
 */
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  /**
   * javascript window reference
   *
   * @private
   * @type {Window}
   * @memberof HomePage
   */
  private windowReference: Window;

  /**
   * Creates an instance of HomePage.
   * @param {ComponentFactoryResolver} r
   * @param {ViewContainerRef} viewContainerRef
   * @param {ImportService} importService
   * @memberof HomePage
   */
  constructor(
    private r: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef,
    private importService: ImportService
  ) {
    // set dayjs global locale de
    dayjs.locale(de);
  }

  /**
   * initialises the preview for mfo document
   *
   * @memberof HomePage
   */
  public async preview() {
    // create the component dynamically
    const factory = this.r.resolveComponentFactory(MfoViewerComponent);
    const comp: ComponentRef<MfoViewerComponent> =
      this.viewContainerRef.createComponent(factory);

    // TODO: implement printview
    // comp.instance.isPrintView = true;
    const response = await fetch('/assets/demo.MFO');
    // const mfo = await this.importService.from.file(response);
    const mfo = window.mfo;
    Utils.log('mfo', mfo);
    const width =
      Math.max.apply(
        Math,
        window.mfo.Pages.map(function (o) {
          return o.Width;
        })
      ) + 32;
    comp.instance.setMfo(window.mfo);

    this.windowReference = window.open(
      '',
      '_blank',
      'toolbar=yes, menubar=yes, width=' + width + ', height=800'
    );
    this.windowReference.document.body.style.margin = '0';

    this.windowReference.document.body.appendChild(comp.location.nativeElement);
  }
}
