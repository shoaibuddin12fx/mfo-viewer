import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@components/shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { Pdf417BarcodeModule } from 'pdf417-barcode';
import { MfoEditorComponent } from '../../components/mfo-editor/mfo-editor.component';
import { HomePageRoutingModule } from './home-routing.module';
import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxQRCodeModule,
    Pdf417BarcodeModule,
    SharedModule,
    HomePageRoutingModule,
  ],
  exports: [MfoEditorComponent],
  declarations: [HomePage],
})
export class HomePageModule {}
