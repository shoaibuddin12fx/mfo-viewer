import { BaseCommand } from './base.command';

/**
 * click command
 *
 * @export
 * @class Click
 * @extends {BaseCommand}
 */
export class Click extends BaseCommand {
  /**
   * element which was clicked
   *
   * @memberof Click
   */
  public element;
}
