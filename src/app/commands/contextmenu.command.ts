import { Click } from './click.command';

/**
 * ContextMenuCommand
 *
 * @export
 * @class ContextMenu
 * @extends {Click}
 */
export class ContextMenu extends Click {
  /**
   * position of context menu to show
   *
   * @memberof ContextMenu
   */
  public position;
}
