/**
 * Base command model
 *
 * @export
 * @class BaseCommand
 */
export class BaseCommand {
  /**
   * name of command
   *
   * @type {string}
   * @memberof BaseCommand
   */
  public name: string;
}
