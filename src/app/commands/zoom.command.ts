import { BaseCommand } from './base.command';

/**
 * Zoom command
 *
 * @export
 * @class Zoom
 * @extends {BaseCommand}
 */
export class Zoom extends BaseCommand {
  /**
   * Zoomdirection: in/out
   *
   * @memberof Zoom
   */
  public direction;
}
