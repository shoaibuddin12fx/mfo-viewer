import { Versions } from './versions';

import * as pako from 'pako';
import { Colors } from './pascal/colors';
import { GuidService } from '@services/guid.service';
import { FontService } from '@services/font.service';

import {
  Container,
  CheckBox,
  Text,
  Shape,
  Edit,
  Barcode,
  Line,
  Image,
  MarkField,
} from './models';
import { Utils } from './util';
import { MFO } from './models/mfo.model';

/**
 * Import a Pascal/Delphi MFO or a v3 MFO
 *
 * @export
 * @class Import
 */
export class Import {
  /**
   * Creates an instance of Import.
   * @param {FontService} fontService
   * @memberof Import
   */
  constructor(private fontService: FontService) {}

  /**
   * Load up a file for parsing
   *
   * @param {Uint8Array} file
   * @returns {MFO}
   * @memberof Import
   */
  public async file(response, format: string): Promise<any> {
    let mfo = new MFO();

    if (format === 'MFW') {
      let mfojson = JSON.parse(
        pako.inflate(await response.arrayBuffer(), { to: 'string' })
      );

      Utils.logPrefixed('import', 'mfojsonRAW', mfojson);

      let ps = [];
      for (let i = 0; i < mfojson.Pages.length; i++) {
        ps[i] = Utils.objectFromMfoInterface(mfojson.Pages[i]);
      }
      mfo.Pages = ps;
      mfo.Fonts = mfojson.Fonts;

      return mfo;
    } else if (format === 'obj') {
      let container = new Container();
      const objjson = JSON.parse(
        pako.inflate(await response.arrayBuffer(), { to: 'string' })
      );
      container = Utils.objectFromMfoInterface(objjson.Container);
      return { Container: container, Fonts: objjson.Fonts };
    }
    let file = await response.text();

    mfo.Pages = [];

    // temporary hold raw values
    // RawPages gets deleted at end of this function
    mfo['RawPages'] = [];

    switch (this.determineFileVersion(file)) {
      case Versions.oldPascal:
        mfo['RawPages'].push(this.loadPage(file));
        await this.parsePageTree(mfo);
        break;
      case Versions.newPascal:
        file.split('^-^-^-^-^').forEach((page) => {
          if (page.length > 0) {
            mfo['RawPages'].push(this.loadPage(page));
          }
        });
        await this.parsePageTree(mfo);
        break;
      case Versions.v3:
        // slice first 7 bits
        // file = file.slice(7);
        // mfo = decode<MFO>(textEncoder.encode(file)) as MFO;
        throw 'Version v3 still untested. Only prepared since tickets did not cover it yet.';
      default:
        throw 'Version not specified. Please contact Support!';
    }

    // this info is no longer necessary
    delete mfo['RawPages'];

    Utils.logPrefixed('import', 'mfoFromOldFormat', mfo);
    return mfo;
  }

  /**
   * Convert a base64 pagestring and inflate the content.
   * Used by the older file formats.
   *
   * @static
   * @param {*} rawPage
   * @returns {Page}
   * @memberof Import
   */
  loadPage(rawPage): string {
    return String.fromCharCode.apply(null, pako.default.inflate(atob(rawPage)));
  }

  /**
   * Determine the File Version based on the content of the file
   *
   * @private
   * @param {Uint8Array} file
   * @returns {number}
   * @memberof Import
   */
  private determineFileVersion(file: string): number {
    if (file.startsWith('\x01\x06\x76\x33\x00\x51\x51')) {
      return Versions.v3;
    }

    return file.includes('^-^-^-^-^') ? Versions.newPascal : Versions.oldPascal;
  }

  /**
   * Morph raw pagedata (Pascal/Delphi) stream to a JS object with correct nesting
   * (does not cover properties. Only types and nesting)
   *
   * @private
   * @param {MFO} mfo
   * @memberof Import
   */
  private async parsePageTree(mfo: MFO) {
    // array of all fonts used in whole document
    const fonts = [];

    // there is currently no other possible way to resolve the tree structure
    // and all properties at the same time as to building a json string
    // and parsing it to an object. The relatively dynamic tree structure makes it
    // almost impossible to use a recursive approach.
    // tslint:disable-next-line: no-string-literal
    mfo['RawPages'].forEach((page: string) => {
      let str = '';
      page.split(/ObjMenue/).forEach((formularObject, position) => {
        // look ahead if the next formularObject starts with "NurBeiNeuanlageBerechnen"
        // since that value still belongs at this formularObject
        const hasStrangeCalculationFlag =
          page.split(/ObjMenue/)[position + 1]?.charCodeAt(1) === 24;

        if (formularObject.charCodeAt(1) === 24) {
          // remove the extra bytes which have been read
          // by the formularObject before
          formularObject = formularObject.slice(26);
        }

        if (
          formularObject.charCodeAt(0) === 8 ||
          formularObject.charCodeAt(0) === 9
        ) {
          str += '"Children": [';
          formularObject = formularObject.slice(5);
        }

        while (
          formularObject.charCodeAt(0) === 2 &&
          formularObject.charCodeAt(1) === 0
        ) {
          str += ']}';
          formularObject = formularObject.slice(2);
        }

        if (formularObject) {
          str += '{';
        }

        // currently disabled.
        // there is no need to keep the old pascal objects
        // str += '"Raw": "' + btoa(formularObject) + '",';

        // boxes in old pascal start their content with the border while
        // css starts the content after the border. to cope with this behaviour,
        // we move the BorderWidth and Type up in the array, temporarily save the value
        // and substract it from Top and Left. Type is used since the type otShape has an
        // 1px borderwidth even if nothing is specified
        let BorderWidth = 0;
        let Type = '';

        // this array defines the filter properties for every
        // possible variable inside of the pascal objects
        const filterArray = [
          {
            // Kind MUST come before PenWidth
            type: 'string',
            pattern: /Kind\x07/,
            skip: 5,
            template: (value) => {
              Type = value;

              // get class for type
              let additionalClassPropsStr = '';
              if (!this.translateKind(value).startsWith('Type not support')) {
                const transType = this.translateKind(value);
                additionalClassPropsStr += this.getClassProperties(transType);
              }

              return (
                `"Type": "${this.translateKind(value)}",` +
                additionalClassPropsStr
              );
            },
          },
          {
            // PenWidth MUST come before Left and Top
            type: 'integer',
            force: true,
            pattern: /PenWidth(\x02|\x03|\x04)/,
            skip: 8,
            template: (value) => {
              BorderWidth = value;
              return `"BorderWidth": ${
                Type === 'otShape' ? BorderWidth || 1 : 0
              },`;
            },
          },
          {
            type: 'integer',
            pattern: /Left(\x02|\x03|\x04)/,
            skip: 4,
            template: (value) =>
              `"X": ${value - (Type === 'otShape' ? BorderWidth || 1 : 0)},`,
          },
          {
            type: 'integer',
            pattern: /Top(\x02|\x03|\x04)/,
            skip: 3,
            template: (value) => {
              return `"Y": ${
                value - (Type === 'otShape' ? BorderWidth || 1 : 0)
              },`;
            },
          },
          {
            type: 'integer',
            pattern: /Width(\x02|\x03|\x04)/,
            skip: 5,
            template: (value) => `"Width": ${value},`,
          },
          {
            type: 'integer',
            pattern: /(?!Font\.)Height(\x02|\x03|\x04)/,
            skip: 6,
            template: (value) => `"Height": ${value},`,
          },
          {
            type: 'string',
            pattern: /Text(\x06|\x14)/,
            skip: 5,
            template: (value) => `"Text": "${value}",`,
          },
          {
            type: 'string',
            pattern: /Font\.Color\x07/,
            skip: 11,
            template: (value) => {
              return `"FontColor": "#${Colors[value] || '000000'}",`;
            },
          },
          {
            type: 'string',
            pattern: /Font\.Charset\x07/,
            skip: 13,
            template: (value) => `"FontCharset": "${value}",`,
          },
          {
            type: 'integer',
            pattern: /XOffset(\x02|\x03|\x04)/,
            skip: 7,
            template: (value) => `"OffsetX": ${value},`,
          },
          {
            type: 'integer',
            pattern: /YOffset(\x02|\x03|\x04)/,
            skip: 7,
            template: (value) => `"OffsetY": ${value},`,
          },
          {
            variable: 'FormFormat',
            type: 'string',
            pattern: /FormFormat\x07/,
            skip: 11,
            // this.translateFormat(value)
            template: () => '',
          },
          {
            /**
             * function BarCodeType2Str(Value: TStBarCodeType): AnsiString;
             *  begin
             *    case Value of
             *        bcUPC_A:           Result := 'UPC_A';
             *        bcUPC_E:           Result := 'UPC_E';
             *        bcEAN_8:           Result := 'EAN_8';
             *        bcEAN_13:          Result := 'EAN_13';
             *        bcInterleaved2of5: Result := 'Interleaved2of5';
             *        bcCodabar:         Result := 'Codabar';
             *        bcCode11:          Result := 'Code11';
             *        bcCode39:          Result := 'Code39';
             *        bcCode93:          Result := 'Code93';
             *        bcCode128:         Result := 'Code128';
             *        else               Result := '';
             *    end;
             *  end;
             */
            variable: 'BCType',
            // not exported or used by the old editor!
          },
          {
            variable: 'JpgImg.Data',

            type: 'image',
            pattern: /JpgImg\.Data(?!\x0a\x04\x00\x00\x00\x00)/,
            skip: 11,
            template: (value) => {
              return `"Image": "${value}",`;
            },
          },
          {
            // nicht mehr verwendet weil alle edit felder
            // und text felder zu "Input" gemacht werden
            variable: 'EditKind',
          },
          {
            type: 'integer',
            pattern: /Font\.Height(\x02|\x03|\x04)/,
            skip: 11,
            /**
             * The given Font Height is useless in CSS so we translate
             * it to the Font Size in Pixel
             * See https://wiki.freepascal.org/Font for formula!
             */
            template: (value) => `
            "FontSize": ${Math.round(((-1 * value) / 96) * 72 * 1.333)},
            `,
          },
          {
            type: 'string',
            pattern: /Font\.Name\x06/,
            skip: 10,
            template: (value) => {
              fonts.indexOf(value) === -1 && fonts.push(value);
              return `"FontFamily": "${value}",`;
            },
          },
          {
            variable: 'Font.Style',
            type: 'array',
            pattern: /Font\.Style\x0b\x06(.*?)\x00\x0a/,
            splitter: /\x08|\x0b/,
            index: 1,
            skip: 11,
            template: (value) => {
              return `
              "FontStyleBold": ${value.includes('fsBold')},
              "FontStyleItalic": ${value.includes('fsItalic')},
              "FontStyleUnderline": ${value.includes('fsUnderline')},
              "FontStyleStrikeOut": ${value.includes('fsStrikeOut')}, `;
            },
          },
          {
            // skipped since Font is present in every Object
            variable: 'ParentFont',
          },
          {
            // seems to be not implemented
            variable: 'OptBold',
          },
          {
            type: 'boolean',
            pattern: /OptShow\x08/,
            skip: 8,
            template: (value) => {
              return `"Print": ${!value},`;
            },
          },
          {
            type: 'string',
            pattern: /[^\.]Color\x07/,
            skip: 7,
            template: (value) => `"Color": "#${Colors[value] || 'FFFFFF'}",`,
          },
          {
            type: 'string',
            pattern: /BorderColor\x07/,
            skip: 12,
            template: (value) =>
              `"BorderColor": "#${Colors[value] || '000000'}",`,
          },
          {
            variable: 'Transparent',
            type: 'boolean',
            pattern: /Transparent\x08/,
            skip: 12,
            template: (value) => `"Transparent": ${value},`,
          },
          {
            type: 'string',
            pattern: /Combo\.Strings\x01\x06/,
            skip: 15,
            template: (value) =>
              `"Preselection": ["${value
                .split(';')
                .filter((f) => f)
                .join('","')}"],`,
          },
          {
            // no clue what this is
            variable: 'Writecard',
          },
          {
            // für checkbox gruppen
            // gruppe 0 = nicht gruppiert
            // index > 0 = alle über 0 sind 1 wert pro gruppe
            type: 'integer',
            pattern: /GroupIndex(\x02|\x03|\x04)/,
            skip: 10,
            template: (value) => `"GroupIndex": ${value},`,
          },
          {
            type: 'integer',
            pattern: /TabIndex(\x02|\x03|\x04)/,
            skip: 8,
            template: (value) => `"TabIndex": ${value},`,
          },
          {
            // DataKind (Auftragsvariable)
            variable: 'DataKind',

            type: 'string',
            pattern: /DataKind\x06/,
            skip: 9,
            template: (value) => {
              return `"DataKind": "${value}",`;
            },
          },
          {
            // Formula{Patient.Nachname}
            variable: 'Formula',

            type: 'string',
            pattern: /Formula\x06/,
            skip: 8,
            template: (value) => {
              let addString = '';

              // check if assignmentrange
              if (value.includes('#')) {
                const rangeBeg = value.indexOf('#') + 1;
                const rangeEnd =
                  value.indexOf(';', rangeBeg) > -1
                    ? value.indexOf(';', rangeBeg)
                    : value.length;

                const assignmentrange = value.substring(rangeBeg, rangeEnd);
                addString = `"AssignmentRange": "${parseInt(
                  assignmentrange.replace(/\D/g, ''),
                  10
                )}",`;
              }

              return `"Formula": "${value}",` + addString;
            },
          },
          {
            // Zeichenabstand?
            // Nicht auslesbar. wird geskippt
            variable: 'HPitch',
          },
          {
            // Zeilenabstand?
            // Nicht auslesbar. wird geskippt
            variable: 'Pitch',
          },
          {
            variable: 'Topic',
            type: 'string',
            pattern: /Topic\x06/,
            skip: 6,
            template: (value) => `"Topic": "${value}",`,
          },
          {
            variable: 'Item',
            type: 'string',
            pattern: /Item\x06/,
            skip: 5,
            template: (value) => `"Item": "${value}",`,
          },
          {
            // sf direkt verwerfen
            // pg = hintergrundbild wird mit ausgedruckt
            // date parameter parsen
            variable: 'Param',
            type: 'string',
            pattern: /Param\x06/,
            skip: 6,
            template: (value) => {
              let dateStr = '';
              const isDateInput = value.includes('date');
              dateStr += `"IsDateInput": ${isDateInput},`;

              // check field contains dateformat
              if (isDateInput && value.includes('format=')) {
                const dateFormatBegin = value.indexOf('format=') + 7;

                let dateFormat = value.substring(
                  dateFormatBegin,
                  value.indexOf(';', dateFormatBegin) > -1
                    ? value.indexOf(';', dateFormatBegin)
                    : value.length
                );

                dateFormat = this.getNewDateFormat(dateFormat);

                dateStr += `"DateFormat": "${dateFormat}",`;
              }

              return `"Param": "${value}",` + dateStr;
            },
          },
          {
            // Ausrichtung
            variable: 'Align',
            type: 'string',
            pattern: /Align\x07/,
            skip: 6,
            template: (value) => {
              if (value === 'taCenter') {
                return `"Align": "center",`;
              } else if (value === 'taRightJustify') {
                return `"Align": "right",`;
              } else {
                return `"Align": "left",`;
              }
            },
          },
          {
            variable: 'ObjMenue' /** NurBeiNeuanlageBerechnen	 */,
            // will be handled a little bit further down
          },
        ];

        filterArray.forEach((filter) => {
          let index = 0;

          switch (filter.type) {
            case 'image':
              index = formularObject.search(filter.pattern);

              if (index > -1) {
                str += filter.template(
                  btoa(
                    formularObject.substr(
                      index + filter.skip + 9,
                      formularObject.search(/\xff\xd9\x05/) -
                        index -
                        filter.skip -
                        9
                    )
                  )
                );
              }
              break;

            case 'boolean':
              index = formularObject.search(filter.pattern);
              if (index > -1) {
                str += filter.template(
                  formularObject
                    .substr(index + filter.skip, 1)
                    .charCodeAt(0) === 5
                );
              }
              break;
            case 'integer':
              index = formularObject.search(filter.pattern);
              if (index > -1) {
                str += filter.template(
                  this.parseIntDynamic(
                    formularObject.substr(index + filter.skip, 1),
                    formularObject.substr(index + filter.skip + 1, 4)
                  )
                );
              }
              if (Type !== '' && filter.force) {
                str += filter.template(0);
              }
              break;
            case 'string':
              index = formularObject.search(filter.pattern);

              if (index > -1) {
                const textlength = formularObject.substr(
                  index + filter.skip,
                  1
                );

                const skipNulls =
                  formularObject
                    .substr(index + filter.skip, 2)
                    .charCodeAt(1) === 0
                    ? 4
                    : 1;

                const text = decodeURIComponent(
                  escape(
                    formularObject.substr(
                      index + filter.skip + skipNulls,
                      textlength.charCodeAt(0)
                    )
                  )
                ).replace(/\"/g, '\\"');

                str += filter.template(text.trim());
              }
              break;

            case 'array':
              index = formularObject.search(filter.pattern);
              if (index > -1) {
                str += filter.template(
                  formularObject
                    .match(filter.pattern)
                    [filter.index].split(filter.splitter)
                );
              }
          }
        });

        str += hasStrangeCalculationFlag
          ? `"CalcOnCreation": ${hasStrangeCalculationFlag},`
          : '';
      });

      /**
       * make json valid by adding commas at certain points
       * and removing unnecessary properties
       */
      str = str.replace('"Raw": "",', '');

      str = str.split('}{').join('},{');

      try {
        const pageObject = JSON.parse(str);
        const po = Utils.objectFromMfoInterface(pageObject);
        po.Id = GuidService.generateGuid();
        // Utils.log(po);
        mfo.Pages.push(po);
      } catch (e) {
        // ToDo: find better suiting error message
        Utils.log(e, str);
        throw 'you f**ked up...';
      }
    });

    mfo.Fonts = [];
    for (let index = 0; index < fonts.length; index++) {
      let fontObject = await this.fontService.getFont(fonts[index]);

      if (fontObject) {
        mfo.Fonts.push(fontObject);
      }
    }

    // json object -> type conversion, map corresponding json types to MFO object types
    let ps = [];
    for (let i = 0; i < mfo.Pages.length; i++) {
      ps[i] = Utils.objectFromMfoInterface(mfo.Pages[i]);
    }
    mfo.Pages = ps;
  }

  /**
   * converts old pascal kind types to actual model types
   *
   * @param {*} kind
   * @return {*}
   * @memberof Import
   */
  translateKind(kind) {
    switch (kind) {
      case 'otText':
      case 'otDataText':
        return 'Text';
      case 'otEdit':
      case 'otDataEdit':
        return 'Input';
      case 'otCheck':
      case 'otDataCheck':
        return 'CheckBox';
      case 'otMemo':
      case 'otDataMemo':
        return 'Input';
      case 'otShape':
        return 'Shape';
      case 'otBalk':
      case 'otLine':
        return 'Line';
      case 'otImage':
      case 'otDataImage':
        return 'Image';
      case 'otDataBarCode':
        return 'BarCode';
      case 'otContainer':
        return 'Container';
      case 'otMarkField':
        return 'MarkField';
      default:
        // Utils.log(kind);
        throw 'Type not supported. Please contact the dev.';
    }
  }

  /**
   * converts old pascal format types to typescript types
   *
   * @param {*} form
   * @memberof Import
   */
  translateFormat(form) {
    /**
     * keep it as a comment but format is
     * already set by Width and Height
     * switch (form) {
     *  case 'foNone':
     *  case 'foA4':
     *  case 'foA4q':
     *  case 'foA5':
     *  case 'foA5q':
     *  case 'foA6':
     *  case 'foA6q':
     * }
     */
  }

  /**
   * parses strange pascal int definition to a modern typescript safe number type
   *
   * @param {*} length
   * @param {*} bytes
   * @return {*}
   * @memberof Import
   */
  parseIntDynamic(length, bytes) {
    if (length.charCodeAt(0) === 2) {
      bytes = bytes.substring(0, 1);
    } else if (length.charCodeAt(0) === 3) {
      bytes = bytes.substring(0, 2);
    } else if (length.charCodeAt(0) === 4) {
      bytes = bytes.substring(0, 4);
    }

    let bitArray = [];
    bytes.split('').forEach((byte) => {
      const res = [];
      const word = byte.charCodeAt(0);
      for (let x = 7; x >= 0; x--) {
        // tslint:disable-next-line: no-bitwise
        res.push((word & Math.pow(2, x)) >> x);
      }

      bitArray = [...res, ...bitArray];
    });

    let value = 0;
    for (let i = 0; i < bitArray.length; i++) {
      if (bitArray[i] === 0) {
        continue;
      }
      if (i === 0) {
        // first bit
        value -= Math.pow(2, bitArray.length - i) / 2;
      } else {
        value += Math.pow(2, bitArray.length - i - 1);
      }
    }

    if (value === undefined) {
      throw 'parseIntDynamic value is undefined';
    }

    return value;
  }

  /**
   * converts old editor dateformat to new
   *
   * @private
   * @param {string} oldformat
   * @returns new format as string
   * @memberof Import
   */
  private getNewDateFormat(dateformatstr: string) {
    dateformatstr = dateformatstr.toLowerCase();

    // days convert
    if (dateformatstr.includes('d')) {
      if (dateformatstr.includes('dddd')) {
        // old dddd = full dayname = new dddd
      } else if (dateformatstr.includes('ddd')) {
        // old ddd = day short = new ddd
      } else if (dateformatstr.includes('dd')) {
        // old dd = day with leading 0 = new DD
        dateformatstr = dateformatstr.replace('dd', 'DD');
      } else if (dateformatstr.includes('d')) {
        // old d = day without leading 0 = new D
        dateformatstr = dateformatstr.replace('d', 'D');
      }
    }

    // month convert
    if (dateformatstr.includes('m')) {
      if (dateformatstr.includes('mmmm')) {
        // old mmmm = full monthname = new MMMM
        dateformatstr = dateformatstr.replace('mmmm', 'MMMM');
      } else if (dateformatstr.includes('mmm')) {
        // old mmm = month short = new MMM
        dateformatstr = dateformatstr.replace('mmm', 'MMM');
      } else if (dateformatstr.includes('mm')) {
        // old mm = day with leading 0 = new MM
        dateformatstr = dateformatstr.replace('mm', 'MM');
      } else if (dateformatstr.includes('m')) {
        // old m = day without leading 0 = new M
        dateformatstr = dateformatstr.replace('m', 'M');
      }
    }

    // year convert
    if (dateformatstr.includes('y')) {
      // old yyyy = full year = new YYYY
      if (dateformatstr.includes('yyyy')) {
        dateformatstr = dateformatstr.replace('yyyy', 'YYYY');

        // old yyy = full year = new YYYY
      } else if (dateformatstr.includes('yyy')) {
        dateformatstr = dateformatstr.replace('yyy', 'YYYY');

        // old yy = year short = new YY
      } else if (dateformatstr.includes('yy')) {
        dateformatstr = dateformatstr.replace('yy', 'YY');

        // old y = year short = new YY
      } else if (dateformatstr.includes('y')) {
        dateformatstr = dateformatstr.replace('y', 'YY');
      }
    }

    // time convert
    if (dateformatstr.includes('t')) {
      // old tttt = full time = new HH:mm:ss
      if (dateformatstr.includes('tttt')) {
        dateformatstr = dateformatstr.replace('tttt', 'HH:mm:ss');

        // old ttt = full time = new HH:mm:ss
      } else if (dateformatstr.includes('ttt')) {
        dateformatstr = dateformatstr.replace('ttt', 'HH:mm:ss');

        // old tt = full time = new HH:mm:ss
      } else if (dateformatstr.includes('tt')) {
        dateformatstr = dateformatstr.replace('tt', 'HH:mm:ss');

        // old t = time hour:minute = HH:mm
      } else if (dateformatstr.includes('t')) {
        dateformatstr = dateformatstr.replace('t', 'HH:mm');
      }
    }

    return dateformatstr;
  }

  /**
   * dynamically get class properties for json
   *
   * @private
   * @param {*} className
   * @returns additionalinfo string for json
   * @memberof Import
   */
  private getClassProperties(className) {
    let nClass;

    // create new instance of given class
    switch (className) {
      case 'Text':
        nClass = new Text();
        break;
      case 'Input':
        nClass = new Edit();
        break;
      case 'CheckBox':
        nClass = new CheckBox();
        break;
      case 'Memo':
        nClass = new Edit();
        break;
      case 'Shape':
        nClass = new Shape();
        break;
      case 'Line':
        nClass = new Line();
        break;
      case 'Image':
        nClass = new Image();
        break;
      case 'BarCode':
        nClass = new Barcode();
        break;
      case 'Container':
        nClass = new Container();
        break;
      case 'MarkField':
        nClass = new MarkField();
        break;
      default:
        nClass = null;
    }

    let addStr = '';

    if (nClass) {
      const props = Object.getOwnPropertyNames(nClass);
      props.forEach((prop) => {
        const proptype = typeof nClass[prop];

        if (proptype === 'string') {
          addStr += `"${prop}":"${nClass[prop]}",`;
        } else {
          addStr += `"${prop}":${nClass[prop]},`;
        }
      });
    }

    return addStr;
  }
}
