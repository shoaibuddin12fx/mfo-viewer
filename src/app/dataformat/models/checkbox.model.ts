import { IDraggable } from '@interfaces/draggable';
import { ITabable } from '@interfaces/tabable';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * Checkbox model class
 *
 * @export
 * @class CheckBox
 * @extends {UiBaseSelectable}
 * @implements {IDraggable}
 */
export class CheckBox extends UiBaseSelectable implements IDraggable, ITabable {
  /**
   * Shows if element is Draggable (Implemented from Draggable})
   *
   * @memberof CheckBox
   */
  public IsDraggable = true;

  /**
   * Shows if element is double clicked
   *
   * @type {boolean}
   * @memberof CheckBox
   */
  public IsDoubleClicked: boolean;

  /**
   * state of Checkbox, checked or not
   *
   * @type {boolean}
   * @memberof CheckBox
   */
  public IsChecked: boolean;

  /**
   * int value handled by any formula or in tab index resolves
   *
   * @type {number}
   * @memberof CheckBox
   */
  public NumValue: number = 1;

  /**
   * Grouping Id for Checkboxes for radio group behaviour
   *
   * @type {number}
   * @memberof CheckBox
   */
  public GroupIndex: number;

  /**
   * BackgroundColor of element
   *
   * @memberof CheckBox
   */
  public BackgroundColor?;

  /**
   * Creates an instance of CheckBox.
   * @memberof CheckBox
   */
  constructor() {
    super();
    this.Interactive = true;
    this.BorderWidth = 0;
    this.GroupIndex = 0;
  }

  /**
   * returns number value if Checkbox is checked, otherwise 0
   *
   * @return {*}  {number}
   * @memberof CheckBox
   */
  public getValue(): number {
    return this.IsChecked ? this.NumValue : 0;
  }
}
