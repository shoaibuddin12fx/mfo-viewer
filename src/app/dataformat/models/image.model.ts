import { UiBaseSelectable } from './uibaseselectable';

/**
 * image model holding image information
 *
 * @export
 * @class Image
 * @extends {UiBaseSelectable}
 */
export class Image extends UiBaseSelectable {
  /**
   *
   * image as base64
   * @memberof Image
   */
  public Image: string;
}
