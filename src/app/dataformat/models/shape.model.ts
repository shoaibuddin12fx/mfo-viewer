import { IHasChildren } from '@interfaces/haschildren';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * shape model to display a shape
 *
 * @export
 * @class Shape
 * @extends {UiBaseSelectable}
 * @implements {IHasChildren}
 */
export class Shape extends UiBaseSelectable implements IHasChildren {
  /**
   * Child Shapes for Grouping several Shapes
   *
   * @type {Shape[]}
   * @memberof Shape
   */
  public Children: Shape[];
}
