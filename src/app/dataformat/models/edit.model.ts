import { IFontInfo } from '@interfaces/fontinfo';
import { IInteractiveContextMenu } from '@interfaces/interactivecontextmenu';
import { ITabable } from '@interfaces/tabable';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * edit model for user input
 *
 * @export
 * @class Edit
 * @extends {UiBaseSelectable}
 * @implements {IInteractiveContextMenu}
 * @implements {IFontInfo}
 */
export class Edit
  extends UiBaseSelectable
  implements IInteractiveContextMenu, IFontInfo, ITabable
{
  /**
   *
   * field is doubleclicked
   * @type {boolean}
   * @memberof Text
   */
  public IsDoubleClicked: boolean;

  /**
   * Text alignment
   *
   * @type {string}
   * @memberof Text
   */
  public Align: string = 'left';

  /**
   * FontFamily setting (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof Text
   */
  public FontFamily: string;

  /**
   * FontCharset setting (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof Text
   */
  public FontCharset: string;

  /**
   * Font color (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof Text
   */
  public FontColor: string;

  /**
   * Font Size (Implemented from FontInfo)
   *
   * @type {number}
   * @memberof Text
   */
  public FontSize: number;

  /**
   * text underlined boolean
   *
   * @type {boolean}
   * @memberof Edit
   */
  public FontStyleUnderline: boolean;

  /**
   * text strikedout boolean
   *
   * @type {boolean}
   * @memberof Edit
   */
  public FontStyleStrikeOut: boolean;

  /**
   * text bold boolean
   *
   * @type {boolean}
   * @memberof Edit
   */
  public FontStyleBold: boolean;

  /**
   * text italic boolean
   *
   * @type {boolean}
   * @memberof Edit
   */
  public FontStyleItalic: boolean;

  /**
   * field is number only
   *
   * @type {boolean}
   * @memberof Edit
   */
  public Number: boolean;

  /**
   * text content
   *
   * @type {string}
   * @memberof Text
   */
  public Text: string;

  /**
   * field is multiline
   *
   * @readonly
   * @memberof Text
   */
  public Multiline: boolean;

  /**
   * databinding topic
   *
   * @type {string}
   * @memberof Edit
   */
  public Topic: string;

  /**
   * databinding item
   *
   * @type {string}
   * @memberof Edit
   */
  public Item: string;

  /**
   * field is a date field
   *
   * @type {boolean}
   * @memberof Edit
   */
  public IsDateInput: boolean;

  /**
   * preselection for right click contextmenu
   *
   * @type {string}
   * @memberof Edit
   */
  public Preselection: string[];

  /**
   * setter for Preselection property as string, parameter has to be any, handles string input and array input
   *
   * @memberof Edit
   */
  public set PreselectionString(v: any) {
    if (typeof v == 'string') {
      this.Preselection = v.replace(/(^,)|(,$)/g, '').split(';');
    } else {
      this.Preselection = v;
    }
  }

  /**
   * getter for Preselection property as string
   *
   * @type {*}
   * @memberof Edit
   */
  public get PreselectionString(): any {
    if (this.Preselection === undefined) this.Preselection = [];
    return this.Preselection.join(';');
  }

  /**
   * Dateformat string
   *
   * @type {string}
   * @memberof Edit
   */
  public DateFormat: string = 'DD.MM.YYYY';

  /**
   * space between lines
   *
   * @type {number}
   * @memberof Edit
   */
  public LineSpacing: number = 1;

  /**
   * space between chars
   *
   * @type {number}
   * @memberof Edit
   */
  public LetterSpacing: number = 0;

  /**
   * assignment date range in days
   *
   * @type {number}
   * @memberof Edit
   */
  public AssignmentRange: number = 0;

  /**
   * Creates an instance of Edit.
   * @memberof Edit
   */
  constructor() {
    super();
    this.Interactive = true;
  }

  /**
   * returns Preselection string array
   *
   * @return {*}  {string[]}
   * @memberof Edit
   */
  public getItemLabel(): string[] {
    if (this.Preselection === undefined || this.Preselection.length == 0)
      this.Preselection = [];
    if (!this.IsDateInput) return this.Preselection;
    return [];
  }

  /**
   * Concats the preselection itemlabel to Displayed text
   *
   * @param {number} idx
   * @return {*}  {void}
   * @memberof Edit
   */
  public clickedItem(idx: number): void {
    if (this.PreselectionString === undefined || !this.PreselectionString)
      return;

    this.Text = this.Text + this.Preselection[idx];
  }
}
