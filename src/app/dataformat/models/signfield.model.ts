import { ITabable } from '@interfaces/tabable';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * model for signfield component
 *
 * @export
 * @class SignField
 * @extends {UiBaseSelectable}
 */
export class SignField extends UiBaseSelectable implements ITabable {
  /**
   * Font color for signature in SignFild
   *
   * @type {string}
   * @memberof MarkField
   */
  public FontColor: string;
}
