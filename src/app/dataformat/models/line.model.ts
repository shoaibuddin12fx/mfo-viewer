import { UiBaseSelectable } from './uibaseselectable';

/**
 * line model, to display a simple line
 *
 * @export
 * @class Line
 * @extends {UiBaseSelectable}
 */
export class Line extends UiBaseSelectable {
  /**
   * X2 coordinate point as endpoint of line
   *
   * @type {number}
   * @memberof Line
   */
  public X2: number;

  /**
   * Y2 coordinate point as endpoint of line
   *
   * @type {number}
   * @memberof Line
   */
  public Y2: number;
}
