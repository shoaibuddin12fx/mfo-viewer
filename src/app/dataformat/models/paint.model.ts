import { IStroke } from '@interfaces/stroke';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * model for signfield component
 *
 * @export
 * @class Painting
 * @extends {UiBaseSelectable}
 */
export class Painting extends UiBaseSelectable {
  /**
   * Font color for signature in SignFild
   *
   * @type {string}
   * @memberof Painting
   */
  public FontColor: string = '#000000';

  /**
   * pen size for strokes made on the canvas
   *
   * @type {number}
   * @memberof Painting
   */
  public PenSize: number = 1;

  /**
   * array of user strokes
   *
   * @type {IStroke[]}
   * @memberof Painting
   */
  public Strokes: IStroke[] = [];

  /**
   * image as base64
   *
   * @memberof Painting
   */
  public Image: string;
}
