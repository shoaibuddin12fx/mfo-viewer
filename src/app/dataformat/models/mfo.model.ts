import { IFormularOptions } from '@interfaces/formularoptions';
import { Utils } from '../util';
import { Container } from './container.model';
import { Font } from './font.model';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * MFO as in the file extension
 *
 * @export
 * @class MFO
 */
export class MFO {
  /**
   * raw content of the file
   *
   * @type {string}
   * @memberof MFO
   */
  public FileContent: string;

  /**
   * page collection
   *
   * @type {UiBaseSelectable[]}
   * @memberof MFO
   */
  public Pages: UiBaseSelectable[];

  /**
   * available fonts
   *
   * @type {Font[]}
   * @memberof MFO
   */
  public Fonts: Font[];

  /**
   * version string of mfo document
   *
   * @type {string}
   * @memberof MFO
   */
  public Version: string;

  /**
   * actual active element
   *
   * @type {UiBaseSelectable}
   * @memberof MFO
   */
  public activeElement: UiBaseSelectable;

  /**
   * formularoptions
   *
   * @type {IFormularOptions}
   * @memberof MFO
   */
  public FormularOptions: IFormularOptions = {
    krankenblattbezeichnung: '',
  };

  /**
   * method to find parent container element for given elem
   *
   * @param {UiBaseSelectable} elem
   * @return {*}  {*}
   * @memberof MFO
   */
  public getParentContainerForElement(elem: UiBaseSelectable): any {
    let splits = elem.Path.split('.');

    // return null if splits.length==2 because it is page
    if (splits.length === 2) return;

    // page
    let parent = this[splits[0]][splits[1]];

    for (let i = 2; i < splits.length - 2; i = i + 2) {
      parent = parent[splits[i]][splits[i + 1]];
    }

    return parent;
  }

  /**
   * Returns current active Page
   *
   * @return {*}  {UiBaseSelectable}
   * @memberof MFO
   */
  public getActivePage(): Container {
    for (let i = 0; i < this.Pages.length; i++) {
      if (this.Pages[i].IsActive) return this.Pages[i] as Container;
    }
  }

  /**
   * returns active element
   *
   * @param {*} root
   * @return {*}
   * @memberof MFO
   */
  public getActiveElement(root: any) {
    root = root;
    let elem = undefined;
    if (root === undefined) return elem;
    for (let i = 0; i < root.length && !elem; i++) {
      elem = root[i].IsActive ? root[i] : undefined;
      if (!elem && Utils.isContainer(root[i]))
        elem = this.getActiveElement(root[i].Children);
    }
    return elem;
  }

  /**
   * returns a light copy of the existing mfo object to save it properly at server side, it removes all overhead which is just used at runtime
   *
   * @return {*}  {*}
   * @memberof MFO
   */
  public getDenoisedAnonymousCopy(): any {
    let copy = {
      FormularOptions: { ...this.FormularOptions },
      Version: this.Version,
      Fonts: this.Fonts,
      Pages: [],
    };

    for (let i = 0; i < this.Pages.length; i++) {
      copy.Pages.push(Utils.denoiseElement(this.Pages[i]));
    }
    Utils.logPrefixed('MFO-model - denoised', copy);
    return copy;
  }
}
