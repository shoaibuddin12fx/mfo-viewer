/**
 *
 *
 * @export
 * @class Font
 */
export class Font {
  /**
   * font name
   *
   * @type {string}
   * @memberof Font
   */
  public Name: string;

  /**
   * font family
   *
   * @type {string}
   * @memberof Font
   */
  public Family: string;

  /**
   * font file
   *
   * @type {string}
   * @memberof Font
   */
  public File: string;
}
