import { ITabable } from '@interfaces/tabable';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * Barcode model class
 *
 * @export
 * @class Barcode
 * @extends {UiBaseSelectable}
 */
export class Barcode extends UiBaseSelectable implements ITabable {
  /**
   * Text content for Barcode display
   *
   * @type {string}
   * @memberof Barcode
   */
  public Text: string;

  /**
   *
   * qr or pdf417
   * @type {string}
   * @memberof Barcode
   */
  public BarcodeType: string;
}
