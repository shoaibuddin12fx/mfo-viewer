import { IFontInfo } from '@interfaces/fontinfo';
import { ITabable } from '@interfaces/tabable';
import { UiBaseSelectable } from './uibaseselectable';

/**
 * model for fixed text
 *
 * @export
 * @class Text
 * @extends {UiBaseSelectable}
 * @implements {IFontInfo}
 */
export class Text extends UiBaseSelectable implements IFontInfo, ITabable {
  /**
   * determines if element is doubleclicked
   *
   * @type {boolean}
   * @memberof Text
   */
  public IsDoubleClicked: boolean;

  /**
   * Text alignment
   *
   * @type {string}
   * @memberof Text
   */
  public Align: string = 'left';

  /**
   * FontFamily setting (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof Text
   */
  FontFamily: string;

  /**
   * FontCharset setting (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof Text
   */
  FontCharset: string;

  /**
   * Font color (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof Text
   */
  FontColor: string;

  /**
   * Font Size (Implemented from FontInfo)
   *
   * @type {number}
   * @memberof Text
   */
  FontSize: number;

  /**
   * default text style is underlined
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleUnderline: boolean;

  /**
   * default text style is striked out
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleStrikeOut: boolean;

  /**
   * default font style is bold
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleBold: boolean;

  /**
   * default font style is italic
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleItalic: boolean;

  /**
   * Text content to display
   *
   * @type {string}
   * @memberof Text
   */
  public Text: string;

  /**
   * databinding topic
   *
   * @type {string}
   * @memberof Text
   */
  public Topic: string;

  /**
   * databinding item
   *
   * @type {string}
   * @memberof Text
   */
  public Item: string;

  /**
   * field is a date field
   *
   * @type {boolean}
   * @memberof Text
   */
  public IsDateInput: boolean;

  /**
   * date format of field
   *
   * @type {string}
   * @memberof Text
   */
  public DateFormat: string = 'DD.MM.YYYY';

  /**
   * space between chars
   *
   * @type {number}
   * @memberof Text
   */
  public LetterSpacing: number = 0;

  /**
   * space between lines
   *
   * @type {number}
   * @memberof Text
   */
  public LineSpacing: number = 1;

  /**
   * assignment date range in days
   *
   * @type {number}
   * @memberof Text
   */
  public AssignmentRange: number = 0;
}
