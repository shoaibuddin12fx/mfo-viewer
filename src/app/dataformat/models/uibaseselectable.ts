import { ISelectable } from '@interfaces/selectable';
import { UiBase } from './uibase.model';

/**
 * Base class for UI element models with selectable interface
 *
 * @export
 * @class UiBaseSelectable
 * @extends {UiBase}
 * @implements {ISelectable}
 */
export class UiBaseSelectable extends UiBase implements ISelectable {
  /**
   * Shows if Element is active (Implemented from ISelectable)
   *
   * @type {boolean}
   * @memberof UiBaseSelectable
   */
  public IsActive: boolean;

  /**
   * Shows if Element is selected (Implemented from ISelectable)
   *
   * @type {boolean}
   * @memberof UiBaseSelectable
   */
  public IsSelected: boolean;
}
