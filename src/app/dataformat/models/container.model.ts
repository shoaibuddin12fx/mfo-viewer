import { IContainer } from '@interfaces/container';
import { UiBaseSelectable } from './uibaseselectable';

/**
 *
 *
 * @export
 * @class Container
 */
export class Container extends UiBaseSelectable implements IContainer {
  /**
   * Child elements
   *
   * @type {Container[]}
   * @memberof Container
   */
  public Children: UiBaseSelectable[];

  /**
   * ID of Container (used for Pages)
   *
   * @memberof Container
   */
  public Id: string;

  /**
   * Background Image
   *
   * @memberof Container
   */
  public Image: string;

  /**
   * Font Size
   *
   * @memberof Container
   */
  public FontSize;

  /**
   * default text style is underlined
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleUnderline: boolean;

  /**
   * default text style is striked out
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleStrikeOut: boolean;

  /**
   * default font style is bold
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleBold: boolean;

  /**
   * default font style is italic
   *
   * @type {boolean}
   * @memberof Container
   */
  public FontStyleItalic: boolean;

  /**
   * Title of Container
   *
   * @memberof Container
   */
  public Text: string;

  /**
   * Tab Index of Container
   *
   * @type {number}
   * @memberof Container
   */
  public TabIndex: number;

  /**
   * Alignment of Text
   *
   * @memberof Container
   */
  public Align?: string;

  /**
   * Font Famlily
   *
   * @memberof Container
   */
  public FontFamily?;

  /**
   * Font Color
   *
   * @memberof Container
   */
  public FontColor?;

  /**
   * Formularoptionen auf Seiten-Ebene
   *
   * @type {string}
   * @memberof Container
   */
  public DataKind: string;

  /**
   * space between lines
   *
   * @type {number}
   * @memberof Edit
   */
  public LineSpacing: number = 1;

  /**
   * space between chars
   *
   * @type {number}
   * @memberof Edit
   */
  public LetterSpacing: number = 0;


  /**
   * Standard Font
   *
   * @memberof Container
   */
   public IFont?;

  /**
   * returns true if container is Page by object path
   *
   * @return {*}  {boolean}
   * @memberof Container
   */
  public isPage(): boolean {
    return this.Path?.split('.').length === 2;
  }
}
