import { IBackgroundInfo } from '@interfaces/backgroundinfo';
import { IBaseInfo } from '@interfaces/baseinfo';
import { IBorderInfo } from '@interfaces/borderinfo';
import { IMovable } from '@interfaces/movable';
import { IPrintable } from '@interfaces/printable';

/**
 * Base class for UI element models
 *
 * @export
 * @class UiBase
 * @implements {IBaseInfo}
 * @implements {IBorderInfo}
 * @implements {IMovable}
 * @implements {IPrintable}
 * @implements {IBackgroundInfo}
 */
export class UiBase
  implements IBaseInfo, IBorderInfo, IMovable, IPrintable, IBackgroundInfo
{
  /**
   * path of element
   *
   * @type {string}
   * @memberof UiBase
   */
  public Path: string;

  /**
   * UI Html Element reference
   *
   * @type {UiBase}
   * @memberof UiBase
   */
  public uiElement: UiBase;

  /**
   * Sets Backgroundcolor of given Element (Implemented from IBackgroundInfo)
   *
   * @type {string}
   * @memberof UiBase
   */
  public Color: string = '#FFFFFF';

  /**
   * Defines if element is printable (Implemented from IPrintable)
   *
   * @type {boolean}
   * @memberof UiBase
   */
  public Print: boolean;

  /**
   * The width of the elment (Implemented from IMovable)
   *
   * @type {number}
   * @memberof UiBase
   */
  public Width: number;

  /**
   * The height of the element (Implemented from IMovable)
   *
   * @type {number}
   * @memberof UiBase
   */
  public Height: number;

  /**
   * X position of the element (Implemented from IMoveable)
   *
   * @type {number}
   * @memberof UiBase
   */
  public X: number = 0;

  /**
   * Y Position of the element (Implemented from IMoveable)
   *
   * @type {number}
   * @memberof UiBase
   */
  public Y: number = 0;

  /**
   * The name of the Bordercolor for this element (Implemented from IBorderColor)
   *
   * @type {string}
   * @memberof UiBase
   */
  public BorderColor: string = '#000000';

  /**
   * The width of the assigned border (Implemented from IBorderColor)
   *
   * @type {number}
   * @memberof UiBase
   */
  public BorderWidth: number;

  /**
   * Type of element as string (Implemented from IBaseInfo)
   *
   * @type {string}
   * @memberof UiBase
   */
  public Type: string;

  /**
   * TabIndex for KeyBoard navigation and cross references (Implemented from IBaseInfo)
   *
   * @type {number}
   * @memberof UiBase
   */
  public TabIndex: number = 0;

  /**
   * property to show if element is interactive (Implemented from IBaseInfo)
   *
   * @type {boolean}
   * @memberof UiBase
   */
  public Interactive?: boolean;

  /**
   * sets the actual element as active element
   *
   * @memberof UiBase
   */
  public setAsActiveElement() {
    setTimeout(() => {
      window.dispatchEvent(
        new CustomEvent('setActiveElement', {
          bubbles: true,
          detail: { path: this.Path },
        })
      );
    }, 1);
  }
}
