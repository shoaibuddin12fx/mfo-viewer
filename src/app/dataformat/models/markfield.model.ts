import { UiBaseSelectable } from './uibaseselectable';
import { IPoint } from '@interfaces/point';

/**
 * MarkField component
 *
 * @export
 * @class MarkField
 * @extends {UiBaseSelectable}
 */
export class MarkField extends UiBaseSelectable {
  /**
   * Array containing the marks drawn by user
   *
   * @type {Array<IPoint>}
   * @memberof MarkField
   */
  public Marks: Array<IPoint> = [];

  /**
   * font color as color for marks
   *
   * @type {string}
   * @memberof MarkField
   */
  public FontColor: string;

  /**
   * image as base64
   *
   * @memberof MarkField
   */
  public Image: string;

  /**
   * connect marks with lines
   *
   * @type {boolean}
   * @memberof MarkField
   */
  public Connect: boolean;

  /**
   * color of marks
   *
   * @type {string}
   * @memberof MarkField
   */
  public Color: string;

  /**
   * background-color
   *
   * @type {string}
   * @memberof MarkField
   */
  public BackgroundColor: string;
}
