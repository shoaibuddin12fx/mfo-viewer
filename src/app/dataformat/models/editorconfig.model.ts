/**
 * Editor configuration
 *
 * @export
 * @class EditorConfig
 */
export class EditorConfig {
  /**
   * show a background grid
   *
   * @private
   * @type {boolean}
   * @memberof EditorConfig
   */
  private _showGrid: boolean;

  /**
   * getter for ShowGrid
   *
   * @readonly
   * @type {boolean}
   * @memberof EditorConfig
   */
  public get ShowGrid(): boolean {
    return this._showGrid;
  }

  /**
   * setter for ShowGrid
   *
   * @memberof EditorConfig
   */
  public set Grid(value: boolean) {
    this._showGrid = value;
  }

  /**
   * grid size
   *
   * @private
   * @type {number}
   * @memberof EditorConfig
   */
  private _gridSize: number;

  /**
   * getter for grid size
   *
   * @type {number}
   * @memberof EditorConfig
   */
  public get GridSize(): number {
    return this._gridSize;
  }

  /**
   * setter for grid size
   *
   * @memberof EditorConfig
   */
  public set GridSize(value: number) {
    this._gridSize = value;
  }

  /**
   * zoom
   *
   * @private
   * @type {number}
   * @memberof EditorConfig
   */
  private _zoom: number;

  /**
   * getter for zoom
   *
   * @type {number}
   * @memberof EditorConfig
   */
  public get Zoom(): number {
    return this._zoom || 1;
  }

  /**
   *setter for zoom
   *
   * @memberof EditorConfig
   */
  public set Zoom(value: number) {
    this._zoom = value;
  }

  /**
   * minimal zoom
   *
   * @private
   * @type {number}
   * @memberof EditorConfig
   */
  private _minZoom: number;

  /**
   * getter for minimal zoom
   *
   * @type {number}
   * @memberof EditorConfig
   */
  public get MinZoom(): number {
    return this._minZoom || 0.5;
  }

  /**
   * setter for minimal zoom
   *
   * @memberof EditorConfig
   */
  public set MinZoom(value: number) {
    this._minZoom = value;
  }

  /**
   * maximum zoom
   *
   * @private
   * @type {number}
   * @memberof EditorConfig
   */
  private _maxZoom: number;
  /**
   *getter for maximum zoom
   *
   * @type {number}
   * @memberof EditorConfig
   */
  public get MaxZoom(): number {
    return this._maxZoom || 2;
  }

  /**
   * setter for maximum zoom
   *
   * @memberof EditorConfig
   */
  public set MaxZoom(value: number) {
    this._maxZoom = value;
  }

  /**
   * default font family
   *
   * @private
   * @type {string}
   * @memberof EditorConfig
   */
  private _fontFamily: string;
  /**
   * getter for font family
   *
   * @type {string}
   * @memberof EditorConfig
   */
  public get FontFamily(): string {
    return this._fontFamily || 'Segoe UI';
  }

  /**
   * setter for font family
   *
   * @memberof EditorConfig
   */
  public set FontFamily(value: string) {
    this._fontFamily = value;
  }

  /**
   * default font size
   *
   * @private
   * @type {number}
   * @memberof EditorConfig
   */
  private _fontSize: number;
  /**
   * getter for font size
   *
   * @type {number}
   * @memberof EditorConfig
   */
  public get FontSize(): number {
    return this._fontSize || 12;
  }

  /**
   * setter for font size
   *
   * @memberof EditorConfig
   */
  public set FontSize(value: number) {
    this._fontSize = value;
  }
}
