import {
  Barcode,
  CheckBox,
  Container,
  Edit,
  Font,
  Line,
  MarkField,
  Shape,
  Text,
  Image,
  SignField,
} from './models';
import { MFO } from './models/mfo.model';
import { UiBaseSelectable } from './models/uibaseselectable';

/**
 * Debug levels for consol logs
 *
 * @enum {number}
 */
enum DEBUG_LEVEL {
  ALL = 0,
  WARNINGS = 1,
  FAILURES = 2,
  NONE = 3,
}

/**
 * Utility collection
 *
 * @export
 * @class Utils
 */
export class Utils {
  /**
   * actual DEBUG level
   *
   * @static
   * @type {DEBUG_LEVEL}
   * @memberof Utils
   */
  public static DEBUG: DEBUG_LEVEL = DEBUG_LEVEL.ALL;

  /**
   * Conversion from MFO interface type to corresponding MFO object type
   *
   * @static
   * @param {*} obj
   * @return {*}  {*}
   * @memberof Utils
   */
  public static objectFromMfoInterface(obj: any): any {
    let newObj = undefined;
    let childs: Container[] = [];

    // switch on type of json object and create corresponding type safe object
    switch (obj.Type.toLowerCase()) {
      case 'container':
        newObj = new Container();
        break;
      case 'text':
        newObj = new Text();
        break;
      case 'input':
      case 'memo':
        newObj = new Edit();
        break;
      case 'checkbox':
        newObj = new CheckBox();
        break;
      case 'shape':
        newObj = new Shape();
        break;
      case 'line':
        newObj = new Line();
        break;
      case 'image':
        newObj = new Image();
        break;
      case 'page':
        newObj = new Container();
        break;
      case 'font':
        newObj = new Font();
        break;
      case 'barcode':
        newObj = new Barcode();
        break;
      case 'markfield':
        newObj = new MarkField();
        break;
      case 'signfield':
        newObj = new SignField();
        break;
      default:
        Utils.fail('error, type missmatch', obj.Type);
        return;
    }

    // iterate over all properties and set them for new Object (doesn't care about walking duck problem,
    // but fits better because getOwnPropertyNames mostly just return first property, because all other properties are inherited from interfaces)
    let props = Object.getOwnPropertyNames(obj);
    props.forEach((p) => {
      // just set properties which are defined, strict compare to undefined, because of boolean properties set to false
      if (obj[p] != undefined) newObj[p] = obj[p];
    });

    // check for Children and sub container, no matter what element is actually parsed (Shape as Child of Shape etc...)
    if (obj.Children != undefined && obj.Children.length > 0) {
      for (let i = 0; i < obj.Children.length; i++) {
        childs[i] = this.objectFromMfoInterface(obj.Children[i]);
      }
      newObj.Children = childs;
    }
    return newObj;
  }

  /**
   * checks if a given property name is set in given object
   *
   * @static
   * @param {*} obj
   * @param {string} propName
   * @return {*}  {boolean}
   * @memberof Utils
   */
  public static hasPropertySet(obj: any, propName: string): boolean {
    return obj[propName] !== undefined;
  }

  /**
   * checks if a given obj is a MFO container by checking for 'Children' property or if its an instance of Container
   *
   * @static
   * @param {*} obj object to check
   * @return {*}  {boolean} true if obj is container
   * @memberof Utils
   */
  public static isContainer(obj: any): boolean {
    return obj instanceof Container;
  }

  /**
   * returns active container for given element, returns element itself if it is a container
   *
   * @static
   * @param {UiBaseSelectable} element
   * @param {MFO} mfo object
   * @return {*}  active container
   * @memberof Utils
   */
  public static getActiveContainer(element: UiBaseSelectable, mfo: MFO): any {
    // check if element is container
    if (Utils.isContainer(element)) return element;

    // get active page
    let activePage = mfo.getActivePage();

    // returns if element is active page (should never happen here, because page is container and should return first)
    if (activePage === element) return activePage;

    // get parent container
    let parent = mfo.getParentContainerForElement(element) ?? activePage;

    return parent;
  }

  /**
   * iterates over given path of element and adjusts the x and y position according to the layer it was created on
   *
   * @static
   * @param {any[]} path
   * @param {UiBaseSelectable} element
   * @memberof Utils
   */
  public static setMouseEventOffsetByEventPath(
    path: any[],
    element: UiBaseSelectable
  ) {
    let idx = 0;
    while (
      path[idx].localName.toLowerCase() === 'mfo-container' ||
      path[idx].localName.toLowerCase() === 'mfo-shape'
    ) {
      element.X += path[idx].offsetLeft;
      element.Y += path[idx].offsetTop;

      idx += 1;
    }
  }

  /**
   * deletes properties from element which aren't necessary for save
   *
   * @static
   * @param {*} element
   * @return {*}  {*}
   * @memberof Utils
   */
  public static denoiseElement(element: any): any {
    // noise properties
    const noise: string[] = [
      'IsActive',
      'IsSelected',
      'IsDoubleClicked',
      'uiElement',
      'Path',
      'Children',
    ];

    const elem = { Type: undefined, Children: undefined };

    // set properties manually and ignore noise
    let props = Object.getOwnPropertyNames(element);
    props.forEach((p) => {
      // just set properties which are defined, strict compare to undefined, because of boolean properties set to false
      if (element[p] != undefined && noise.indexOf(p) == -1)
        elem[p] = element[p];
    });

    // switch on type to handle children
    if (
      elem.Type == 'Container' ||
      (elem.Type == 'Shape' && element.Children != undefined)
    ) {
      elem.Children = [];
      for (let i = 0; i < element.Children.length; i++) {
        elem.Children.push(this.denoiseElement(element.Children[i]));
      }
    }
    return elem;
  }

  /**
   * logs given args with a given prefix to console if DEBUG LEVEL is ALL
   *
   * @static
   * @param {string} prefix
   * @param {...any[]} args
   * @return {*}
   * @memberof Utils
   */
  public static logPrefixed(prefix: string, ...args: any[]) {
    if (this.DEBUG > DEBUG_LEVEL.ALL) return;
    args = this.buildArgs(prefix, ...args);
    console.log(...args);
  }

  /**
   * logs given args to console if DEBUG LEVEL is ALL
   *
   * @static
   * @param {...any[]} args
   * @return {*}
   * @memberof Utils
   */
  public static log(...args: any[]) {
    if (this.DEBUG > DEBUG_LEVEL.ALL) return;
    args = this.buildArgs(undefined, ...args);
    console.log(...args);
  }

  /**
   * logs a warning to console with given prefix if DEBUG LEVEL is ALL or WARNING
   *
   * @static
   * @param {string} prefix
   * @param {...any[]} args
   * @return {*}
   * @memberof Utils
   */
  public static warnPrefixed(prefix: string, ...args: any[]) {
    if (this.DEBUG > DEBUG_LEVEL.WARNINGS) return;
    args = this.buildArgs(prefix, ...args);
    console.warn(...args);
  }

  /**
   * logs a warning to console if DEBUG LEVEL is ALL or WARNING
   *
   * @static
   * @param {...any[]} args
   * @return {*}
   * @memberof Utils
   */
  public static warn(...args: any[]) {
    if (this.DEBUG > DEBUG_LEVEL.WARNINGS) return;
    args = this.buildArgs(undefined, ...args);
    console.warn(...args);
  }

  /**
   * logs an error to console with given prefix when DEBUG LEVEL is not NONE
   *
   * @static
   * @param {string} prefix
   * @param {...any[]} args
   * @return {*}
   * @memberof Utils
   */
  public static failPrefixed(prefix: string, ...args: any[]) {
    if (this.DEBUG > DEBUG_LEVEL.FAILURES) return;
    args = this.buildArgs(prefix, ...args);
    console.error(...args);
  }

  /**
   * logs an error to console when DEBUG LEVEL is not NONE
   *
   * @static
   * @param {...any[]} args
   * @return {*}
   * @memberof Utils
   */
  public static fail(...args: any[]) {
    if (this.DEBUG > DEBUG_LEVEL.FAILURES) return;
    args = this.buildArgs(undefined, ...args);
    console.error(...args);
  }

  /**
   * returns a current time stamp string
   *
   * @private
   * @static
   * @return {*}  {string}
   * @memberof Utils
   */
  private static getTimeStampString(): string {
    const date = new Date();
    let str =
      date.toLocaleDateString() + ', ' + date.toLocaleTimeString() + '  ::  ';
    return str;
  }

  /**
   * builds arguments to push to console output with a given prefix and timestamp
   *
   * @private
   * @static
   * @param {string} prefix
   * @param {...any[]} args
   * @return {*}
   * @memberof Utils
   */
  private static buildArgs(prefix: string, ...args: any[]) {
    args.splice(0, 0, this.getTimeStampString(), (prefix ?? '') + '\n');
    return args;
  }
}
