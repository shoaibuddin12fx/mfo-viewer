/**
 * contains all mandatory properties for selectable elements
 *
 * @export
 * @interface ISelectable
 */
export interface ISelectable {
  /**
   * determines that the element is active
   *
   * @type {boolean}
   * @memberof ISelectable
   */
  IsActive: boolean;

  /**
   * determines that an element is selected
   *
   * @type {boolean}
   * @memberof ISelectable
   */
  IsSelected: boolean;
}
