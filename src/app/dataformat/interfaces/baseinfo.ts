/**
 * contains all mandatory properties for BaseElement elements
 *
 * @export
 * @interface IBaseInfo
 */
export interface IBaseInfo {
  /**
   * Type string of element type
   *
   * @type {string}
   * @memberof IBaseInfo
   */
  Type: string;

  /**
   * TabIndex of interactive element for keyboard navigation and cross references
   *
   * @type {number}
   * @memberof IBaseInfo
   */
  TabIndex: number;

  /**
   * determines if element is interactive
   *
   * @type {boolean}
   * @memberof IBaseInfo
   */
  Interactive?: boolean;
}
