/**
 * Interface to descripe a simple coordinate by its X and Y coordinates
 *
 * @export
 * @interface IPoint
 */
export interface IPoint {
  /**
   * X coordinate
   *
   * @type {number}
   * @memberof IPoint
   */
  X: number;

  /**
   * Y coordinate
   *
   * @type {number}
   * @memberof IPoint
   */
  Y: number;
}
