import { IPoint } from './point';

export interface IStroke {
  Coordinates: IPoint[];
}
