/**
 * contains all mandatory properties for BackgroundInfo elements
 *
 * @export
 * @interface IDraggable
 */
export interface IDraggable {
  /**
   * determines if element is draggable
   *
   * @type {boolean}
   * @memberof IDraggable
   */
  IsDraggable: boolean;
}
