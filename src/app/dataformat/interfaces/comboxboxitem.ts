/**
 * contains all mandatory properties for Simplecombobox items elements
 *
 * @export
 * @interface IComboxboxItem
 */
export interface IComboxboxItem {
  /**
   * captions to show in combobox
   *
   * @type {string}
   * @memberof IComboxboxItem
   */
  caption: string;

  /**
   * value of element
   *
   * @type {string}
   * @memberof IComboxboxItem
   */
  value: string;

  /**
   * title of element
   *
   * @type {string}
   * @memberof IComboxboxItem
   */
  title?: string;
}
