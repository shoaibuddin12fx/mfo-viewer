import { HistoryAction } from '../enums/historyactions.enum';

/**
 * interface for historyobject
 *
 * @export
 * @interface IHistoryObject
 */
export interface IHistoryObject {
  /**
   * element that was changed
   *
   * @type {*}
   * @memberof IHistoryObject
   */
  element: any;

  /**
   * action made
   *
   * @type {HistoryAction}
   * @memberof IHistoryObject
   */
  action: HistoryAction;

  /**
   * parent collection if needed (deletion of addition)
   *
   * @type {*}
   * @memberof IHistoryObject
   */
  parent?: any;

  /**
   * timestamp to reduce history noise
   *
   * @type {number}
   * @memberof IHistoryObject
   */
  timestamp?: number;

  /**
   * collection of changes made to the same object, that will be canceled with one back or forward action
   *
   * @type {Array<IPropChangeItem>}
   * @memberof IHistoryObject
   */
  changes?: Array<IPropChangeItem>;
}

/**
 * interface for popertychangeitem
 *
 * @export
 * @interface IPropChangeItem
 */
export interface IPropChangeItem {
  /**
   * propertyname that was changed
   *
   * @type {string}
   * @memberof IPropChangeItem
   */
  propertyName: string;
  /**
   * old value of given property
   *
   * @type {*}
   * @memberof IPropChangeItem
   */
  propertyValueOld: any;
  /**
   * new value provided for this property
   *
   * @type {*}
   * @memberof IPropChangeItem
   */
  propertyValueNew: any;
}
