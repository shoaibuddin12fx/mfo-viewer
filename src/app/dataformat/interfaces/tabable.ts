/**
 * Interface for tabable elements
 *
 * @export
 * @interface ITabable
 */
export interface ITabable {
  /**
   * TabIndex of element
   *
   * @type {number}
   * @memberof ITabable
   */
  TabIndex: number;
}
