/**
 *contains all mandatory properties for FormularOption elements elements
 *
 * @export
 * @interface IFormularOptions
 */
export interface IFormularOptions {
  /**
   * internal "krankenblattbezeichnung" for Medical Office as return value
   *
   * @type {string}
   * @memberof IFormularOptions
   */
  krankenblattbezeichnung: string;
}
