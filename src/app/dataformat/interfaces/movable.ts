/**
 * contains all mandatory properties for movable elements
 *
 * @export
 * @interface IMovable
 */
export interface IMovable {
  /**
   * Width of element
   *
   * @type {number}
   * @memberof IMovable
   */
  Width: number;
  /**
   * Height of element
   *
   * @type {number}
   * @memberof IMovable
   */
  Height: number;

  /**
   * X coordinate position of element
   *
   * @type {number}
   * @memberof IMovable
   */
  X: number;

  /**
   * Y coordinate position of element
   *
   * @type {number}
   * @memberof IMovable
   */
  Y: number;
}
