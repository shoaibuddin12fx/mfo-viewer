/**
 * entry of assignment array
 *
 * @interface IAssignmentEntry
 */
interface IAssignmentEntry {
  /**
   * key of assignment
   *
   * @type {string}
   * @memberof IAssignmentEntry
   */
  schluessel: string;

  /**
   * textcontent of assignment
   *
   * @type {string}
   * @memberof IAssignmentEntry
   */
  text: string;
}

/**
 * interface for api
 *
 * @export
 * @interface IAssignmentResponse
 */
export interface IAssignmentResponse {
  /**
   * result of request
   *
   * @type {boolean}
   * @memberof IAssignmentResponse
   */
  result: boolean;

  /**
   * array of assignments
   *
   * @type {Array<IAssignmentEntry>}
   * @memberof IAssignmentResponse
   */
  auftraege: Array<IAssignmentEntry>;
}
