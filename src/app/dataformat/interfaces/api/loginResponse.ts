/**
 * response for loginfunction
 *
 * @export
 * @interface ILoginResponse
 */
export interface ILoginResponse {
  /**
   * result for function
   *
   * @type {boolean}
   * @memberof ILoginResponse
   */
  result: boolean;

  /**
   * session string
   *
   * @type {string}
   * @memberof ILoginResponse
   */
  session: string;

  /**
   * target (old we will not need it)
   *
   * @type {string}
   * @memberof ILoginResponse
   */
  target: string;

  /**
   * displayed name of user in Medical Office
   *
   * @type {string}
   * @memberof ILoginResponse
   */
  username: string;
}
