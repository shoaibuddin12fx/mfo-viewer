/**
 * entry of autotextlist
 *
 * @export
 * @interface IAutotextEntry
 */
export interface IAutotextEntry {
  /**
   * kuerzel of autotext
   *
   * @type {string}
   * @memberof IAutotextEntry
   */
  kuerzel: string;

  /**
   * text to insert
   *
   * @type {string}
   * @memberof IAutotextEntry
   */
  text: string;
}

/**
 * response of API
 *
 * @export
 * @interface IAutotextListResponse
 */
export interface IAutotextListResponse {
  /**
   * result of function
   *
   * @type {boolean}
   * @memberof IAutotextListResponse
   */
  result: boolean;

  /**
   * array of autotexts
   *
   * @type {Array<IAutotextEntry>}
   * @memberof IAutotextListResponse
   */
  autotexte: Array<IAutotextEntry>;
}
