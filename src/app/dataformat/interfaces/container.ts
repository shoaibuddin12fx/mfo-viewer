import { UiBaseSelectable } from '../models/uibaseselectable';

/**
 * contains all mandatory properties for Container elements, joins IHasChildren and BasInfo into one Interface for array of elements
 *
 * @export
 * @interface IBaseInfo
 */
export interface IContainer {
  /**
   * child elements of container
   *
   * @type {Array<IContainer>}
   * @memberof IContainer
   */
  Children: Array<UiBaseSelectable>;

  /**
   * Type string of element type
   *
   * @type {string}
   * @memberof IBaseInfo
   */
  Type: string;

  /**
   * TabIndex of interactive element for keyboard navigation and cross references
   *
   * @type {number}
   * @memberof IBaseInfo
   */
  TabIndex: number;

  /**
   * Text content of element
   *
   * @type {string}
   * @memberof IContainer
   */
  Text: string;
}
