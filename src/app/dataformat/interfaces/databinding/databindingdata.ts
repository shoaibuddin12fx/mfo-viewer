/**
 * interface for an databinding entry
 *
 * @interface IDatabindingEntry
 */
interface IDatabindingEntry {
  items: Array<{ name: string; default: string }>;
  extras: Array<{ name: string; default: string }>;
}

/**
 * Interface for Databinding data
 *
 * @export
 * @interface IDatabindingdata
 */
export interface IDatabindingdata {
  AArzt: IDatabindingEntry;
  'AARZT.InternerLE': IDatabindingEntry;
  Formularkopf: IDatabindingEntry;
  Hauptversicherter: IDatabindingEntry;
  Kostenträger: IDatabindingEntry;
  'Kostenträger.BG': IDatabindingEntry;
  'Kostenträger.GKV': IDatabindingEntry;
  'Kostenträger.eGK': IDatabindingEntry;
  'Kostenträger.Überweiser': IDatabindingEntry;
  Patient: IDatabindingEntry;
  'Patient.Arbeitgeber': IDatabindingEntry;
  'Patient.Bezugsperson': IDatabindingEntry;
  'Patient.Bezugsperson2': IDatabindingEntry;
  'Patient.Bezugsperson3': IDatabindingEntry;
  'Patient.Bezugsperson4': IDatabindingEntry;
  'Patient.Hausarzt': IDatabindingEntry;
  'Patient.Hausarzt2': IDatabindingEntry;
  'Patient.Stammdaten': IDatabindingEntry;
  Praxis: IDatabindingEntry;
  RegionalKasse: IDatabindingEntry;
  Stempel: IDatabindingEntry;
}
