/**
 * Interface for interaktive components with context menu entries
 *
 * @export
 * @interface IInteractiveContextMenu
 */
export interface IInteractiveContextMenu {
  /**
   * should return an array of possible context menu entries to display on right click context menu
   *
   * @return {*}  {string[]}
   * @memberof IInteractiveContextMenu
   */
  getItemLabel(): string[];

  /**
   * Is invoked with the index of the clicked item index from getItemLabel() index
   *
   * @param {number} idx
   * @memberof IInteractiveContextMenu
   */
  clickedItem(idx: number): void;
}
