/**
 * Interface for Data Value stored by Medical Office (Datapackage belonging to a specific formular)
 *
 * @export
 * @interface IDataValue
 */
export interface IDataValue {
  /**
   * path for value
   *
   * @type {string}
   * @memberof IDataValue
   */
  path: string;

  /**
   * property to set the value to, default should be treated "Text"
   *
   * @type {string}
   * @memberof IDataValue
   */
  propertyName?: string;

  /**
   * data of object
   *
   * @type {string}
   * @memberof IDataValue
   */
  value: any;
}
