/**
 * contains all mandatory properties for BackgroundInfo elements
 *
 * @export
 * @interface IBackgroundInfo
 */
export interface IBackgroundInfo {
  /**
   * Backgroundcolor of element
   *
   * @type {string}
   * @memberof IBackgroundInfo
   */
  Color: string;
}
