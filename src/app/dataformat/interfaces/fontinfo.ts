/**
 * interface for components that need font info
 *
 * @export
 * @interface IFontInfo
 */
export interface IFontInfo {
  /**
   * Text alignment
   *
   * @type {string}
   * @memberof IFontInfo
   */
  Align: string;

  /**
   * FontFamily setting (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof IFontInfo
   */
  FontFamily: string;

  /**
   * FontCharset setting (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof IFontInfo
   */
  FontCharset: string;

  /**
   * Font color (Implemented from FontInfo)
   *
   * @type {string}
   * @memberof IFontInfo
   */
  FontColor: string;

  /**
   * Font Size (Implemented from FontInfo)
   *
   * @type {number}
   * @memberof IFontInfo
   */
  FontSize: number;

  /**
   * text underlined boolean
   *
   * @type {boolean}
   * @memberof IFontInfo
   */
  FontStyleUnderline: boolean;

  /**
   * text strikedout boolean
   *
   * @type {boolean}
   * @memberof IFontInfo
   */
  FontStyleStrikeOut: boolean;

  /**
   * text bold boolean
   *
   * @type {boolean}
   * @memberof IFontInfo
   */
  FontStyleBold: boolean;

  /**
   * italic font style
   *
   * @type {boolean}
   * @memberof IFontInfo
   */
  FontStyleItalic: boolean;
}
