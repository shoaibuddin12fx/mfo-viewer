/**
 * contains all mandatory properties for BorderInfo elements
 *
 * @export
 * @interface IBorderInfo
 */
export interface IBorderInfo {
  /**
   * BorderColor of element
   *
   * @type {string}
   * @memberof IBorderInfo
   */
  BorderColor: string;

  /**
   * Border width in px for element
   *
   * @type {number}
   * @memberof IBorderInfo
   */
  BorderWidth: number;
}
