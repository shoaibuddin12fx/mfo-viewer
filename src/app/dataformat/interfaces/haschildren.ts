import { Container } from '../models';
import { UiBaseSelectable } from '../models/uibaseselectable';
import { IBaseInfo } from './baseinfo';

/**
 * contains all mandatory properties for elements with child elements
 *
 * @export
 * @interface IHasChildren
 */
export interface IHasChildren {
  /**
   * array of child elements
   *
   * @type {Array<UiBaseSelectable>}
   * @memberof IHasChildren
   */
  Children: Array<UiBaseSelectable>;
}
