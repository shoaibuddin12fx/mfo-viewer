import { IDataValue } from './datavalue';

/**
 * interface for datavalue collection
 *
 * @export
 * @interface IDataPackage
 */
export interface IDataPackage {
  /**
   * collection of data values belonging to a MFO form
   *
   * @type {IDataValue[]}
   * @memberof IDataPackage
   */
  data: IDataValue[];
}
