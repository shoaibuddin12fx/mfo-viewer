/**
 * contains all mandatory properties for printable elements
 *
 * @export
 * @interface IPrintable
 */
export interface IPrintable {
  /**
   * determines that an element is printable
   *
   * @type {boolean}
   * @memberof IPrintable
   */
  Print: boolean;
}
