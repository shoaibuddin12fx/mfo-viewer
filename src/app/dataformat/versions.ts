/**
 * Pascal Version info
 *
 * @export
 * @enum {number}
 */
export enum Versions {
  oldPascal = 1,
  newPascal = 2,
  v3 = 3,
}
