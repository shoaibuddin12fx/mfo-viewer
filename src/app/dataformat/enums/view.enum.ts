/**
 * View type
 *
 * @export
 * @enum {number}
 */
export enum View {
  Tabs,
  Flow,
}
