/**
 * history actions
 *
 * @export
 * @enum {number}
 */
export enum HistoryAction {
  Add,
  Remove,
  PropertyChange,
  PathChange,
}
