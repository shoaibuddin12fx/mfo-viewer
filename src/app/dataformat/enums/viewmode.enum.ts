/**
 * View Modes for Editor
 *
 * @export
 * @enum {number}
 */
export enum ViewMode {
  Edit,
  Interactive,
  Print,
  Preview,
}
