import { Component, HostListener } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  /**
   * Creates an instance of AppComponent.
   * @param {Platform} platform
   * @param {SplashScreen} splashScreen
   * @param {StatusBar} statusBar
   * @memberof AppComponent
   */
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  /**
   * initializes the app
   *
   * @memberof AppComponent
   */
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /**
   * eventhandler for mousewheel events
   *
   * @param {*} event
   * @memberof AppComponent
   */
  @HostListener('wheel', ['$event'])
  wheelListener(event) {
    if (event.ctrlKey) {
      let zoomEvent;
      event.preventDefault();

      // ToDo: define delta values
      zoomEvent = new CustomEvent('mfo:zoom', {
        detail: {
          direction: event.deltaY < 0 ? 'in' : 'out',
        },
      });
      window.dispatchEvent(zoomEvent);
    }
  }
}
