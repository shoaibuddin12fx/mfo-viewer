import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAutotextListResponse } from '@interfaces/api/autotextResponse';
import { ILoginResponse } from '@interfaces/api/loginResponse';
import { Utils } from '../dataformat/util';
import { Globals } from '@services/global.service';
import { MFO } from '../dataformat/models/mfo.model';
import { IDataPackage } from '@interfaces/datapackage';

/**
 * API Service class to handle API calls to MO-Server
 *
 * @export
 * @class ApiService
 */
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // TODO get URL from chromium
  /**
   * api URL
   *
   * @memberof ApiService
   */
  public apiUrl = 'http://localhost/mo/api';

  /**
   * keeps session string
   *
   * @memberof ApiService
   */
  public session = '';

  /**
   * Creates an instance of ApiService.
   * @param {HttpClient} http
   * @memberof ApiService
   */
  constructor(private http: HttpClient) {
    this.demoLogin();
  }

  /**
   * calls api with url and data
   *
   * @private
   * @param {string} target
   * @param {*} data
   * @returns {Promise<any>}
   * @memberof ApiService
   */
  private apiCall(target: string, data: any): Promise<any> {
    // TODO add real session!
    data.session = this.session;
    const postdata = JSON.stringify(data);

    return this.http.post(this.apiUrl + '/' + target, postdata).toPromise();
  }

  /**
   * calls login function with hardcoded username and password
   *
   * @private
   * @memberof ApiService
   */
  private demoLogin() {
    const user = 'admin'; // mo username
    const pass = '21232f297a57a5a743894a0e4a801fc3'; // password as md5

    this.doLogin(user, pass);
  }

  /**
   * sets needed session for API
   *
   * @param {string} username
   * @param {string} password
   * @memberof ApiService
   */
  public async doLogin(username: string, password: string) {
    const data = {
      user: username,
      pass: password, //! Attention! if user input MD5 is needed here!
    };

    try {
      const loginresp: ILoginResponse = await this.apiCall('login', data);
      Utils.logPrefixed('API-Service', 'doLogin response', loginresp);
      this.session = loginresp.session;
    } catch (error) {
      Utils.failPrefixed('API-Service', 'doLogin catched err', error);
      // TODO errorhandler
    }
  }

  /**
   * sets autotexts in globals
   *
   * @memberof ApiService
   */
  public async getAutoTextList() {
    try {
      const autotextResponse: IAutotextListResponse = await this.getAutoText(
        ''
      );
      Utils.logPrefixed('API-Service', 'getAutoTextList', autotextResponse);
      Globals.autotexts = autotextResponse.autotexte;
    } catch (error) {
      Utils.failPrefixed('API-Service', 'getAutoTextList catched err', error);
      // TODO errorhandler
    }
  }

  /**
   * gets autotext for search
   *
   * @param {string} search
   * @returns
   * @memberof ApiService
   */
  public async getAutoText(search: string) {
    const data = { search };
    return await this.apiCall('system/autotext_get', data);
  }

  /**
   * gets assignment for search
   *
   * @param {string} search
   * @returns
   * @memberof ApiService
   */
  public async getAssignment(search: string) {
    const data = { search };
    return await this.apiCall('system/auftrag_get', data);
  }

  // TODO clarify params
  public async print() {
    // system
    // TODO clarify function
  }

  public async saveMFO(mfo: MFO) {
    // system
    const data = { mfo };

    try {
      const saveMFOResponse = await this.apiCall('system/mfo_save', data);
      Utils.logPrefixed('API-Service', 'saveMFO response', saveMFOResponse);
    } catch (error) {
      Utils.failPrefixed('API-Service', 'saveMFO catched err', error);
      // TODO errorhandler
    }
  }

  public async saveSignedMFO(
    mfosurogat: number,
    patid: number,
    dataPackage: IDataPackage
  ) {
    // patient
  }

  public async loadMFO(mfosurogat: number) {
    // system
  }

  public async loadMFOPatientData(id: number, patid: number) {
    // patient
  }

  public async convertContent(content: string, patid: number) {
    // wa_eval_variables
  }

  // ! JUST NOTES
  // ### variablen auflösen ###
  // -> feld für feld
  // -> überprüfen was enthalten ist
  // -> reihenfolge muss beachtet werden
  // uebergabe: typ (php datenbindung usw), inhalt

  // ### Drucken braucht nur ne API ###
  // -> generell drüber sprechen was dort getan werden muss

  // ### auftragssuche ###
  // -> was genau soll die tun?

  // ### speichern des formulars an sich ###
  // -> uebergabe: mfo gepakot

  // ### speichern patienten bezogen ###
  // -> daten aufdröseln, felder whitelisten mit nur daten

  // ### laden roh + mit patientendaten ###
  // !
}
