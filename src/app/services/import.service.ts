import { Injectable } from '@angular/core';
import { Import } from '../dataformat/import';
import { FontService } from './font.service';

/**
 * Import/Load service for MFO files
 *
 * @export
 * @class ImportService
 */
@Injectable({
  providedIn: 'root',
})
export class ImportService {
  /**
   * Import instance for importing MFO files
   *
   * @type {Import}
   * @memberof ImportService
   */
  public from: Import;

  /**
   * Creates an instance of ImportService.
   * @param {FontService} fontService
   * @memberof ImportService
   */
  constructor(private fontService: FontService) {
    this.from = new Import(this.fontService);
  }
}
