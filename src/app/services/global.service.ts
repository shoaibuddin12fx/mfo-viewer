import { IAutotextEntry } from '@interfaces/api/autotextResponse';

/**
 * global class for variables
 *
 * @export
 * @class Globals
 */
export class Globals {
  /**
   * list of all autotexts
   *
   * @static
   * @type {Array<IAutotextEntry>}
   * @memberof Globals
   */
  public static autotexts: Array<IAutotextEntry>;
}
