import { Injectable } from '@angular/core';
import { IHistoryObject, IPropChangeItem } from '@interfaces/historyobject';
import { HistoryAction } from '../dataformat/enums/historyactions.enum';
import * as objectPath from 'object-path';
import { MFO } from '../dataformat/models/mfo.model';
import { Utils } from '../dataformat/util';

@Injectable({
  providedIn: 'root',
})
export class HistoryService {
  /**
   * mfo object
   *
   * @private
   * @type {MFO}
   * @memberof HistoryService
   */
  private mfo: MFO;

  private readonly CANVASELEMENTS: string[] = [
    'signfield',
    'markfield',
    'painting',
  ];

  /**
   * array of undos
   *
   * @type {Array<IHistoryObject>}
   * @memberof HistoryService
   */
  public undos: Array<IHistoryObject> = [];

  /**
   * array of redos
   *
   * @type {Array<IHistoryObject>}
   * @memberof HistoryService
   */
  public redos: Array<IHistoryObject> = [];

  /**
   * checks if element is a page
   *
   * @private
   * @param {*} element
   * @returns {boolean}
   * @memberof HistoryService
   */
  private isPage(element): boolean {
    if (element.Type !== 'Container') {
      return false;
    }

    const pathPageNbrOnly = element.Path.replace('Pages.', '');
    return !isNaN(pathPageNbrOnly);
  }

  /**
   * returns element position
   *
   * @private
   * @param {string} path
   * @returns
   * @memberof HistoryService
   */
  private getChildrenPos(path: string) {
    const splits = path.split('.');

    if (path.includes('Children')) {
      return splits[splits.length - 1];
    } else {
      Utils.warnPrefixed('HistoryService', 'No Children given in path!');
    }
  }

  /**
   * sets mfo
   *
   * @param {*} mfo
   * @memberof HistoryService
   */
  public setMfo(mfo) {
    this.mfo = mfo;
  }

  /**
   * adds a new undo
   *
   * @param {IHistoryObject} entry
   * @memberof HistoryService
   */
  public addUndo(entry: IHistoryObject) {
    this.undos.push(entry);
  }

  /**
   * undos last action of undos
   *
   * @returns
   * @memberof HistoryService
   */
  public undo() {
    // if no undos return
    if (this.undos.length === 0) {
      return;
    }

    // get last item of undos and remove it from undos
    // and push it to the redos
    const lastItem = this.undos.pop();
    this.redos.push(lastItem);

    // if item was added remove it
    if (lastItem.action === HistoryAction['Add']) {
      objectPath.del(this.mfo, lastItem.element.Path);
    }

    // if item was removed add it
    if (lastItem.action === HistoryAction['Remove']) {
      if (lastItem.parent) {
        const pos = this.getChildrenPos(lastItem.element.Path);
        lastItem.parent.splice(pos, 0, lastItem.element);

        this.mfo.activeElement = lastItem.element;
        return;
      }

      // if page
      if (this.isPage(lastItem.element)) {
        // remove IsActive from page
        lastItem.element.IsActive = false;

        // get pageIndex
        const pageIndex = lastItem.element.Path.replace('Pages.', '');

        // insert in pages
        this.mfo.Pages.splice(pageIndex, 0, lastItem.element);
        return;
      }
    }

    // property change
    if (lastItem.action === HistoryAction['PropertyChange']) {
      if (lastItem.changes) {
        for (const change of lastItem.changes) {
          lastItem.element[change.propertyName] = change.propertyValueOld;
          if (
            this.CANVASELEMENTS.includes(lastItem.element.Type.toLowerCase())
          ) {
            window.dispatchEvent(
              new CustomEvent('dataLoaded', {
                bubbles: true,
                detail: { path: lastItem.element.Path },
              })
            );
          }
        }
      }
    }

    // path change
    if (lastItem.action === HistoryAction['PathChange']) {
      const oldPath = lastItem.changes[0].propertyValueOld;
      const newPath = lastItem.changes[0].propertyValueNew;

      // remove element from new path
      const newPos = this.getChildrenPos(newPath);
      lastItem.parent.splice(newPos, 1);

      // add element at oldpos
      const oldPos = this.getChildrenPos(oldPath);
      lastItem.parent.splice(oldPos, 0, lastItem.element);
    }
  }

  /**
   * redos last action of redos
   *
   * @returns
   * @memberof HistoryService
   */
  public redo() {
    // if no redos return
    if (this.redos.length === 0) {
      return;
    }

    // get last item of redos and remove it from redos
    // and push it to the undos
    const lastItem = this.redos.pop();
    this.undos.push(lastItem);

    // if item was added (removed by undo), readd it
    if (lastItem.action === HistoryAction['Add']) {
      if (lastItem.parent) {
        const pos = this.getChildrenPos(lastItem.element.Path);
        lastItem.parent.splice(pos, 0, lastItem.element);
        return;
      }

      // if page
      if (this.isPage(lastItem.element)) {
        // remove IsActive from page
        lastItem.element.IsActive = false;

        // get pageIndex
        const pageIndex = lastItem.element.Path.replace('Pages.', '');

        // insert in pages
        this.mfo.Pages.splice(pageIndex, 0, lastItem.element);
      }
    }

    // if item was removed (added by undo), remove it
    if (lastItem.action === HistoryAction['Remove']) {
      objectPath.del(this.mfo, lastItem.element.Path);
    }

    // if property change (set back by undo), set back to "newval"
    if (lastItem.action === HistoryAction['PropertyChange']) {
      if (lastItem.changes) {
        for (const change of lastItem.changes) {
          lastItem.element[change.propertyName] = change.propertyValueNew;
          if (
            this.CANVASELEMENTS.includes(lastItem.element.Type.toLowerCase())
          ) {
            window.dispatchEvent(
              new CustomEvent('dataLoaded', {
                bubbles: true,
                detail: { path: lastItem.element.Path, popLast: true },
              })
            );
          }
        }
      }
    }

    // path change (set back by undo), set back
    if (lastItem.action === HistoryAction['PathChange']) {
      const oldPath = lastItem.changes[0].propertyValueNew;
      const newPath = lastItem.changes[0].propertyValueOld;

      // remove element from new path
      const newPos = this.getChildrenPos(newPath);
      lastItem.parent.splice(newPos, 1);

      // add element at oldpos
      const oldPos = this.getChildrenPos(oldPath);
      lastItem.parent.splice(oldPos, 0, lastItem.element);
    }
  }

  /**
   * propertyChange for elements
   *
   * @param {*} element
   * @param {string} propertyName
   * @param {*} propertyValueOld
   * @param {*} propertyValueNew
   * @memberof HistoryService
   */
  public addPropertyChange(element: any, changes: Array<IPropChangeItem>) {
    // get all undos for the last second
    let toCheckUndos = this.undos.filter((undo) => {
      return (
        undo.changes &&
        undo.changes.length === changes.length &&
        undo.timestamp &&
        undo.timestamp > Date.now() - 1000
      );
    });

    let found = false;

    // each new given change
    changes.forEach((change) => {
      // to check undos for last second
      toCheckUndos.forEach((undo, checkundoIndex) => {
        // last second for each change
        undo.changes.forEach((undochange, changeIndex) => {
          // if property already changed in last second just update value
          if (undochange.propertyName === change.propertyName) {
            found = true;
            toCheckUndos[checkundoIndex].changes[changeIndex].propertyValueNew =
              change.propertyValueNew;
            toCheckUndos[checkundoIndex].timestamp = Date.now();
          }
        });
      });
    });

    // if not found add new undo
    if (!found) {
      this.addUndo({
        action: HistoryAction['PropertyChange'],
        element,
        changes,
        timestamp: Date.now(),
      });
    }
  }

  /**
   * on change for to background and foreground
   *
   * @param {*} element
   * @param {string} pathOld
   * @param {string} pathNew
   * @param {*} parent
   * @memberof HistoryService
   */
  public addPathChange(
    element: any,
    pathOld: string,
    pathNew: string,
    parent: any
  ) {
    this.addUndo({
      action: HistoryAction['PathChange'],
      element,
      parent,
      changes: [
        {
          propertyValueNew: pathNew,
          propertyValueOld: pathOld,
          propertyName: 'Path',
        },
      ],
    });
  }
}
