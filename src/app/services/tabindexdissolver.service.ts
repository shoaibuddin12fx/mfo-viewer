import { Injectable } from '@angular/core';
import { IContainer } from '@interfaces/container';
import { ITabable } from '@interfaces/tabable';
import { MFO } from '../dataformat/models/mfo.model';
import { Utils } from '../dataformat/util';

/**
 * TabIndex dissolve service class
 *
 * @export
 * @class TabIndexDissolverService
 */
@Injectable({
  providedIn: 'root',
})
export class TabIndexDissolverService {
  /**
   * mfo object
   *
   * @private
   * @type {MFO}
   * @memberof TabIndexDissolverService
   */
  private MFO: MFO;

  /**
   * collection of all controls containing a tab index
   *
   * @private
   * @type {Container[]}
   * @memberof TabIndexDissolverService
   */
  private tabControls: any[];

  /**
   * next possible tabindex
   *
   * @memberof TabIndexDissolverService
   */
  public nextTabIndex = 0;

  /**
   * Creates an instance of TabIndexDissolverService.
   * @memberof TabIndexDissolverService
   */
  constructor() {}

  /**
   * sets actual MFO object for caching tabindex controls
   *
   * @param {MFO} mfo
   * @memberof TabIndexDissolverService
   */
  public setMfo(mfo: MFO) {
    this.MFO = mfo;
    this.updateTIfromObjects();
  }

  /**
   * update tabindex controls as cached element reference
   *
   * @memberof TabIndexDissolverService
   */
  public updateTIfromObjects() {
    this.tabControls = [];
    this.nextTabIndex = 0;
    this.getTIObjects(this.MFO.Pages);
    Utils.log('resolvedObjectArray', this.tabControls);
  }

  /**
   * fetch all elements with given tabindex, recoursive
   *
   * @private
   * @param {Container[]} container
   * @return void
   * @memberof TabIndexDissolverService
   */
  private getTIObjects(container: any[]) {
    if (container == undefined || container == null) return;
    for (let i = 0; i < container.length; i++) {
      let child = container[i];

      const c: any = child as ITabable;
      if (c.TabIndex > 0) {
        this.setNewTabControl(c);
      }

      this.getTIObjects(child.Children);
    }
  }

  /**
   * intialises the refresh of all MFO Pages
   *
   * @memberof TabIndexDissolverService
   */
  public resolveAllTabIndexes() {
    this.resolveAllTIs(this.MFO.Pages);
  }

  /**
   * resolves all tabindex references within a given container array
   *
   * @private
   * @param {Container[]} container
   * @memberof TabIndexDissolverService
   */
  private resolveAllTIs(container: any[]) {
    container.forEach((c) => {
      if (
        !(c.Type == 'Input' || c.Type == 'Text' || c.Type == 'Barcode') &&
        Utils.isContainer(c)
      )
        this.resolveAllTIs(c.Children);
      else this.resolveTIValue(c);
    });
  }

  /**
   * resolves a single container and replaces the pattern "#(index number);" with the propriate tabindex value
   *
   * @param {Container} c
   * @return void
   * @memberof TabIndexDissolverService
   */
  public resolveTIValue(c: any) {
    // nothing todo if no text
    if (c.Text == undefined || c.Text == null) return;

    // get referenced tabindexes
    let tiArray = c.Text.toString().match(/#([0-9]+)\;/g);

    // return if no tabindexes referenced
    if (tiArray == undefined || tiArray.length <= 0) return;
    // cleanup tis and replace with value from cached values
    for (let i = 0; i < tiArray.length; i++) {
      console.log(this.tabControls[tiArray[i]]);

      let val =
        this.tabControls[tiArray[i]].Text ??
        this.tabControls[tiArray[i]].getValue();

      // let val = this.tabControls[tiArray[i]].Text ?? '';
      // if (this.tabControls[tiArray[i]].Type == 'CheckBox') {
      //   val = this.tabControls[tiArray[i]].getValue();
      // }

      c.Text = c.Text.replace(tiArray[i], val);
    }
  }

  /**
   * sets next tabindex for given container
   *
   * @param {*} container
   * @memberof TabIndexDissolverService
   */
  public setNextTabIndex(container: ITabable) {
    let maxTab = this.nextTabIndex;
    container.TabIndex = maxTab;
    this.setNewTabControl(container);
  }

  /**
   * adds a new Control to cached collection, increases next TabIndex
   *
   * @private
   * @param {Container} control
   * @memberof TabIndexDissolverService
   */
  private setNewTabControl(control: ITabable) {
    this.tabControls['#' + control.TabIndex.toString() + ';'] = control;

    if (control.TabIndex >= this.nextTabIndex) {
      this.nextTabIndex = control.TabIndex + 1;
    }
  }
}
