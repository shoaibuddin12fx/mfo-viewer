import { Injectable } from '@angular/core';
import { IDatabindingdata } from '@interfaces/databinding/databindingdata';

@Injectable({
  providedIn: 'root',
})
export class DatabindingService {
  public data: IDatabindingdata;

  /**
   * Creates an instance of DatabindingService.
   * @memberof DatabindingService
   */
  constructor() {
    if (!this.data) {
      fetch('/assets/json/databinding.json')
        .then((resp) => resp.json())
        .then((data: IDatabindingdata) => {
          const dataObj = data;
          const keys = Object.keys(dataObj);

          keys.forEach((key) => {
            dataObj[key].items = dataObj[key].items.sort((a, b) =>
              a.name > b.name ? 1 : b.name > a.name ? -1 : 0
            );

            if (dataObj[key].extras) {
              dataObj[key].extras = dataObj[key].extras.sort((a, b) =>
                a.name > b.name ? 1 : b.name > a.name ? -1 : 0
              );
            }
          });

          this.data = dataObj;
        });
    }
  }

  /**
   * returns an array with the databinding topics in a nested structure
   *
   * @returns
   * @memberof DatabindingService
   */
  public getTopicStructure() {
    const topicStructure = [];
    let lastTopic;
    Object.keys(this.data).forEach((topic) => {
      const newTopic = { name: topic, items: [], children: [] };
      newTopic.items = [...this.data[topic].items.map((a) => a.name)];
      if (this.data[topic].extras) {
        newTopic.items = [
          ...newTopic.items,
          ...this.data[topic].extras.map((a) => a.name),
        ];
      }
      if (topic.includes('.') && topic.startsWith(lastTopic.name)) {
        newTopic.name = newTopic.name.split('.')[1];
        lastTopic.children.push(newTopic);
      } else {
        topicStructure.push(newTopic);
        lastTopic = topicStructure[topicStructure.length - 1];
      }
    });
    return topicStructure;
  }
}
