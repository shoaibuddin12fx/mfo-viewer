import { Injectable } from '@angular/core';
import { MFO } from '../dataformat/models/mfo.model';
import * as pako from 'pako';
import * as jspdf from 'jspdf';
import domtoimage from 'dom-to-image';
import { Utils } from '../dataformat/util';

/**
 * Export service for MFO Files
 *
 * @export
 * @class ExportService
 */
@Injectable({
  providedIn: 'root',
})
export class ExportService {
  /**
   * Creates an instance of ExportService.
   * @memberof ExportService
   */
  constructor() {}

  /**
   * export to MFW format
   *
   * @param {MFO} mfo
   * @memberof ExportService
   */
  public toMFW(mfo: MFO) {
    const a = document.createElement('a');
    const mfoCopy = mfo.getDenoisedAnonymousCopy();
    const data = pako.deflate(JSON.stringify(mfoCopy));

    const file = new Blob([data], { type: 'text/plain' });
    a.href = URL.createObjectURL(file);
    a.download = 'export.MFW';
    a.click();
  }

  /**
   * export to object format
   *
   * @param {Object} object
   * @memberof ExportService
   */
  public toObject(object: any) {
    const a = document.createElement('a');
    const objCopy = Utils.denoiseElement(object);
    const data = pako.deflate(JSON.stringify(objCopy));
    const file = new Blob([data], { type: 'text/plain' });
    a.href = URL.createObjectURL(file);
    a.download = object['Container'].Type + '.obj';
    a.click();
  }

  /**
   * save the form as pdf by screenshotting every page
   *
   * @param {NodeListOf<HTMLElement>} pages
   * @param {string} name
   * @returns
   * @memberof ExportService
   */
  public async toPDF(pages: NodeListOf<HTMLElement>, name: string) {
    if (!pages) {
      return;
    }
    const pdf = new jspdf.jsPDF('p', 'px', 'a4');
    const options = {
      height: 0,
      width: 0,
      style: { margin: '0' },
    };
    let activePage: boolean;

    for (let i = 0; i < pages.length; i++) {
      options.height = pages[i].offsetHeight;
      options.width = pages[i].offsetWidth;

      if (pages[i].style.display === 'none') {
        activePage = false;
        pages[i].style.display = 'block';
      } else {
        activePage = true;
      }
      await domtoimage
        .toPng(pages[i], options)
        .then((dataURL) => {
          if (dataURL) {
            if (i > 0) {
              pdf.addPage('a4', 'p');
              pdf.setPage(i + 1);
            }
            pdf.addImage(dataURL, 'PNG', 0, 0, 0, 0);
          }
        })
        .catch((res) => {
          Utils.log(res);
          return res;
        });
      if (!activePage) {
        pages[i].style.display = 'none';
      }
    }
    pdf.save(name + '.pdf');
  }
}
