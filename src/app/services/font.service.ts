import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FontService {
  /**
   * Array of all available fonts in MFO Editor
   *
   * @memberof FontService
   */
  public FontMapping = [
    {
      Family: 'Arial',
      Pattern: /Arial(?! Black).*/i,
      File: 'arial',
      default: false
    },
    {
      Family: 'Arial Black',
      Pattern: /Arial Black/i,
      File: 'ariblk',
      default: false
    },
    {
      Family: 'Bahnschrift',
      Pattern: /Bahnschrift*/i,
      File: 'bahnschrift',
      default: false
    },
    {
      Family: 'Calibri',
      Pattern: /Calibri*/i,
      File: 'calibri',
      default: false
    },
    {
      Family: 'Cambria',
      Pattern: /Cambria*/i,
      File: 'cambria',
      default: false
    },
    {
      Family: 'Candara',
      Pattern: /Candara*/i,
      File: 'candara',
      default: false
    },
    {
      Family: 'Comic Sans MS',
      Pattern: /Comic Sans*/i,
      File: 'comic',
      default: false
    },
    {
      Family: 'Consolas',
      Pattern: /Consolas*/i,
      File: 'consola',
      default: false
    },
    {
      Family: 'Constantiana',
      Pattern: /Constantiana*/i,
      File: 'constan',
      default: false
    },
    {
      Family: 'Corbel',
      Pattern: /Corbel*/i,
      File: 'corbel',
      default: false
    },
    {
      Family: 'Courier New',
      Pattern: /Courier New*/i,
      File: 'cour',
      default: false
    },
    {
      Family: 'Ebrima',
      Pattern: /Ebrima*/i,
      File: 'ebrima',
      default: false
    },
    {
      Family: 'Franklin Gothic Medium',
      Pattern: /Franklin Gothic Medium*/i,
      File: 'framd',
      default: false
    },
    {
      Family: 'Gabriola',
      Pattern: /Gabriola*/i,
      File: 'gabriola',
      default: false
    },
    {
      Family: 'Gadugi',
      Pattern: /Gadugi*/i,
      File: 'gadugi',
      default: false
    },
    {
      Family: 'Georgia',
      Pattern: /Georgia*/i,
      File: 'georgia',
      default: false
    },
    {
      Family: 'Impact',
      Pattern: /Impact*/i,
      File: 'impact',
      default: false
    },
    {
      Family: 'Ink Free',
      Pattern: /Ink Free*/i,
      File: 'inkfree',
      default: false
    },
    {
      Family: 'Javanese Text',
      Pattern: /Javanese Text*/i,
      File: 'javatext',
      default: false
    },
    {
      Family: 'Lucida Sans Unicode',
      Pattern: /Lucida Sans Unicode*/i,
      File: 'l_10646',
      default: false
    },
    {
      Family: 'Lucida Console',
      Pattern: /Lucida Console*/i,
      File: 'lucon',
      default: false
    },
    {
      Family: 'Microsoft Sans Serif',
      Pattern: /Microsoft Sans Serif*/i,
      File: 'micross',
      default: false
    },
    {
      Family: 'Palatino Linotype',
      Pattern: /Palatino Linotype*/i,
      File: 'pala',
      default: false
    },
    {
      Family: 'Segoe MDL2 Assets',
      Pattern: /Segoe MDL2 Assets*/i,
      File: 'segmdl2',
      default: false
    },
    {
      Family: 'Segoe Print',
      Pattern: /Segoe Print*/i,
      File: 'segoepr',
      default: false
    },
    {
      Family: 'Segoe UI',
      Pattern: /Segoe UI$/i,
      File: 'segoeui',
      default: false
    },
    {
      Family: 'Segoe UI Black',
      Pattern: /Segoe UI Black*/i,
      File: 'seguibl',
      default: false
    },
    {
      Family: 'Segoe UI Emoji',
      Pattern: /Segoe UI Emoji*/i,
      File: 'seguiemj',
      default: false
    },
    {
      Family: 'Segoe UI Historic',
      Pattern: /Segoe UI Historic*/i,
      File: 'seguihis',
      default: false
    },
    {
      Family: 'Segoe UI Light',
      Pattern: /Segoe UI Light*/i,
      File: 'seguili',
      default: false
    },
    {
      Family: 'Segoe UI Semibold',
      Pattern: /Segoe UI Semibold*/i,
      File: 'seguisb',
      default: false
    },
    {
      Family: 'Segoe UI Semilight',
      Pattern: /Segoe UI Semilight*/i,
      File: 'seguisli',
      default: false
    },
    {
      Family: 'Segoe UI Symbol',
      Pattern: /Segoe UI Symbol*/i,
      File: 'seguisym',
      default: false
    },
    {
      Family: 'Sitka Small',
      Pattern: /Sitka Small*/i,
      File: 'sitka',
      default: false
    },
    {
      Family: 'Sylfaen',
      Pattern: /Sylfaen*/i,
      File: 'sylfaen',
      default: false
    },
    {
      Family: 'Symbol',
      Pattern: /Symbol*/i,
      File: 'symbol',
      default: false
    },
    {
      Family: 'Tahoma',
      Pattern: /Tahoma*/i,
      File: 'tahoma',
      default: true
    },
    {
      Family: 'Times New Roman',
      Pattern: /Times New Roman*/i,
      File: 'times',
      default: false
    },
    {
      Family: 'Trebuchet MS',
      Pattern: /Trebuchet MS*/i,
      File: 'trebucbd',
      default: false
    },
    {
      Family: 'Verdana',
      Pattern: /Verdana*/i,
      File: 'verdana',
      default: false
    },
    {
      Family: 'Webdings',
      Pattern: /Webdings*/i,
      File: 'webdings',
      default: false
    },
    {
      Family: 'Wingdings',
      Pattern: /Wingdings*/i,
      File: 'wingding',
      default: false
    },
  ];

  /**
   * Creates an instance of FontService.
   * @memberof FontService
   */
  constructor() {}

  /**
   * Find Font Info by Family Name
   *
   * @static
   * @param {*} family
   * @return {*}
   * @memberof FontService
   */
  getFontInfoByFamily(family) {
    return this.FontMapping.find((f) => f.Pattern.test(family));
  }

  /**
   * Fetch System Font Files from the Assets directory
   *
   * @static
   * @param {*} font
   * @return {*}
   * @memberof FontService
   */
  async fetchFontFile(font) {
    if (!font) {
      return;
    }

    if (font.File.startsWith('data:')) {
      return;
    }
    let blob = await (await fetch(`/assets/fonts/${font.File}.woff2`)).blob();

    let dataUrl = await new Promise((resolve) => {
      let reader = new FileReader();
      reader.onload = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });

    font.File = dataUrl;

    return font;
  }

  /**
   * Obtain Font by Family Name
   *
   * @static
   * @param {*} family
   * @return {*}
   * @memberof FontService
   */
  async getFont(family: string) {
    return await this.fetchFontFile(this.getFontInfoByFamily(family));
  }

  async getDefaultFont(){
    let font = this.FontMapping.filter( x => x.default == true );
    if(font.length > 0){
      return font[0];
    }

    return this.FontMapping[0];

  }

  async setDefaultFont(family: string){
    console.log(family);
    let fonts = this.FontMapping.map( x => {
      x.default = x.Family == family;
      return x; 
    });

    this.FontMapping = fonts;
  }
}
