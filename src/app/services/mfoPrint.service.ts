import { Injectable } from '@angular/core';
import { MFO } from '../dataformat/models/mfo.model';
import { Utils } from '../dataformat/util';

@Injectable({
  providedIn: 'root',
})
export class MfoPrint {
  /**
   *
   * duplicate of MFO Object for print mode
   * @type {MFO}
   * @memberof PrintMfo
   */
  public mfo: MFO;

  /**
   * set MFO object for print mode
   *
   * @param {MFO} mfo
   * @memberof MfoPrint
   */
  public setMfo(mfo: string) {
    this.mfo = JSON.parse(mfo);
    // json object -> type conversion, map corresponding json types to MFO object types
    let ps = [];
    for (let i = 0; i < this.mfo.Pages.length; i++) {
      ps[i] = Utils.objectFromMfoInterface(this.mfo.Pages[i]);
    }
    this.mfo.Pages = ps;
    Utils.logPrefixed('MFO PRINT - mfo', this.mfo);
  }
}
