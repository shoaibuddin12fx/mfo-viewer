import { Injectable } from '@angular/core';
import { IComboxboxItem } from '@interfaces/comboxboxitem';
import * as dayjs from 'dayjs';
import * as de from 'dayjs/locale/de';
import * as customParseFormat from 'dayjs/plugin/customParseFormat';
import datepicker from 'js-datepicker';

/**
 * dateformatting service
 *
 * @export
 * @class DateformaterService
 */
@Injectable({
  providedIn: 'root',
})
export class DateformaterService {
  /**
   * all examples of the combobox
   *
   * @type {Array<IComboxboxItem>}
   * @memberof DateformaterService
   */
  public itemsCombobox: Array<IComboxboxItem> = [
    {
      caption: 'Tag.Monat.Jahr',
      value: 'DD.MM.YYYY',
      title: dayjs().format('DD.MM.YYYY'),
    },
    {
      caption: 'Tag.Monat.Jahr-kurz',
      value: 'DD.MM.YY',
      title: dayjs().format('DD.MM.YY'),
    },
    {
      caption: 'Monat.Jahr',
      value: 'MM.YYYY',
      title: dayjs().format('MM.YYYY'),
    },
    {
      caption: 'Tag.Monat',
      value: 'DD.MM',
      title: dayjs().format('DD.MM'),
    },
    {
      caption: 'Stunde:Minute',
      value: 'HH:mm',
      title: dayjs().format('HH:mm'),
    },
    {
      caption: 'Stunde:Minute:Sekunde',
      value: 'HH:mm:ss',
      title: dayjs().format('HH:mm:ss'),
    },
    {
      caption: 'Wochentag',
      value: 'dddd',
      title: dayjs().format('dddd'),
    },
    {
      caption: 'Wochentag-kurz',
      value: 'ddd',
      title: dayjs().format('ddd'),
    },
    {
      caption: 'Monatsname',
      value: 'MMMM',
      title: dayjs().format('MMMM'),
    },
    {
      caption: 'Monatsname-kurz',
      value: 'MMM',
      title: dayjs().format('MMM'),
    },
    {
      caption: 'Wochentag Tag. Monatsname Jahr',
      value: 'dddd DD. MMMM YYYY',
      title: dayjs().format('dddd DD. MMMM YYYY'),
    },
  ];

  /**
   * dateformat object for calendar
   *
   * @type {{year: string, month: string, day: string}}
   * @memberof DateformaterService
   */
  public dayFormatOptions: { year: string; month: string; day: string } = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  };

  /**
   * possible german date input formats
   *
   * @public
   * @type {Array<string>}
   * @memberof DateformaterService
   */
  public possibleGermanFormats: Array<string> = [
    'ddd',
    'dddd',
    'MM',
    'MMM',
    'MMMM',
    'YYYY',
    'DD.MM',
    'MM.YYYY',
    'HH:mm',
    'HH:mm:ss',
    'DD.MM.YY',
    'DD.MM.YYYY',
    'DD.MM.YYYY HH:mm',
    'DD.MM.YYYY HH.mm',
    'DD.MM.YYYY HH:mm:ss',
    'dddd DD. MMMM YYYY',
  ];

  /**
   * displaynames for day enumeration
   *
   * @private
   * @memberof DateformaterService
   */
  private days = [
    'Sonntag',
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag',
  ];

  /**
   * abbreviated display day names
   *
   * @private
   * @memberof DateformaterService
   */
  private daysShort = ['So.', 'Mo.', 'Di.', 'Mi.', 'Do.', 'Fr.', 'Sa.'];

  /**
   * display names for months
   *
   * @private
   * @memberof DateformaterService
   */
  private months = [
    'Januar',
    'Februar',
    'März',
    'April',
    'Mai',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'Dezember',
  ];

  /**
   * abbreviated display names for months
   *
   * @private
   * @memberof DateformaterService
   */
  private monthsShort = [
    'Jan',
    'Feb',
    'März',
    'Apr',
    'Mai',
    'Juni',
    'Juli',
    'Aug',
    'Sept',
    'Okt',
    'Nov',
    'Dez',
  ];

  /**
   * Creates an instance of DateformaterService.
   * @memberof DateformaterService
   */
  constructor() {
    dayjs.locale(de);
    dayjs.extend(customParseFormat);
  }

  /**
   * format given date with given format
   *
   * @param {string} dateStr
   * @param {string} formatStr
   * @returns {string} formated date
   * @memberof DateformaterService
   */
  public formatDate(formatStr: string, dateStr?: string): string {
    const retdateStr = dateStr
      ? dayjs(dateStr, this.possibleGermanFormats, true).format(formatStr)
      : dayjs(new Date()).format(formatStr);

    return retdateStr === 'Invalid Date'
      ? 'Datum konnte nicht formatiert werden.'
      : retdateStr;
  }

  /**
   * returns javascript date for string if possible
   *
   * @param {string} dateStr
   * @returns
   * @memberof DateformaterService
   */
  public getDateObjFromString(dateStr: string, dateFormat: string) {
    const specialFormats = ['ddd', 'dddd', 'MM', 'MMMM', 'dddd DD. MMMM YYYY'];

    if (specialFormats.includes(dateFormat)) {
      let newDate = new Date();

      // ddd or dddd format
      if (dateFormat === 'ddd' || dateFormat === 'dddd') {
        const actDay = newDate.getDay();
        let dayToSet = 0;

        dayToSet =
          dateFormat === 'ddd'
            ? this.daysShort.indexOf(dateStr)
            : this.days.indexOf(dateStr);

        const distance = dayToSet !== actDay ? dayToSet - actDay : 0;
        newDate.setDate(newDate.getDate() + distance);
      }

      // MMM or MMMM format
      if (dateFormat === 'MMM' || dateFormat === 'MMMM') {
        const actMonth = newDate.getMonth();

        let monthToSet = 0;
        monthToSet =
          dateFormat === 'MMM'
            ? this.monthsShort.indexOf(dateStr)
            : this.months.indexOf(dateStr);

        const distance = monthToSet !== actMonth ? monthToSet - actMonth : 0;
        newDate.setMonth(newDate.getMonth() + distance);
      }

      if (dateFormat === 'dddd DD. MMMM YYYY') {
        // special handling needed, dayjs is meh at this point
        const splits = dateStr.split(' ');

        // splits example ['Mittwoch', '12.', 'Mai', '2021']
        if (splits.length === 4) {
          const parsedDay: number = +splits[1].replace('.', '');
          const parsedMonth: number = this.months.indexOf(splits[2]);
          const parsedYear: number = +splits[3];

          newDate.setDate(parsedDay);
          newDate.setMonth(parsedMonth);
          newDate.setFullYear(parsedYear);
        }
      }

      return dayjs(newDate);
    } else {
      const returnDate = dayjs(dateStr, this.possibleGermanFormats, true);
      return returnDate.isValid() ? returnDate : '';
    }
  }

  /**
   * handles a for or back change of a given date
   *
   * @param {string} direction - "forwards" will add one day, otherwise one day will be removed
   * @param {Date} dateObj
   * @param {string} format
   * @return {*}
   * @memberof DateformaterService
   */
  public changeDate(direction: string, dateObj: Date, format: string) {
    const changeVal = direction === 'forwards' ? 1 : -1;

    // switch for different dateformats
    switch (format) {
      case 'ddd':
      case 'dddd':
      case 'DD':
      case 'DD.MM':
      case 'DD.MM.YY':
      case 'DD.MM.YYYY':
      case 'dddd DD. MMMM YYYY':
        dateObj.setDate(dateObj.getDate() + changeVal);
        break;

      case 'HH:mm':
        dateObj.setMinutes(dateObj.getMinutes() + changeVal);
        break;

      case 'HH:mm:ss':
        dateObj.setSeconds(dateObj.getSeconds() + changeVal);

      case 'MM':
      case 'MMM':
      case 'MMMM':
      case 'MM.YYYY':
        dateObj.setMonth(dateObj.getMonth() + changeVal);
        break;

      case 'YYYY':
        dateObj.setFullYear(dateObj.getFullYear() + changeVal);
        break;
    }

    return dayjs(dateObj).format(format);
  }

  /**
   * creates a datepicker (calendar) based on id and function
   *
   * @param {string} elementRefId
   * @param {(instance) => void} onSelect
   * @returns {datepicker}
   * @memberof DateformaterService
   */
  public createDatePicker(
    elementRefId: string,
    onSelect: (instance) => void
  ): datepicker {
    const monthslabels = [
      'Jan',
      'Feb',
      'Mär',
      'Apr',
      'Mai',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Okt',
      'Nov',
      'Dez',
    ];

    const daylabels = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];

    const options = {
      customDays: daylabels,
      customMonths: monthslabels,
      customOverlayMonths: monthslabels,
      overlayPlaceholder: 'Jahr',
      overlayButton: 'Bestätigen',
      position: 'c',
      onSelect,
    };

    const picker = datepicker('#' + elementRefId, options);
    return picker;
  }
}
