import { Injectable } from '@angular/core';
import { AutotextListComponent } from '@components/autotext/autotext-list.component';
import { AutotextSelectPopoverComponent } from '@components/autotext/autotext-select-popover.component';
import { ModalController, PopoverController } from '@ionic/angular';
import { Utils } from '../dataformat/util';
import { Globals } from './global.service';

@Injectable({
  providedIn: 'root',
})
export class AutotextService {
  constructor(
    private modalCtrl: ModalController,
    private popoverCtrl: PopoverController
  ) {}

  /**
   * checks if element contains autotext and replaces it
   *
   * @param {*} element
   * @returns
   * @memberof AutotextService
   */
  public async checkForAutotext(element: any) {
    const nativeInput: HTMLInputElement =
      element.uiElement.nativeInput.firstChild.parentElement;

    const cursorpos: number = nativeInput.selectionStart;

    // get possible kuerzel
    if (element.Text !== '') {
      const text: string = element.Text;
      let startPos = 0;
      let endPos = 0;

      // cursor at beginning of text
      if (cursorpos === 0) {
        startPos = 0;
        endPos = text.includes(' ') ? text.indexOf(' ') : text.length;
      }
      // cursorpos at end of text
      else if (cursorpos === text.length) {
        startPos = text.includes(' ') ? text.lastIndexOf(' ') + 1 : 0;
        endPos = cursorpos;
      }
      // cursorpos in text
      else {
        // directly in word
        if (text[cursorpos] && text[cursorpos] !== ' ') {
          startPos = text.lastIndexOf(' ', cursorpos) + 1;
          endPos =
            text.indexOf(' ', cursorpos) > -1
              ? text.indexOf(' ', cursorpos)
              : text.length;
        }

        // if whitespace
        if (text[cursorpos] && text[cursorpos] === ' ') {
          startPos = text.lastIndexOf(' ', cursorpos - 1) + 1;
          endPos =
            text.indexOf(' ', cursorpos) > -1
              ? text.indexOf(' ', cursorpos)
              : text.length;
        }
      }

      const kuerzel = text.substring(startPos, endPos).trim();

      if (kuerzel !== '') {
        if (Globals.autotexts === undefined) {
          // TODO return error
          Utils.failPrefixed(
            'autotextservice',
            'Global.autotexts is undefined'
          );
          return;
        }

        let autotexts = Globals.autotexts.filter((entry) => {
          return entry.kuerzel.includes(kuerzel.toUpperCase());
        });

        if (autotexts !== undefined) {
          for (const matchingText of autotexts) {
            if (matchingText.kuerzel === kuerzel.toUpperCase()) {
              const textBeg = text.substring(0, startPos);
              const textEnd = text.substring(endPos, text.length);
              element.Text = textBeg + matchingText.text + textEnd;
              this.checkForAutotextAction(element);
              return;
            }
          }

          this.showAutotextList(element, kuerzel, startPos, endPos);
        }
      }
    } else {
      this.showAutotextList(element, '', 0, 0);
    }
  }

  /**
   * show autotextlist
   *
   * @param {string} [search='']
   * @param {*} element
   * @memberof AutotextService
   */
  public async showAutotextList(
    element: any,
    search: string = '',
    replaceBegin?: number,
    replaceEnd?: number
  ) {
    Utils.logPrefixed('autotextservice', 'showing autotextlist');

    const modal = await this.modalCtrl.create({
      component: AutotextListComponent,
      cssClass: 'autotextModal',
      backdropDismiss: true,
      componentProps: { search: search.toUpperCase() },
    });

    await modal.present();

    modal.onDidDismiss().then((result) => {
      if (result.data) {
        const text: string = element.Text;
        const textBeg = text.substring(0, replaceBegin);
        const textEnd = text.substring(replaceEnd, text.length);
        element.Text = textBeg + result.data.text + textEnd;

        this.checkForAutotextAction(element);
      }
    });
  }

  /**
   * checks for autotextaction and jumps to placeholders
   *
   * @param {*} element
   * @memberof AutotextService
   */
  public checkForAutotextAction(element: any) {
    element.IsDoubleClicked = true;
    element.setAsActiveElement();

    // TODO find better way
    const nativeInput: HTMLInputElement =
      element.uiElement.nativeInput.firstChild.parentElement;

    // timeout needed
    setTimeout(() => {
      nativeInput.focus();
      let action;
      const text = element.Text;

      // check for {...
      const foundIndex = text.indexOf('{...');

      if (foundIndex > -1) {
        // check type
        if (text[foundIndex + 4] === '}') {
          // freetext
          action = {
            markStart: foundIndex,
            markEnd: foundIndex + 5,
            type: 'freetext',
          };
        } else {
          // select
          const markEnd = text.indexOf('}', foundIndex) + 1;
          action = {
            markStart: foundIndex,
            markEnd,
            type: 'select',
          };
        }
      }

      // check if action is defined
      if (action && Object.keys(action).length) {
        // timeout needed
        setTimeout(() => {
          nativeInput.setSelectionRange(action.markStart, action.markEnd);

          if (action.type === 'select') {
            this.autotextSelectAction(
              element,
              action.markStart,
              action.markEnd
            );
          }
        }, 1);
      }
    }, 1);
  }

  private async autotextSelectAction(
    element: any,
    startIndex: number,
    endIndex: number
  ) {
    const text: string = element.Text;
    const stringToReplace = text.substring(startIndex, endIndex);
    const selectOptionsString = text.substring(startIndex + 4, endIndex - 1);
    const selectOptions = selectOptionsString.split(';');

    // set custom css vars
    const nativeInput: HTMLInputElement =
      element.uiElement.nativeInput.firstChild.parentElement;
    const bcr = nativeInput.getBoundingClientRect();
    document.body.style.setProperty(
      '--autotextlist-top',
      bcr.top + bcr.height + 'px'
    );
    document.body.style.setProperty('--autotextlist-left', bcr.left + 'px');

    const popover = await this.popoverCtrl.create({
      cssClass: 'autotextlistPopover',
      showBackdrop: false,
      backdropDismiss: true,
      component: AutotextSelectPopoverComponent,
      componentProps: { selectableItems: selectOptions },
    });

    await popover.present();

    element.IsDoubleClicked = true;
    element.setAsActiveElement();
    setTimeout(() => {
      const nativeInput: HTMLInputElement =
        element.uiElement.nativeInput.firstChild.parentElement;
      nativeInput.focus();
      nativeInput.setSelectionRange(startIndex, endIndex);
    }, 1);

    popover.onDidDismiss().then((result) => {
      if (result.data) {
        // replace text
        const newText = text.replace(stringToReplace, result.data);
        element.Text = newText;
        element.setAsActiveElement();
        element.IsDoubleClicked = true;
        this.checkForAutotextAction(element);
      }
    });
  }
}
