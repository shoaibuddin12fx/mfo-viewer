import { ElementRef, Injectable } from '@angular/core';
import { IDataPackage } from '@interfaces/datapackage';
import { IDataValue } from '@interfaces/datavalue';
import { MFO } from '../dataformat/models/mfo.model';
import { UiBaseSelectable } from '../dataformat/models/uibaseselectable';
import { Utils } from '../dataformat/util';

/**
 * Data Service for collecting data to store in Medical Office or to read data and update formula
 *
 * @export
 * @class DataService
 */
@Injectable({
  providedIn: 'root',
})
export class DataService {
  /**
   * cached mfo object
   *
   * @private
   * @type {MFO}
   * @memberof DataService
   */
  private mfo: MFO;

  /**
   * whitelist for properties which should be saved
   * !TODO add properties for Signfield and Markfield
   *
   * @private
   * @type {string[]}
   * @memberof DataService
   */
  private readonly PROPERTYWHITELIST: string[] = [
    'Text',
    'IsChecked',
    'Strokes',
    'Marks',
  ];

  /**
   * object type whitelist, contains all object types which can contain user input
   *
   * @private
   * @type {string[]}
   * @memberof DataService
   */
  private readonly OBJECTWHITELIST: string[] = [
    'input',
    'markfield',
    'signfield',
    'checkbox',
    'painting',
  ];

  /**
   * Creates an instance of DataService.
   * @memberof DataService
   */
  constructor() {}

  /**
   * updates the MFO formula with saved user data
   *
   * @param {IDataPackage} dtPackage
   * @param {MFO} mfo
   * @memberof DataService
   */
  public updateMfoWithPackageData(dtPackage: IDataPackage, mfo: MFO): void {
    this.mfo = mfo;
    // walk trough data of package and update elements
    for (let idx = 0; idx < dtPackage.data.length; idx++) {
      let data: IDataValue = dtPackage.data[idx];
      // handle optional propertyName, default considered to be Text
      data.propertyName = data.propertyName ?? 'Text';
      let element = this.resolvePath(data.path);
      element[data.propertyName] = data.value;
    }
    // custom event needed, because canvas can't be accessed from model
    window.dispatchEvent(
      new CustomEvent('dataLoaded', {
        bubbles: true,
        detail: [],
      })
    );
  }

  /**
   * resolves a string path to a UiBaseSelectable element
   *
   * @private
   * @param {string} path
   * @return {*}  {UiBaseSelectable}
   * @memberof DataService
   */
  private resolvePath(path: string): UiBaseSelectable {
    const splits = path.split('.');
    let root: any = this.mfo;
    for (let idx: number = 0; idx < splits.length; idx++) {
      root = root[splits[idx]];
    }
    return root;
  }

  /**
   * creates a data package for saving entered form data in Medical Office
   *
   * @param {MFO} mfo
   * @return {*}  {IDataPackage}
   * @memberof DataService
   */
  public createDataPackage(mfo: MFO): IDataPackage {
    this.mfo = mfo;
    const dtPackage: IDataPackage = { data: [] };

    // walk page tree
    for (let idx = 0; idx < this.mfo.Pages.length; idx++) {
      let element: any = this.mfo.Pages[idx];
      this.walkObjectTree(element, dtPackage);
    }

    Utils.logPrefixed('DataService - created Package', dtPackage);
    return dtPackage;
  }

  /**
   * recoursive function to walk the object hirarchy to collect entered data
   *
   * @private
   * @param {*} root
   * @param {IDataPackage} dtPackage
   * @memberof DataService
   */
  private walkObjectTree(root: any, dtPackage: IDataPackage) {
    if (root.Children?.length > 0)
      for (let i = 0; i < root.Children?.length; i++) {
        this.walkObjectTree(root.Children[i], dtPackage);
      }
    else this.createDataValuesByWhitelist(root, dtPackage);
  }

  /**
   * creates the data value entries for given element in the given data package
   *
   * @private
   * @param {*} element
   * @param {IDataPackage} dtPackage
   * @return {*}
   * @memberof DataService
   */
  private createDataValuesByWhitelist(element: any, dtPackage: IDataPackage) {
    // skip every element with no data to save
    if (this.OBJECTWHITELIST.indexOf(element.Type.toLowerCase()) < 0) return;

    // walk through property whitelist
    for (let idx = 0; idx < this.PROPERTYWHITELIST.length; idx++) {
      let propValue = element[this.PROPERTYWHITELIST[idx]];
      // skip empty properties or non existing properties
      if (propValue != undefined && propValue != '') {
        let value: IDataValue = {
          path: element.Path,
          propertyName: this.PROPERTYWHITELIST[idx],
          value: propValue,
        };
        // delete optional property propertyName when it's Text
        if (this.PROPERTYWHITELIST[idx] == 'Text') delete value.propertyName;
        dtPackage.data.push(value);
      }
    }
  }
}
