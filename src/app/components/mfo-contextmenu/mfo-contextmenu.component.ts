import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { TabIndexDissolverService } from '@services/tabindexdissolver.service';
import * as objectPath from 'object-path';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { MFO } from 'src/app/dataformat/models/mfo.model';
import {
  Barcode,
  Container,
  Edit,
  Image,
  Line,
  MarkField,
  Shape,
  SignField,
  Text,
} from '../../dataformat/models';
import { CheckBox } from '../../dataformat/models/';
import { HistoryService } from '@services/history.service';
import { HistoryAction } from 'src/app/dataformat/enums/historyactions.enum';
import { Utils } from 'src/app/dataformat/util';
import { ApiService } from '@services/api.service';
import { ModalController } from '@ionic/angular';
import { Painting } from 'src/app/dataformat/models/paint.model';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-contextmenu',
  templateUrl: './mfo-contextmenu.component.html',
  styleUrls: ['./mfo-contextmenu.component.scss'],
})
export class MfoContextMenuComponent implements AfterViewInit {
  /**
   * active element
   *
   * @memberof MfoContextMenuComponent
   */
  public element;

  /**
   * boolean when actual element is page
   *
   * @memberof MfoContextMenuComponent
   */
  public elementIsPage;

  /**
   * local clipboard for copied object
   *
   * @memberof MfoContextMenuComponent
   */
  public copied;

  /**
   * local clipboard for cutted objects
   *
   * @memberof MfoContextMenuComponent
   */
  public cutted;

  /**
   * MFO object reference
   *
   * @type {MFO}
   * @memberof MfoContextMenuComponent
   */
  public mfo: MFO;

  /**
   * left position of context menu to show at
   *
   * @private
   * @memberof MfoContextMenuComponent
   */
  private _left = 0;

  /**
   * top position of context menu to show at
   *
   * @private
   * @memberof MfoContextMenuComponent
   */
  private _top = 0;

  /**
   * relative x position of context menu to show at
   *
   * @private
   * @memberof MfoContextMenuComponent
   */
  private _relativeX = 0;

  /**
   * relative y position of context menu to show at
   *
   * @private
   * @memberof MfoContextMenuComponent
   */
  private _relativeY = 0;

  /**
   * indicates if menu is shown
   *
   * @private
   * @memberof MfoContextMenuComponent
   */
  private _showing = true;

  /**
   * zoom level
   *
   * @private
   * @memberof MfoContextMenuComponent
   */
  private _zoom = 1;

  /**
   * Mode of the editor
   *
   * @type {ViewMode}
   * @memberof MfoContextMenuComponent
   */
  @Input() Mode: ViewMode;

  /**
   * Ouptut event emitter for image upload
   *
   * @memberof MfoContextMenuComponent
   */
  @Output() imageUpload = new EventEmitter<string>();

  /**
   * Output event emitter for object export
   *
   * @memberof MfoContextMenuComponent
   */
  @Output() export = new EventEmitter<string>();

  /**
   * Output event emitter for object import
   *
   * @memberof MfoContextMenuComponent
   */
  @Output() import = new EventEmitter();

  /**
   * objectInput element reference
   *
   * @type {ElementRef}
   * @memberof MfoContextMenuComponent
   */
  @ViewChild('objectInput', { read: ElementRef, static: false })
  objectInput: ElementRef;

  /**
   * host binding for top property
   *
   * @readonly
   * @type {number}
   * @memberof MfoContextMenuComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this._top;
  }

  /**
   * host binding for left property
   *
   * @readonly
   * @type {number}
   * @memberof MfoContextMenuComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this._left;
  }

  /**
   * host binding for display
   *
   * @readonly
   * @type {string}
   * @memberof MfoContextMenuComponent
   */
  @HostBinding('style.display') get display(): string {
    return this._showing && this.element ? 'block' : 'none';
  }

  /**
   * host binding for zoom
   *
   * @readonly
   * @type {number}
   * @memberof MfoContextMenuComponent
   */
  @HostBinding('style.zoom') get zoom(): number {
    return this._zoom;
  }

  /**
   * Creates an instance of MfoContextMenuComponent.
   * @param {ElementRef} hostRef
   * @param {TabIndexDissolverService} tiresolveService
   * @memberof MfoContextMenuComponent
   */
  constructor(
    public hostRef: ElementRef,
    private tiresolveService: TabIndexDissolverService,
    private history: HistoryService
  ) {}

  /**
   * lifecycle hook after view init
   *
   * @memberof MfoContextMenuComponent
   */
  ngAfterViewInit() {}

  /**
   * adds history entry for new created Element
   *
   * @private
   * @param {*} element
   * @memberof MfoContextMenuComponent
   */
  private addHistoryUndo(element, addAction = true, parentEl?) {
    this.history.addUndo({
      element,
      action: addAction ? HistoryAction['Add'] : HistoryAction['Remove'],
      parent: addAction ? this.element.Children : parentEl,
    });
  }

  /**
   * get newElementposition
   *
   * @private
   * @param {*} newElement
   * @param {number} overrideX
   * @param {number} overrideY
   * @returns
   * @memberof MfoContextMenuComponent
   */
  private getOverrideProps(
    newElement: any,
    overrideX: number,
    overrideY: number
  ) {
    newElement.X = overrideX;
    newElement.Y = overrideY;

    let extraOffsetX = 0;
    let extraOffsetY = 0;

    const activeElement = this.element;

    // check if active Element is container
    if (Utils.isContainer(activeElement)) {
      // check if cursor is in mfo
      const activePage = this.mfo.getActivePage();

      if (
        overrideX < 0 ||
        overrideX > activePage.Width ||
        overrideY < 0 ||
        overrideY > activePage.Height
      ) {
        newElement.X = 10;
        newElement.Y = 10;
      }
    } else {
      // shapes can have shapes as child
      if (newElement.Type !== 'Shape' || this.element.Type !== 'Shape') {
        if (activeElement.Type !== 'Shape') {
          this.element = this.mfo.getParentContainerForElement(activeElement);
          newElement.X = activeElement.X + 10;
          newElement.Y = activeElement.Y + 10;
        } else {
          let parent = this.mfo.getParentContainerForElement(activeElement);

          // get parent until container
          while (parent.Type !== 'Container') {
            // add the extraoffsets
            extraOffsetX += parent.X;
            extraOffsetY += parent.Y;
            parent = this.mfo.getParentContainerForElement(parent);
          }
          this.element = parent;

          // check if mouse in shape
          if (
            overrideX < 0 ||
            overrideX > activeElement.Width ||
            overrideY < 0 ||
            overrideY > activeElement.Height
          ) {
            newElement.X = 10;
            newElement.Y = 10;
          } else {
            // add position of shape for perfect mousepos
            newElement.X = activeElement.X + overrideX + extraOffsetX;
            newElement.Y = activeElement.Y + overrideY + extraOffsetY;
          }
        }
      }
    }

    return newElement;
  }

  /**
   * shows a object
   *
   * @param {*} element
   * @param {*} x
   * @param {*} y
   * @param {*} relativeX
   * @param {*} relativeY
   * @memberof MfoContextMenuComponent
   */
  public show(element, x, y, relativeX, relativeY) {
    this.element = element;
    this._showing = true;
    this._top = y;
    this._left = x;
    this._relativeX = relativeX;
    this._relativeY = relativeY;
    if (element.__getPath.split('.').length === 2) {
      this.elementIsPage = true;
    } else {
      this.elementIsPage = false;
    }
  }

  /**
   * event handler to hide a object
   *
   * @memberof MfoContextMenuComponent
   */
  @HostListener('window:click', ['$event'])
  @HostListener('window:dblclick', ['$event'])
  @HostListener('window:contextmenu', ['$event'])
  @HostListener('window:keydown', ['$event'])
  @HostListener('window:scrollY', ['$event'])
  public hide() {
    this.element = undefined;
    this._showing = false;
  }

  /**
   * sends an object to front
   *
   * @memberof MfoContextMenuComponent
   */
  public toFront(backupElement?: any) {
    if (backupElement && this.element === undefined) {
      this.element = backupElement;
    }

    let path = this.element.__getPath.split('.');
    const copy = JSON.parse(JSON.stringify(this.element));
    // convert object to classobject
    const copiedElement = Utils.objectFromMfoInterface(copy);

    // traverse 1 up to Children Property
    path.pop();

    // get oldPath
    const oldPath = this.element.__getPath;
    const actionParent = this.mfo.getParentContainerForElement(this.element);

    // delete old entry
    objectPath.del(this.mfo, this.element.__getPath);

    // insert copy of element as last position
    let parent = objectPath.get(this.mfo, path);
    parent.push(copiedElement);

    // get new Path
    const newPath = parent[parent.length - 1].__getPath;

    // set newElement as activeElement otherwise mfo will break
    const newElement = parent[parent.length - 1];
    newElement.setAsActiveElement();

    // add history
    this.history.addPathChange(
      this.element,
      oldPath,
      newPath,
      actionParent.Children
    );
  }

  /**
   * sends an object to background
   *
   * @memberof MfoContextMenuComponent
   */
  public toBack(backupElement?: any) {
    if (backupElement && this.element === undefined) {
      this.element = backupElement;
    }

    let path = this.element.__getPath.split('.');
    const copy = JSON.parse(JSON.stringify(this.element));
    // convert object to classobject
    const copiedElement = Utils.objectFromMfoInterface(copy);

    // traverse 1 up to Children Property
    path.pop();

    // get oldPath
    const oldPath = this.element.__getPath;
    const actionParent = this.mfo.getParentContainerForElement(this.element);

    // delete old entry
    objectPath.del(this.mfo, this.element.__getPath);

    // insert copy of element as first position
    let parent = objectPath.get(this.mfo, path);
    parent.unshift(copiedElement);

    // get new Path
    const newPath = parent[0].__getPath;

    // set newElement as activeElement otherwise mfo will break
    const newElement = parent[0];
    newElement.setAsActiveElement();

    // add history
    this.history.addPathChange(
      this.element,
      oldPath,
      newPath,
      actionParent.Children
    );
  }

  /**
   * deletes a object
   *
   * @memberof MfoContextMenuComponent
   */
  public delete() {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let parent = this.mfo.getParentContainerForElement(this.element);

    if (parent !== undefined) {
      Utils.logPrefixed('mfo-contextmenu', 'removing element', this.element);
      this.addHistoryUndo(this.element, false, parent.Children);
      objectPath.del(this.mfo, this.element.__getPath);

      // set activeElement to Page Object to prevent errors in creation
      this.mfo.activeElement = this.mfo.getActivePage();
      return;
    }
  }

  /**
   * copy a object
   *
   * @memberof MfoContextMenuComponent
   */
  public copy(backupElement?: any) {
    if (backupElement && this.element === undefined) {
      this.element = backupElement;
    }

    this.copied = JSON.parse(JSON.stringify(this.element));
    this.cutted = undefined;
  }

  /**
   * cuts an object
   *
   * @param {*} [backupElement]
   * @memberof MfoContextMenuComponent
   */
  public cut(backupElement?: any) {
    if (backupElement && this.element === undefined) {
      this.element = backupElement;
    }

    this.cutted = JSON.parse(JSON.stringify(this.element));
    objectPath.del(this.mfo, this.element.__getPath);

    this.copied = undefined;
  }

  /**
   * pastes object
   *
   * @param {*} [backupElement]
   * @memberof MfoContextMenuComponent
   */
  public paste(backupElement?: any, overrideX?: number, overrideY?: number) {
    if (backupElement && this.element === undefined) {
      this.element = backupElement;
    }

    if (this.copied || this.cutted) {
      // create a real copy to make multiple pastes of one copy possible
      let newElement = this.copied
        ? JSON.parse(JSON.stringify(this.copied))
        : JSON.parse(JSON.stringify(this.cutted));

      // convert newElement to classObject
      newElement = Utils.objectFromMfoInterface(newElement);

      newElement.IsActive = false;
      newElement.IsSelected = false;
      newElement.X = this._relativeX;
      newElement.Y = this._relativeY;

      let parent;

      // if overrideX and overrideY
      if (overrideX !== undefined && overrideY !== undefined) {
        newElement = this.getOverrideProps(newElement, overrideX, overrideY);
      } else {
        newElement = this.getOverrideProps(
          newElement,
          this._relativeX,
          this._relativeY
        );
      }
      parent = this.element;
      parent.Children = parent.Children || [];
      parent.Children.push(newElement);

      // if cutted was set
      if (this.cutted) {
        this.cutted = undefined;
      }

      // history
      this.history.addUndo({
        element: parent.Children[parent.Children.length - 1],
        action: HistoryAction['Add'],
        parent: parent.Children,
      });

      // return the new pasted element if backupElement is set
      if (backupElement) {
        // ! dont change, returning newElement results in wrong path!
        return parent.Children[parent.Children.length - 1];
      }
    }
  }

  /**
   * creates new Text object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createText(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new Text();
    newElement.Type = 'Text';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 200;
    newElement.Height = 20;
    newElement.Text = '';
    newElement.FontSize = this.element.FontSize || 12;
    newElement.FontFamily = this.element.FontFamily || 'Tahoma';
    newElement.FontColor = this.element.FontColor || '#000000';
    newElement.LineSpacing = this.element.LineSpacing || 1;
    newElement.LetterSpacing = this.element.LetterSpacing || 0;
    newElement.Print = true;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Color = '#FFFFFF';
    this.tiresolveService.setNextTabIndex(newElement);

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new Shape object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createShape(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new Shape();
    newElement.Type = 'Shape';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 200;
    newElement.Height = 20;
    newElement.BorderWidth = 1;
    newElement.BorderColor = '#000000';
    newElement.Color = '#FFFFFF';
    newElement.Print = true;
    newElement.Color = '#FFFFFF';

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new Image object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createImage(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new Image();
    newElement.Type = 'Image';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 200;
    newElement.Height = 200;
    newElement.Print = true;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Image = '';

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new Edit object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createEdit(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new Edit();
    newElement.Type = 'Input';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 200;
    newElement.Height = 20;
    newElement.Multiline = true;
    newElement.Number = false;
    newElement.Print = true;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.FontSize = this.element.FontSize || 12;
    newElement.FontFamily = this.element.FontFamily || 'Tahoma';
    newElement.FontColor = this.element.FontColor || '#000000';
    newElement.LineSpacing = this.element.LineSpacing || 1;
    newElement.LetterSpacing = this.element.LetterSpacing || 0;
    this.tiresolveService.setNextTabIndex(newElement);
    newElement.Text = '';
    newElement.Color = '#FFFFFF';

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new MarkField object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createMarkField(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new MarkField();
    newElement.Type = 'MarkField';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 250;
    newElement.Height = 200;
    newElement.Color = '#FFFFFF';
    newElement.FontColor = this.element.FontColor || '#000000';
    newElement.BackgroundColor = '';
    newElement.Print = true;
    newElement.Connect = false;
    newElement.Image = '';
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new Line object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createLine(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new Line();
    newElement.Type = 'Line';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.X2 = this._relativeX + 50;
    newElement.Y2 = this._relativeY + 50;
    newElement.Width = 2;
    newElement.Color = '#000000';
    newElement.Print = true;

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new CheckBox object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createCheckBox(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new CheckBox();
    newElement.Type = 'CheckBox';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 20;
    newElement.Height = 20;
    newElement.BackgroundColor = '#c0c0c0';
    newElement.Color = '#ffffff';
    this.tiresolveService.setNextTabIndex(newElement);

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new Barcode object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createBarcode(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new Barcode();
    newElement.Type = 'Barcode';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 80;
    newElement.Height = 80;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Print = true;
    newElement.Text = '';
    newElement.BarcodeType = 'QR';

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new Container object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createContainer(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new Container();
    newElement.Type = 'Container';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 200;
    newElement.Height = 200;
    newElement.Color = '#FFFFFF';
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Print = false;
    newElement.Image = '';
    this.tiresolveService.setNextTabIndex(newElement);
    newElement.FontFamily = this.element?.FontFamily || 'Tahoma';
    newElement.FontColor = this.element?.FontColor || '#000000';
    newElement.FontSize = this.element?.FontSize || '';
    newElement.LineSpacing = this.element?.LineSpacing || 1;
    newElement.LetterSpacing = this.element?.LetterSpacing || 0;
    newElement.Children = [];

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children = this.element.Children || [];
    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates a painting component
   *
   * @param {*} event
   * @return {*}
   * @memberof MfoContextMenuComponent
   */
  public createPainting() {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    const newElement = new Painting();
    newElement.Type = 'Painting';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 250;
    newElement.Height = 200;
    newElement.Color = '#FFFFFF';
    newElement.FontColor = '#000000';
    newElement.Print = true;
    newElement.Image = '';
    newElement.BorderWidth = 1;
    newElement.BorderColor = '#000000';

    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * creates new SignField object
   *
   * @param {number} [overrideX]
   * @param {number} [overrideY]
   * @memberof MfoContextMenuComponent
   */
  public createSignField(overrideX?: number, overrideY?: number) {
    // if element is undefined
    if (this.element === undefined) {
      this.element = this.mfo.activeElement
        ? this.mfo.activeElement
        : this.mfo.getActivePage();
    }

    let newElement = new SignField();
    newElement.Type = 'SignField';
    newElement.X = this._relativeX;
    newElement.Y = this._relativeY;
    newElement.Width = 250;
    newElement.Height = 50;
    newElement.BorderColor = '#000000';
    newElement.BorderWidth = 1;
    newElement.Print = true;
    this.tiresolveService.setNextTabIndex(newElement);

    newElement.FontColor = '#000000';
    newElement.Color = '#FFFFFF';

    // if from keyboard shortcut
    if (overrideX !== undefined && overrideY !== undefined) {
      newElement = this.getOverrideProps(newElement, overrideX, overrideY);
    }

    this.element.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement);
  }

  /**
   * emit chosen object file and reset the input value
   *
   * @public
   * @memberof MfoContextMenuComponent
   */
  public importObject() {
    this.import.emit({
      File: this.objectInput.nativeElement.files[0],
      Type: 'Object',
    });
    this.objectInput.nativeElement.value = null;
  }
}
