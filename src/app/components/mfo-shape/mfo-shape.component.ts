import { Component, ElementRef, HostBinding, Input } from '@angular/core';
import { HistoryService } from '@services/history.service';
import { AppConfig } from '../../app.config';
import { Shape } from '../../dataformat/models/shape.model';
import { MfoBase } from '../mfo-base/mfo-base';

/**
 * component to display a shape (rectangle) on a page
 *
 * @export
 * @class MfoShapeComponent
 * @extends {MfoBase}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-shape',
  templateUrl: './mfo-shape.component.html',
})
export class MfoShapeComponent extends MfoBase {
  /**
   * element context
   *
   * @type {Shape}
   * @memberof MfoShapeComponent
   */
  @Input() element: Shape;

  /**
   * border-style binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    return this.element.BorderWidth
      ? 'solid'
      : this.Mode === 0
      ? 'dotted'
      : 'none';
  }

  /**
   * border-color binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === 0
      ? 'grey'
      : 'transparent';
  }

  /**
   * border-width binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === 0
      ? 1
      : 0;
  }

  /**
   * box-sizing binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   * width binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * height binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * top binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * left binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * position binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * display binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * background-color binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoShapeComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return this.element.Color;
  }

  /**
   * Creates an instance of MfoShapeComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @memberof MfoShapeComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }
}
