import { Component, ElementRef, HostBinding, Input } from '@angular/core';
import { HistoryService } from '@services/history.service';
import { AppConfig } from '../../app.config';
import { Line } from '../../dataformat/models';
import { MfoBase } from '../mfo-base/mfo-base';

/**
 * displays a line on a page
 *
 * @export
 * @class MfoLineComponent
 * @extends {MfoBase}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-line',
  templateUrl: './mfo-line.component.html',
})
export class MfoLineComponent extends MfoBase {
  /**
   * element context
   *
   * @type {Line}
   * @memberof MfoLineComponent
   */
  @Input() element: Line;

  /**
   * width of the container
   *
   * @type {number}
   * @memberof MfoLineComponent
   */
  @Input() width: number;

  /**
   * height of the container
   *
   * @type {number}
   * @memberof MfoLineComponent
   */
  @Input() height: number;

  /**
   * box-sizing binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   *
   * width binding
   * @readonly
   * @type {number}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.width.px') get styleWidth(): number {
    return Math.abs(this.element.X - this.element.X2) + this.element.Width;
  }

  /**
   *
   * height binding
   * @readonly
   * @type {number}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.height.px') get styleHeight(): number {
    return Math.abs(this.element.Y - this.element.Y2) + this.element.Width;
  }

  /**
   *
   * top binding
   * @readonly
   * @type {number}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y < this.element.Y2 ? this.element.Y : this.element.Y2;
  }

  /**
   *
   * left binding
   * @readonly
   * @type {number}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X < this.element.X2 ? this.element.X : this.element.X2;
  }

  /**
   *
   * position binding
   * @readonly
   * @type {string}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   *
   * display binding
   * @readonly
   * @type {string}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * pointer-events binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoLineComponent
   */
  @HostBinding('style.pointerEvents') get pointerEvents(): string {
    return 'none';
  }

  /**
   * Creates an instance of MfoLineComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @memberof MfoLineComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }
}
