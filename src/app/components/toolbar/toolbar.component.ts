import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
} from '@angular/core';
import { AlertController } from '@ionic/angular';
import { DatabindingService } from '@services/databinding.service';
import { DateformaterService } from '@services/dateformater.service';
import { FontService } from '@services/font.service';
import { HistoryService } from '@services/history.service';
import { TabIndexDissolverService } from '@services/tabindexdissolver.service';
import { HistoryAction } from 'src/app/dataformat/enums/historyactions.enum';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import {
  Barcode,
  CheckBox,
  Container,
  Edit,
  Image,
  Line,
  MarkField,
  Shape,
  SignField,
  Text,
} from 'src/app/dataformat/models';
import { MFO } from 'src/app/dataformat/models/mfo.model';
import { Painting } from 'src/app/dataformat/models/paint.model';
import { UiBaseSelectable } from 'src/app/dataformat/models/uibaseselectable';
import { Utils } from 'src/app/dataformat/util';
import { environment } from '../../../environments/environment';
import { AppConfig } from '../../app.config';

/**
 * component for creating new object on active page
 *
 * @export
 * @class ToolbarComponent
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
  public environment;

  /**
   * MFO Object for pathfinding
   *
   * @type {MFO}
   * @memberof ToolbarComponent
   */
  @Input() public MFO: MFO;

  /**
   * Mode of the editor
   *
   * @type {ViewMode}
   * @memberof MfoContainerComponent
   */
  @Input() public Mode: ViewMode;

  @Output()
  public ButtonToggled: EventEmitter<UiBaseSelectable> = new EventEmitter<UiBaseSelectable>();

  /**
   * actual active Element for finding propriate parent
   *
   * @type {UiBaseSelectable}
   * @memberof ToolbarComponent
   */
  public activeElement: UiBaseSelectable;

  public toggled: string;

  /**
   * Creates an instance of ToolbarComponent.
   * @param {AppConfig} config
   * @param {AlertController} alertController
   * @param {TranslationService} translation
   * @param {FontService} fontService
   * @param {DatabindingService} dataBinding
   * @param {DateformaterService} dateFormater
   * @param {ElementRef} hostRef
   * @param {TabIndexDissolverService} tiresolveService
   * @memberof ToolbarComponent
   */
  constructor(
    public config: AppConfig,
    public alertController: AlertController,
    public fontService: FontService,
    public dataBinding: DatabindingService,
    public dateFormater: DateformaterService,
    public hostRef: ElementRef,
    private tiresolveService: TabIndexDissolverService,
    private history: HistoryService
  ) {
    this.environment = environment;
  }

  @HostListener('window:elementCreated', ['$event'])
  private untoggle(data) {
    // Utils.log('untoggle');
    this.toggled = undefined;
  }

  /**
   * adds history entry for new created element
   *
   * @private
   * @param {*} element
   * @param {*} parent
   * @memberof ToolbarComponent
   */
  private addHistoryUndo(element, parent) {
    this.history.addUndo({
      element,
      action: HistoryAction['Add'],
      parent,
    });
  }

  /**
   * creates a new Text object
   *
   * @return {*}
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public async createText(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    // let c = this.MFO.getContainerForActiveElement();
    let fonts = this.MFO.Fonts;
    if (c === undefined) return;
    
    const newElement = new Text();
    newElement.Type = 'Text';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 200;
    newElement.Height = 20;
    newElement.Text = '';
    newElement.FontSize = 12;
    //newElement.FontFamily = c.FontFamily || 'Tahoma' ;
    let defaultFont = await this.fontService.getDefaultFont();
    // changed the logic here:  fonts[fonts.length-1].Family || defaultFont.Family;    
    newElement.FontFamily = defaultFont.Family 
    newElement.FontColor = '#000000';
    newElement.Print = true;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Color = '#FFFFFF';
    console.log(newElement.FontFamily)
    this.tiresolveService.setNextTabIndex(newElement);
    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        this.ButtonToggled.emit(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    // Utils.log('createText', event, this.activeElement, c);
    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Shape object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createShape(event) {
    let c;

    if (this.activeElement.Type !== 'Shape') {
      c = Utils.getActiveContainer(this.activeElement, this.MFO);
    } else {
      c = this.activeElement;
    }

    const newElement = new Shape();
    newElement.Type = 'Shape';
    newElement.X = 10;
    newElement.Y = 10;
    newElement.Width = 200;
    newElement.Height = 20;
    newElement.BorderWidth = 1;
    newElement.BorderColor = '#000000';
    newElement.Print = true;

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }
    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Image object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createImage(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new Image();
    newElement.Type = 'Image';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 200;
    newElement.Height = 200;
    newElement.Print = true;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Image = '';

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }
    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Editfield object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createEdit(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
   
    const newElement = new Edit();
    newElement.Type = 'Input';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 200;
    newElement.Height = 20;
    newElement.Multiline = true;
    newElement.Number = false;
    newElement.Print = true;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.FontSize = c.FontSize || 12;
    newElement.FontFamily = c.FontFamily || 'Tahoma';
    newElement.FontColor = c.FontColor || '#000000';
    this.tiresolveService.setNextTabIndex(newElement);
    newElement.Text = '';
    newElement.Color = '#FFFFFF';

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Markfield object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createMarkField(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new MarkField();
    newElement.Type = 'MarkField';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 250;
    newElement.Height = 200;
    newElement.Color = '#FFFFFF';
    newElement.FontColor = '#000000';
    newElement.BackgroundColor = '';
    newElement.Print = true;
    newElement.Connect = false;
    newElement.Image = '';
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates a painting component
   *
   * @param {*} event
   * @return {*}
   * @memberof ToolbarComponent
   */
  public createPainting(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new Painting();
    newElement.Type = 'Painting';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 250;
    newElement.Height = 200;
    newElement.Color = '#FFFFFF';
    newElement.FontColor = '#000000';
    newElement.Print = true;
    newElement.Image = '';
    newElement.BorderWidth = 1;
    newElement.BorderColor = '#000000';

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);

        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Line object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createLine(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new Line();
    newElement.Type = 'Line';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.X2 = this.activeElement.X + 50;
    newElement.Y2 = this.activeElement.Y + 50;
    newElement.Width = 2;
    newElement.Color = '#000000';
    newElement.Print = true;

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Checkbox object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createCheckBox(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new CheckBox();
    newElement.Type = 'CheckBox';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 20;
    newElement.Height = 20;
    newElement.BackgroundColor = '#c0c0c0';
    this.tiresolveService.setNextTabIndex(newElement);

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Barcode object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createBarcode(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new Barcode();
    newElement.Type = 'Barcode';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 80;
    newElement.Height = 80;
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Print = true;
    newElement.Text = '';
    newElement.BarcodeType = 'QR';

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Container object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createContainer(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new Container();
    newElement.Type = 'Container';
    Utils.log('ae', this.activeElement);
    newElement.X = 10;
    newElement.Y = 10;
    newElement.Width = 200;
    newElement.Height = 200;
    newElement.Color = '#FFFFFF';
    newElement.BorderWidth = 0;
    newElement.BorderColor = '#000000';
    newElement.Print = false;
    newElement.Image = '';
    this.tiresolveService.setNextTabIndex(newElement);
    newElement.FontFamily = c.FontFamily || 'Tahoma';
    newElement.FontColor = c.FontColor || '#000000';
    newElement.FontSize = c.FontSize || '';
    newElement.Children = [];
    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }

    c.Children = c.Children || [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }

  /**
   * creates new Signfield object
   *
   * @param {*} event
   * @memberof ToolbarComponent
   */
  public createSignField(event) {
    let c = Utils.getActiveContainer(this.activeElement, this.MFO);
    const newElement = new SignField();
    newElement.Type = 'SignField';
    newElement.X = c === this.activeElement ? 10 : this.activeElement.X + 10;
    newElement.Y = c === this.activeElement ? 10 : this.activeElement.Y + 10;
    newElement.Width = 250;
    newElement.Height = 50;
    newElement.BorderColor = '#000000';
    newElement.BorderWidth = 1;
    newElement.Print = true;
    this.tiresolveService.setNextTabIndex(newElement);
    newElement.FontColor = '#000000';
    newElement.Color = '#FFFFFF';

    if (!event.shiftKey) {
      if (this.toggled == newElement.Type.toLowerCase()) {
        this.untoggle(undefined);
        return;
      }
      this.toggled = newElement.Type.toLowerCase();
      this.ButtonToggled.emit(newElement);
      return;
    }
    c.Children = c.Children ?? [];
    c.Children.push(newElement);
    newElement.setAsActiveElement();

    this.addHistoryUndo(newElement, c.Children);
  }
}
