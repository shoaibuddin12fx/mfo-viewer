import {
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
} from '@angular/core';
import { HistoryService } from '@services/history.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { AppConfig } from '../../app.config';
import { CheckBox } from '../../dataformat/models/';
import { MfoBase } from '../mfo-base/mfo-base';

/**
 * Checkbox Component
 *
 * @export
 * @class MfoCheckBoxComponent
 * @extends {MfoBase}
 */
@Component({
  selector: 'mfo-checkbox',
  templateUrl: './mfo-checkbox.component.html',
})
export class MfoCheckBoxComponent extends MfoBase {
  /**
   * Element Context.
   *
   * @type {Container}
   * @memberof MfoCheckBoxComponent
   */
  @Input() element: CheckBox;

  /**
   * BorderStyle binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    return this.element.BorderWidth
      ? 'inset'
      : this.Mode === ViewMode.Edit
      ? 'dotted'
      : 'none';
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === ViewMode.Edit
      ? 'grey'
      : 'transparent';
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === ViewMode.Edit
      ? 1
      : 0;
  }

  /**
   * Host binding for border radius
   *
   * @readonly
   * @type {number}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.borderRadius.px') get borderRadius(): number {
    return this.Mode === ViewMode.Edit && !this.element.BorderWidth ? 4 : 0;
  }

  /**
   * BoxSizing binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   * BackgroundColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return this.Mode == ViewMode.Interactive || this.Mode == ViewMode.Preview
      ? 'var(--ion-color-light)'
      : this.element.Color;
  }

  /**
   * host binding for background image
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.backgroundImage') get backgroundImage(): string {
    if (this.element.IsChecked) {
      return 'url(../../assets/icons/close-outline.svg)';
    }
  }

  /**
   * host binding for background image size
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.backgroundSize') get beckgroundImageSize(): string {
    return '100% 100%';
  }

  /**
   * Width binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * Position binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * Display binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * event handler for click event to handle group toggle
   *
   * @param {MouseEvent} event
   * @return {*}
   * @memberof MfoCheckBoxComponent
   */
  @HostListener('click', ['$event'])
  toggle(event: MouseEvent) {
    if (this.Mode !== ViewMode.Interactive && this.Mode !== ViewMode.Preview)
      return;
    event.preventDefault();
    event.stopPropagation();
    this.uncheckGroup();
    this.toggleCheckBox();
  }

  /**
   * toggle checkbox on double click
   *
   * @param {MouseEvent} event
   * @return {*}
   * @memberof MfoCheckBoxComponent
   */
  @HostListener('dblclick', ['$event'])
  editViewtoggle(event: MouseEvent) {
    if (this.Mode !== ViewMode.Edit) return;
    event.preventDefault();
    event.stopPropagation();
    this.uncheckGroup();
    this.toggleCheckBox();
  }

  /**
   * Weight binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoCheckBoxComponent
   */
  @HostBinding('style.whiteSpace') get whiteSpace(): string {
    return 'pre-wrap';
  }

  /**
   * toggles checkbox
   *
   * @param {boolean} [checked]
   * @memberof MfoCheckBoxComponent
   */
  public toggleCheckBox(checked?: boolean) {
    if (checked == null) this.element.IsChecked = !this.element.IsChecked;
    else this.element.IsChecked = checked;

    this.history.addPropertyChange(this.element, [
      {
        propertyName: 'IsChecked',
        propertyValueNew: this.element.IsChecked,
        propertyValueOld: !this.element.IsChecked,
      },
    ]);
  }

  /**
   * raises event for unchecking checkbox groups
   *
   * @private
   * @memberof MfoCheckBoxComponent
   */
  private uncheckGroup() {
    if (!this.element.IsChecked && this.element.GroupIndex > 0) {
      (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
        new CustomEvent('checkBoxChecked', {
          bubbles: true,
          detail: this.element.GroupIndex,
        })
      );
    }
  }

  /**
   * Creates an instance of MfoCheckBoxComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @memberof MfoCheckBoxComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }
}
