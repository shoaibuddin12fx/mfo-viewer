import { Component, EventEmitter, Input, Output } from '@angular/core';
import { HistoryService } from '@services/history.service';

/**
 * text input component for sidebar properties
 *
 * @export
 * @class SbTextInput
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-text-input',
  templateUrl: './sb-text-input.component.html',
  styleUrls: ['./sb-text-input.component.scss'],
})
export class SbTextInput {
  constructor(private history: HistoryService) {}

  /**
   * reference to active selected element
   *
   * @type {*}
   * @memberof SbTextInput
   */
  @Input() activeElement: any;

  /**
   * property as string
   *
   * @type {string}
   * @memberof SbTextInput
   */
  @Input() property: string;

  /**
   * textarea function
   * 
   * @memberof SbTextInput
   */
  @Output() openTextareaFunction = new EventEmitter<string>();

  private oldVal = '';

  /**
   * focused property
   *
   * @type {boolean}
   * @memberof SbTextInput
   */
  public focused: boolean = false;

  /**
   * clears element content
   *
   * @memberof SbTextInput
   */
  public clear() {
    const oldVal = this.activeElement[this.property] || '';
    this.activeElement[this.property] = '';
    this.addHistoryChange(oldVal);
  }

  /**
   * calls opentextarea function of sidebar
   *
   * @memberof SbTextInput
   */
  public openTextarea() {
    this.openTextareaFunction.emit();
  }

  /**
   * onfocus function
   *
   * @memberof SbTextInput
   */
  public onFocus() {
    this.focused = true;
    this.oldVal = this.activeElement[this.property] || '';
  }

  /**
   * onblur function
   *
   * @memberof SbTextInput
   */
  public onBlur() {
    this.focused = false;
    this.addHistoryChange(this.oldVal);
  }

  /**
   * add history propertychange
   *
   * @private
   * @param {string} oldVal
   * @memberof SbTextInput
   */
  private addHistoryChange(oldVal: string) {
    if (oldVal !== this.activeElement[this.property]) {
      this.history.addPropertyChange(this.activeElement, [
        {
          propertyValueOld: oldVal,
          propertyValueNew: this.activeElement[this.property],
          propertyName: this.property,
        },
      ]);
    }
  }
}
