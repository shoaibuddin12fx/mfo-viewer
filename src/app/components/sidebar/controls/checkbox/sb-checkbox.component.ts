import { Component, Input } from '@angular/core';
import { HistoryService } from '@services/history.service';

/**
 * checkbox component for sidebar properties
 *
 * @export
 * @class SbCheckbox
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-checkbox',
  templateUrl: './sb-checkbox.component.html',
  styleUrls: ['./sb-checkbox.component.scss'],
})
export class SbCheckbox {
  /**
   * Creates an instance of SbCheckbox.
   * @param {HistoryService} history
   * @memberof SbCheckbox
   */
  constructor(private history: HistoryService) {}

  /**
   * reference to activeElement
   *
   * @memberof SbCheckbox
   */
  @Input() activeElement;

  /**
   * property
   *
   * @type {string}
   * @memberof SbCheckbox
   */
  @Input() property: string;

  /**
   * toggles value true || false
   *
   * @memberof SbCheckbox
   */
  public toggle(): void {
    const oldVal = this.activeElement[this.property];
    this.activeElement[this.property] = !this.activeElement[this.property];

    // history change
    this.history.addPropertyChange(this.activeElement, [
      {
        propertyValueNew: this.activeElement[this.property],
        propertyValueOld: oldVal,
        propertyName: this.property,
      },
    ]);
  }
}
