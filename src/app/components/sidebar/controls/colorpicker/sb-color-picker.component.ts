import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { HistoryService } from '@services/history.service';

/**
 * color picker component for sidebar components
 *
 * @export
 * @class SbColorPicker
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-color-picker',
  templateUrl: './sb-color-picker.component.html',
  styleUrls: ['./sb-color-picker.component.scss'],
})
export class SbColorPicker {
  /**
   * Creates an instance of SbColorPicker.
   * @param {HistoryService} history
   * @memberof SbColorPicker
   */
  constructor(private history: HistoryService) {}

  /**
   * reference to active element
   *
   * @memberof SbColorPicker
   */
  @Input() activeElement;

  /**
   * property as string
   *
   * @type {string}
   * @memberof SbColorPicker
   */
  @Input() property: string;

  /**
   * "real" ionic color picker
   *
   * @type {ElementRef}
   * @memberof SbColorPicker
   */
  @ViewChild('picker', { read: ElementRef }) picker: ElementRef;

  /**
   * activates ionic color picker which is hidden
   *
   * @memberof SbColorPicker
   */
  public async activate() {
    const input = await this.picker.nativeElement.getInputElement();
    input.click();
  }

  /**
   * changeEvent for colorpicker
   *
   * @param {string} value
   * @memberof SbColorPicker
   */
  public onChange(value: string) {
    const oldVal = this.activeElement[this.property];
    this.activeElement[this.property] = value;

    this.history.addPropertyChange(this.activeElement, [
      {
        propertyName: this.property,
        propertyValueOld: oldVal,
        propertyValueNew: value,
      },
    ]);
  }
}
