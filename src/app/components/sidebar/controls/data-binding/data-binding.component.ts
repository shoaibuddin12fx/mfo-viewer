import { HistoryService } from '@services/history.service';
import { DbDialogComponent } from '@components/sidebar/controls/data-binding/db-dialog.component';
import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sb-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.scss'],
})
export class SbDataBinding {
  /**
   * Creates an instance of SbDataBinding.
   * @param {ModalController} modalController
   * @param {HistoryService} historyService
   * @memberof SbDataBinding
   */
  constructor(
    private modalController: ModalController,
    private historyService: HistoryService
  ) {}

  /**
   * reference to active selected element
   *
   * @type {*}
   * @memberof SbDataBinding
   */
  @Input() activeElement: any;

  /**
   * open the databinding modal
   *
   * @returns
   * @memberof SbDataBinding
   */
  public async openDialog() {
    const modal = await this.modalController.create({
      component: DbDialogComponent,
      componentProps: {
        Topic: this.activeElement.Topic ? this.activeElement.Topic : '',
        Item: this.activeElement.Item ? this.activeElement.Item : '',
      },
      cssClass: 'dataBindingModal',
      showBackdrop: false,
    });

    modal.onDidDismiss().then((data) => {
      if (data.data) {
        this.historyService.addPropertyChange(this.activeElement, [
          {
            propertyValueOld: this.activeElement.Topic,
            propertyValueNew: data.data.topic,
            propertyName: 'Topic',
          },
          {
            propertyValueOld: this.activeElement.Item,
            propertyValueNew: data.data.item,
            propertyName: 'Item',
          },
        ]);

        this.activeElement.Topic = data.data.topic;
        this.activeElement.Item = data.data.item;
      }
    });

    return await modal.present();
  }

  /**
   * clear databinding
   *
   * @memberof SbDataBinding
   */
  public clear() {
    this.historyService.addPropertyChange(this.activeElement, [
      {
        propertyValueOld: this.activeElement.Topic,
        propertyValueNew: null,
        propertyName: 'Topic',
      },
      {
        propertyValueOld: this.activeElement.Item,
        propertyValueNew: null,
        propertyName: 'Item',
      },
    ]);
    this.activeElement.Topic = null;
    this.activeElement.Item = null;
  }
}
