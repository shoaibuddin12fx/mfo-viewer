import {
  AfterViewChecked,
  Component,
  Input,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ChangeDetectorRef } from '@angular/core';
import { DatabindingService } from '@services/databinding.service';

/**
 * databinding dialog component
 *
 * @export
 * @class DbDialogComponent
 * @implements {AfterViewChecked}
 */
@Component({
  selector: 'app-db-dialog',
  templateUrl: './db-dialog.component.html',
  styleUrls: ['./db-dialog.component.scss'],
})
export class DbDialogComponent implements AfterViewChecked, AfterViewInit {
  /**
   *
   * databinding topic
   * @type {*}
   * @memberof DbDialogComponent
   */
  @Input() public Topic: string;

  /**
   * databinding item
   *
   * @type {string}
   * @memberof DbDialogComponent
   */
  @Input() public Item: string;

  /**
   * contains the topics and their items in a structured array
   *
   * @type {[]}
   * @memberof DbDialogComponent
   */
  public topicStructure: Array<any>;

  /**
   *
   * search string property
   * @type {string}
   * @memberof DbDialogComponent
   */
  public searchString: string;

  /**
   *
   * focused property
   * @type {boolean}
   * @memberof DbDialogComponent
   */
  public focused = false;

  /**
   * Array with all topics that have item(s) matching the searchstring
   *
   * @type {Array<string>}
   * @memberof DbDialogComponent
   */
  public innerMatches: Array<string>;
  /**
   * the initial topic, needed in case topic is a subtopic, so sublist needs to be expanden on init
   *
   * @type {string}
   * @memberof DbDialogComponent
   */
  public initTopic: string;

  /**
   * Creates an instance of DbDialogComponent.
   * @param {ModalController} modalController
   * @param {ChangeDetectorRef} changeDetectorRef
   * @param {DatabindingService} dataBinding
   * @memberof DbDialogComponent
   */
  constructor(
    private modalController: ModalController,
    private changeDetectorRef: ChangeDetectorRef,
    public dataBinding: DatabindingService
  ) {
    this.topicStructure = dataBinding.getTopicStructure();
  }

  /**
   *
   * close modal and pass selected topic and item
   * @public
   * @memberof DbDialogComponent
   */
  public confirm() {
    this.modalController.dismiss({
      topic: this.Topic,
      item: this.Item,
    });
  }

  /**
   * set topic to the selected one
   *
   * @public
   * @param {string} topic
   * @memberof DbDialogComponent
   */
  public setTopic(topic: string, event?: Event) {
    this.Topic = topic;
    this.Item = '';
    if (event) {
      event.stopPropagation();
    }
  }

  /**
   * close dialog
   *
   * @public
   * @memberof DbDialogComponent
   */
  public close() {
    this.modalController.dismiss();
  }

  /**
   * set item to the selected one
   *
   * @public
   * @param {string} item
   * @memberof DbDialogComponent
   */
  public setItem(item: string) {
    this.Item = item;
  }

  /**
   * clear search input
   *
   * @public
   * @memberof DbDialogComponent
   */
  public clearSearch(ref: HTMLInputElement) {
    ref.value = '';
    this.searchString = '';
  }

  /**
   * onfocus function
   *
   * @memberof DbDialogComponent
   */
  public onFocus(ref: HTMLInputElement) {
    this.focused = true;
    ref.placeholder = '';
  }

  /**
   * onblur function
   *
   * @param {HTMLInputElement} ref
   * @memberof DbDialogComponent
   */
  public onBlur(ref: HTMLInputElement) {
    this.focused = false;
    ref.placeholder = 'Suchtext';
  }

  /**
   * show subtopics
   *
   * @param {Event} event
   * @memberof DbDialogComponent
   */
  public showSubTopics(event: Event) {
    const el = event.currentTarget as HTMLElement;
    el.hasAttribute('data-show')
      ? el.removeAttribute('data-show')
      : el.setAttribute('data-show', '');
    event.stopPropagation();
    this.initTopic = null;
  }

  /**
   * update the innerMatches Array whenever searchString changes
   *
   * @returns {void}
   * @memberof DbDialogComponent
   */
  public updateInnerMatches(): void {
    if (!this.searchString.length) {
      return;
    }
    this.innerMatches = [];

    this.topicStructure.forEach((topic) => {
      if (this.innerMatch(topic)) {
        this.innerMatches.push(topic.name);
      }
      topic.children.forEach((child) => {
        if (this.innerMatch(child)) {
          this.innerMatches.push(child.name);
        }
      });
    });
  }

  /**
   * check if topic contains items that match the searchstring
   *
   * @private
   * @param {*} topic
   * @returns {boolean}
   * @memberof DbDialogComponent
   */
  private innerMatch(topic: any): boolean {
    if (!topic.name.toLowerCase().includes(this.searchString.toLowerCase())) {
      for (let i = 0; i < topic.items.length; i++) {
        if (
          topic.items[i].toLowerCase().includes(this.searchString.toLowerCase())
        ) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * ngAfterViewInit
   *
   * @memberof DbDialogComponent
   */
  ngAfterViewInit(): void {
    this.initTopic = this.Topic.split('.')[0];
    console.log(this.initTopic);
  }

  /**
   * ngAfterViewChecked
   *
   * @memberof MfoContainerComponent
   */
  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }
}
