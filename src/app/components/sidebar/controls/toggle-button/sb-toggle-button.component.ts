import { Component, Input } from '@angular/core';
import { HistoryService } from '@services/history.service';

/**
 * toggle button for sidebar properties
 *
 * @export
 * @class SbToggleButton
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-toggle-button',
  templateUrl: './sb-toggle-button.component.html',
  styleUrls: ['./sb-toggle-button.component.scss'],
})
export class SbToggleButton {
  constructor(private history: HistoryService) {}

  /**
   * reference to activeElement
   *
   * @memberof SbToggleButton
   */
  @Input() activeElement;

  /**
   * property
   *
   * @type {string}
   * @memberof SbToggleButton
   */
  @Input() property: string;

  /**
   * imagepath for togglebutton
   *
   * @type {string}
   * @memberof SbToggleButton
   */
  @Input() imageSrc: string;

  /**
   * optional value to set on button click
   *
   * @type {string}
   * @memberof SbToggleButton
   */
  @Input() value: string;

  /**
   * return value
   *
   * @returns {boolean}
   * @memberof SbToggleButton
   */
  public getValue(): boolean {
    if (this.value) {
      return this.activeElement[this.property]
        ? this.activeElement[this.property] === this.value
        : false;
    } else {
      return this.activeElement[this.property] || false;
    }
  }

  /**
   * toggle value
   *
   * @returns
   * @memberof SbToggleButton
   */
  public toggle() {
    let oldVal;
    if (this.value) {
      oldVal = this.activeElement[this.property];
      this.activeElement[this.property] = this.value;
    } else {
      oldVal = this.getValue();
      this.activeElement[this.property] = !this.getValue();
    }

    this.addHistoryChange(oldVal);
  }

  /**
   * history change for toggles
   *
   * @private
   * @param {(string | boolean)} oldValue
   * @memberof SbToggleButton
   */
  private addHistoryChange(oldValue: string | boolean) {
    this.history.addPropertyChange(this.activeElement, [
      {
        propertyName: this.property,
        propertyValueOld: oldValue,
        propertyValueNew: this.activeElement[this.property],
      },
    ]);
  }
}
