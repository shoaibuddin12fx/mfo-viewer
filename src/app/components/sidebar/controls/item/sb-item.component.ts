import { Component, Input } from '@angular/core';

/**
 * wrapping component for sidebar components
 *
 * @export
 * @class SbItem
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-item',
  templateUrl: './sb-item.component.html',
  styleUrls: ['./sb-item.component.scss'],
})
export class SbItem {
  /**
   * caption (headline) of item
   *
   * @type {string}
   * @memberof SbItem
   */
  @Input() caption: string;

  /**
   * Creates an instance of SbItem.
   * @memberof SbItem
   */
  constructor() {}
}
