import { Component, Input } from '@angular/core';
import { HistoryService } from '@services/history.service';

/**
 * number input component for sidebar properties
 *
 * @export
 * @class SbNumberInput
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-number-input',
  templateUrl: './sb-number-input.component.html',
  styleUrls: ['./sb-number-input.component.scss'],
})
export class SbNumberInput {
  /**
   * Creates an instance of SbNumberInput.
   * @param {HistoryService} history
   * @memberof SbNumberInput
   */
  constructor(private history: HistoryService) {}

  /**
   * label in front of input [optional]
   *
   * @type {string}
   * @memberof SbNumberInput
   */
  @Input() label: string;

  /**
   * reference to active selected element
   *
   * @type {*}
   * @memberof SbNumberInput
   */
  @Input() activeElement: any;

  /**
   * property as string
   *
   * @type {string}
   * @memberof SbNumberInput
   */
  @Input() property: string;

  /**
   * increases value by 1
   *
   * @memberof SbNumberInput
   */
  public increaseVal(): void {
    this.onChange(this.activeElement[this.property] + 1);
  }

  /**
   * decreases value by 1
   *
   * @memberof SbNumberInput
   */
  public decreaseVal(): void {
    this.onChange(this.activeElement[this.property] - 1);
  }

  /**
   * sets value for activeElement on input
   *
   * @param {*} val
   * @memberof SbNumberInput
   */
  public onChange(val: any): void {
    // block other changes, only on input triggers
    if (this.activeElement[this.property] === val) {
      return;
    }

    if (this.activeElement[this.property] !== undefined) {
      const oldval = this.activeElement[this.property];
      let n: number = +val;
      this.activeElement[this.property] = n;
      this.history.addPropertyChange(this.activeElement, [
        {
          propertyName: this.property,
          propertyValueOld: oldval,
          propertyValueNew: n,
        },
      ]);
    }
  }

  /**
   * gets displayed value and rounds it since we dont need decimals
   *
   * @returns
   * @memberof SbNumberInput
   */
  public getDisplayValue() {
    return this.activeElement[this.property] !== undefined
      ? Math.round(this.activeElement[this.property])
      : '';
  }
}
