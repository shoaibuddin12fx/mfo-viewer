import { Component } from '@angular/core';

/**
 * text component for sidebar
 *
 * @export
 * @class SbText
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-text',
  templateUrl: './sb-text.component.html',
  styleUrls: ['./sb-text.component.scss'],
})
export class SbText {
  /**
   * Creates an instance of SbText.
   * @memberof SbText
   */
  constructor() {}
}
