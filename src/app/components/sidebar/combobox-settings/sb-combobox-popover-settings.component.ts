import { Component } from "@angular/core";
import { NavParams, PopoverController } from "@ionic/angular";

/**
 * popover component for sidebar elements with selectable properties
 *
 * @export
 * @class SbComboboxPopover
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: "sb-combobox-popover-settings",
  templateUrl: "./sb-combobox-popover-settings.component.html",
  styleUrls: ["./sb-combobox-popover-settings.component.scss"],
})
export class SbComboboxPopoverSettings {
  /**
   * items to choose from
   *
   * @type {Array<any>}
   * @memberof SbComboboxPopover
   */
  public items: Array<any>;

  /**
   * propertyname of value to display of object
   *
   * @type {string}
   * @memberof SbComboboxPopover
   */
  public captionvalue: string;

  /**
   * selected of list
   *
   * @type {*}
   * @memberof SbComboboxPopover
   */
  public selected: any;

  /**
   * Creates an instance of SbComboboxPopover.
   * @param {PopoverController} popoverCtrl
   * @param {NavParams} navparams
   * @memberof SbComboboxPopover
   */
  constructor(
    private popoverCtrl: PopoverController,
    private navparams: NavParams
  ) {
    this.items = this.navparams.get("items");
    this.captionvalue = this.navparams.get("captionvalue");
    this.selected = this.navparams.get("selected");
  }

  /**
   * item on click
   *
   * @param {*} item
   * @memberof SbComboboxPopover
   */
  public selectItem(item) {
    this.popoverCtrl.dismiss({ item });
  }
}
