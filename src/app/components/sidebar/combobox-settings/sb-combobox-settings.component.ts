import { AfterViewInit, Component, EventEmitter, Input, Output } from '@angular/core';
import { IPropChangeItem } from '@interfaces/historyobject';
import { PopoverController } from '@ionic/angular';
import { FontService } from '@services/font.service';
import { HistoryService } from '@services/history.service';
import { Utils } from 'src/app/dataformat/util';
import { SbComboboxPopoverSettings } from './sb-combobox-popover-settings.component';

/**
 *
 *
 * @export
 * @class SbCombobox
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sb-combobox-settings',
  templateUrl: './sb-combobox-settings.component.html',
  styleUrls: ['./sb-combobox-settings.component.scss'],
})
export class SbComboboxSettings implements AfterViewInit {
  /**
   * reference to activeElement
   *
   * @memberof SbCombobox
   */
  @Input() activeElement;

  @Input() settings: any;

  /**
   * property
   *
   * @type {string}
   * @memberof SbCombobox
   */
  @Input() property: string;

  /**
   * items to choose from
   *
   * @type {Array<any>}
   * @memberof SbCombobox
   */
  @Input() items: Array<any>;

  /**
   * reference to property of objects to display
   *
   * @type {string}
   * @memberof SbCombobox
   */
  @Input() captionvalue: string;

  /**
   * show open textedit button [default = false]
   *
   * @type {boolean}
   * @memberof SbCombobox
   */
  @Input() showEditButton: boolean = false;

  /**
   * type, can be used for special cases
   *
   * @type {string}
   * @memberof SbCombobox
   */
  @Input() type: string = '';

  /**
   * default value to display
   *
   * @type {*}
   * @memberof SbCombobox
   */
  @Input() default: any;

  @Input() selectedValue: any;

  /**
   * open textarea on click [only if showEditButton = true]
   *
   * @memberof SbCombobox
   */
  @Output() openTextareaFunction = new EventEmitter<string>();

  /**
   * Creates an instance of SbCombobox.
   * @param {PopoverController} popoverCtrl
   * @param {HistoryService} history
   * @memberof SbCombobox
   */
  constructor(
    private popoverCtrl: PopoverController,
    private history: HistoryService,
    private fontService: FontService,
  ) {
  
  }

  ngAfterViewInit(): void {
    
    // find default font if settings are set 

    if(this.type == 'settings' && this.property == 'SettingsFontFamily'){
      this.fontService.getDefaultFont().then( v => {
        console.log(v);
        this.selectedValue = v.Family;
      });
    }
    
  }

  /**
   * gets fieldtype
   *
   * @returns
   * @memberof SbCombobox
   */
  public getFieldType() {
    if (this.activeElement.IsDateInput) {
      return 'Datum';
    }

    if (this.activeElement.Number) {
      return 'Nummer';
    }

    return 'Text';
  }

  /**
   * sets multiple values for activeElement
   *
   * @private
   * @param {Array<any>} valuesToSet
   * @memberof SbCombobox
   */
  private setActiveElementValues(valuesToSet: Array<any>) {
    valuesToSet.forEach((setval) => {
      const key = Object.keys(setval)[0];
      const value = Object.values(setval)[0];
      this.activeElement[key] = value;
    });
  }

  /**
   * opens popover
   *
   * @param {Event} event
   * @memberof SbCombobox
   */
  public async openSelectPopover(event: Event) {
    let selected = this.activeElement[this.property];

    // fieldtype
    if (this.type === 'fieldtype') {
      selected = this.getFieldType();
    }

    const popover = await this.popoverCtrl.create({
      component: SbComboboxPopoverSettings,
      componentProps: {
        items: this.items,
        selected,
        captionvalue: this.captionvalue,
      },
      backdropDismiss: true,
      showBackdrop: false,
      event,
      cssClass: 'comboboxpopover',
    });

    await popover.present();

    // get new opened popover
    let popoverElement = document.getElementsByClassName(
      'popover-content'
    )[0] as HTMLElement;

    // override style "left"
    const left: number = +parseFloat(popoverElement.style.left);
    popoverElement.style.left = left + 5 + 'px';

    // override style "top"
    const top: number = +parseFloat(popoverElement.style.top);
    popoverElement.style.top = top + 4 + 'px';

    // on close popover
    popover.onDidDismiss().then((respData) => {
      console.log(respData, this.activeElement, this.type);
      if (respData && respData.data?.item) {
        const respItem = respData.data.item;
        if(this.type !== '' || !this.type ){
          this.selectedValue = respItem[this.captionvalue];
        }

        if(this.type == 'settings' && this.property == 'SettingsFontFamily'){
          this.fontService.setDefaultFont(this.selectedValue);
        }

        if (respItem.valuesToSet) {
          // this.addHistoryChange(respItem.valuesToSet);
          return this.setActiveElementValues(respItem.valuesToSet);
        }

        // check if respItem has property value
        if (respItem.value) {
          this.addHistoryChange(respItem.value);
          this.activeElement[this.property] = respItem.value;
        } else {
          // this.addHistoryChange(respItem[this.captionvalue]);
          this.activeElement[this.property] = respItem[this.captionvalue];
        }
        
      }
    });
  }

  /**
   * opens textarea
   *
   * @memberof SbCombobox
   */
  public openTextarea() {
    this.openTextareaFunction.emit();
  }

  /**
   * add changes to history
   *
   * @private
   * @param {*} values
   * @memberof SbCombobox
   */
  private addHistoryChange(values: any) {
    const changes: Array<IPropChangeItem> = [];

    if (typeof values === 'string') {
      const oldVal = this.activeElement[this.property];
      const newVal = values;

      changes.push({
        propertyName: this.property,
        propertyValueNew: newVal,
        propertyValueOld: oldVal,
      });
    } else if (typeof values === 'object') {
      // check if array
      if (Array.isArray(values)) {
        values.forEach((change) => {
          const key = Object.keys(change)[0];
          const value = Object.values(change)[0];
          const oldVal = this.activeElement[key] || false;
          const newVal = value;

          changes.push({
            propertyName: key,
            propertyValueOld: oldVal,
            propertyValueNew: newVal,
          });
        });
      }
    }

    if (changes.length > 0) {
      this.history.addPropertyChange(this.activeElement, changes);
    } else {
      Utils.warn('Unknown propertychangetype combobox!', values);
    }
  }
}
