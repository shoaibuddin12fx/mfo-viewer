import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { InputModalComponent } from '@components/sidebar/input-modal/input-modal.component';
import { AlertController, ModalController } from '@ionic/angular';
import { DatabindingService } from '@services/databinding.service';
import { DateformaterService } from '@services/dateformater.service';
import { FontService } from '@services/font.service';
import { HistoryService } from '@services/history.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { Utils } from 'src/app/dataformat/util';
import { environment } from '../../../environments/environment';
import { AppConfig } from '../../app.config';

/**
 * a component that displays and enables to change the properties of a mfo element
 *
 * @export
 * @class SidebarComponent
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  /**
   * Mode of the editor
   *
   * @type {ViewMode}
   * @memberof MfoContainerComponent
   */
  @Input() Mode: ViewMode;

  /**
   * formular options
   *
   * @type {*}
   * @memberof SidebarComponent
   */
  @Input() FormularOptions: any;

  /**
   * export event emitter
   *
   * @memberof SidebarComponent
   */
  @Output() export = new EventEmitter<string>();

  /**
   * import event emitter
   *
   * @memberof SidebarComponent
   */
  @Output() import = new EventEmitter();

  // @ViewChild('Align') Align: TemplateRef<any>;
  // @ViewChild('BorderColor') BorderColor: TemplateRef<any>;
  // @ViewChild('BorderWidth') BorderWidth: TemplateRef<any>;
  // @ViewChild('Color') Color: TemplateRef<any>;
  // @ViewChild('FontCharset') FontCharset: TemplateRef<any>;
  // @ViewChild('FontColor') FontColor: TemplateRef<any>;
  // @ViewChild('FontFamily') FontFamily: TemplateRef<any>;
  // @ViewChild('FontSize') FontSize: TemplateRef<any>;
  // @ViewChild('FontStyle') FontStyle: TemplateRef<any>;
  // @ViewChild('Height') Height: TemplateRef<any>;
  // @ViewChild('OffsetX') OffsetX: TemplateRef<any>;
  // @ViewChild('OffsetY') OffsetY: TemplateRef<any>;
  // @ViewChild('Print') Print: TemplateRef<any>;
  // @ViewChild('Text') Text: TemplateRef<any>;
  // @ViewChild('Type') Type: TemplateRef<any>;
  // @ViewChild('Width') Width: TemplateRef<any>;
  // @ViewChild('X') X: TemplateRef<any>;
  // @ViewChild('Y') Y: TemplateRef<any>;
  // @ViewChild('X2') X2: TemplateRef<any>;
  // @ViewChild('Y2') Y2: TemplateRef<any>;
  // @ViewChild('Image') Image: TemplateRef<any>;
  // @ViewChild('Multiline') Multiline: TemplateRef<any>;
  // @ViewChild('Number') Number: TemplateRef<any>;
  // @ViewChild('TabIndex') TabIndex: TemplateRef<any>;
  // @ViewChild('Connect') Connect: TemplateRef<any>;
  // @ViewChild('BarcodeType') BarcodeType: TemplateRef<any>;
  // @ViewChild('IsChecked') IsChecked: TemplateRef<any>;

  /**
   * reference to image upload input element
   *
   * @type {ElementRef}
   * @memberof SidebarComponent
   */
  @ViewChild('imageInput', { read: ElementRef, static: false })
  imageInput: ElementRef;

  /**
   * reference to file input for document import
   *
   * @type {ElementRef}
   * @memberof SidebarComponent
   */
  @ViewChild('importInput', { read: ElementRef, static: false })
  importInput: ElementRef;

  /**
   * reference to file input for object import
   *
   * @type {ElementRef}
   * @memberof SidebarComponent
   */
  @ViewChild('objectInput', { read: ElementRef, static: false })
  objectInput: ElementRef;

  /**
   * Creates an instance of SidebarComponent.
   * @param {AppConfig} config
   * @param {AlertController} alertController
   * @param {FontService} fontService
   * @param {DatabindingService} dataBinding
   * @param {DateformaterService} dateFormater
   * @param {ElementRef} hostRef
   * @param {ModalController} modalController
   * @param {HistoryService} history
   * @memberof SidebarComponent
   */
  constructor(
    public config: AppConfig,
    public alertController: AlertController,
    public fontService: FontService,
    public dataBinding: DatabindingService,
    public dateFormater: DateformaterService,
    public hostRef: ElementRef,
    private modalController: ModalController,
    private history: HistoryService
  ) {}

  /**
   * reference to the currently selected element in the treeview
   *
   * @type {object}
   * @memberof SidebarComponent
   */
  public activeElement: any;

  /**
   * fieldtypes for selectable combobox
   *
   * @memberof SidebarComponent
   */
  public fieldTypes = [
    {
      caption: 'Text',
      valuesToSet: [{ IsDateInput: false }, { Number: false }],
    },
    {
      caption: 'Nummer',
      valuesToSet: [{ IsDateInput: false }, { Number: true }],
    },
    {
      caption: 'Datum',
      valuesToSet: [{ IsDateInput: true }, { Number: false }],
    },
  ];

  /**
   * qrtypes for combobox
   *
   * @memberof SidebarComponent
   */
  public qrTypes = [
    { caption: 'QR-Code', value: 'QR' },
    { caption: 'PDF417', value: 'PDF417' },
  ];

  /**
   * using it prevent angular to recreate your DOM and it will keep track of changes
   *
   * @param {*} index
   * @return {*}
   * @memberof SidebarComponent
   */
  public trackByFn(index) {
    return index;
  }

  /**
   * updates value of Image property to input value from input dialog
   *
   * @param {File} file
   * @memberof SidebarComponent
   */
  public async changeImage(file: File) {
    const dataUrl = await new Promise((resolve) => {
      const reader = new FileReader();
      reader.onload = () => resolve(reader.result);
      reader.readAsDataURL(file);
    });
    this.activeElement.Image = dataUrl;
    this.imageInput.nativeElement.value = null;

    if (
      this.activeElement.Type === 'Image' ||
      this.activeElement.Type === 'MarkField'
    ) {
      const img = new Image();
      img.src = dataUrl as string;
      img.onload = () => {
        this.activeElement.Width = img.width;
        this.activeElement.Height = img.height;
      };
    }
  }

  /**
   * resets value of Image property and imageInput element
   *
   * @memberof SidebarComponent
   */
  public deleteImg() {
    this.activeElement.Image = '';
    this.imageInput.nativeElement.value = null;
  }

  /**
   * resolves tabindexes
   *
   * @memberof SidebarComponent
   */
  public resolveTIClick() {
    Utils.log('resolveClicked');
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('resolveTabIndexes', {
        bubbles: true,
      })
    );
  }

  /**
   * opens an inputdialog for given property
   *
   * @param {string} header
   * @param {string} property
   * @param {boolean} [isFormularOptions=false]
   * @memberof SidebarComponent
   */
  public async InputDialog(
    header: string,
    property: string,
    isFormularOptions: boolean = false
  ) {
    const modal = await this.modalController.create({
      component: InputModalComponent,
      componentProps: {
        title: header,
        value: isFormularOptions
          ? this.FormularOptions[property]
          : this.activeElement[property],
      },
      cssClass: 'inputModal',
      showBackdrop: false,
    });

    modal.onDidDismiss().then((data) => {
      if (data.data) {
        if (isFormularOptions) {
          this.FormularOptions[property] = data.data.newValue;
        } else {
          const oldVal = this.activeElement[property];
          this.activeElement[property] = data.data.newValue;
          this.history.addPropertyChange(this.activeElement, [
            {
              propertyName: property,
              propertyValueOld: oldVal,
              propertyValueNew: this.activeElement[property],
            },
          ]);
        }
      }
    });
    return await modal.present();
  }

  /**
   * trigger for changing view mode
   *
   * @memberof SidebarComponent
   */
  public changeViewModeTrigger(mode: ViewMode) {
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('changeViewMode', {
        detail: mode,
        bubbles: true,
      })
    );
  }

  /**
   * determines if property should be visible for activeElement
   *
   * @param {string} propertyname
   * @returns {boolean}
   * @memberof SidebarComponent
   */
  public displayProperty(propertyname: string): boolean {
    const type = this.activeElement.Type;
    let allowed = [];
    let disallowed = [];
    let propertycheck = [];

    switch (propertyname) {
      // Tabindex
      case 'TabIndex':
        disallowed = ['Line'];
        break;

      // Height
      case 'Height':
        disallowed = ['Line'];
        break;

      // Print
      case 'Print':
        disallowed = ['Line', 'Shape', 'Container', 'SignField'];
        break;

      // BackgroundColor
      case 'BackgroundColor':
        disallowed = ['Barcode', 'Image'];
        break;

      // Border generel
      case 'Border':
        disallowed = ['Line', 'Barcode', 'Image'];
        break;

      // Font generel
      case 'Font':
        allowed = ['Text', 'Input'];
        break;

      // Font extra props
      case 'FontExtraProps':
        allowed = ['Text', 'Input', 'MarkField', 'SignField'];
        break;

      // letterSpacing
      case 'LetterSpacing':
        allowed = ['Text', 'Input'];
        break;

      // LineSpacing
      case 'LineSpacing':
        propertycheck = [{ Multiline: true }];
        break;

      // TextEditOptions
      case 'TextEditOptions':
        allowed = ['Text', 'Input'];
        break;

      // DateFormat
      case 'DateFormat':
        propertycheck = [{ IsDateInput: true }];
        break;

      // CalcOnCreation
      case 'CalcOnCreation':
        propertycheck = [{ Formula: 'length>0' }];
        break;

      // CheckboxOptions
      case 'CheckboxOptions':
        allowed = ['CheckBox'];
        break;

      // LineExtraPos
      case 'LineExtraPos':
        allowed = ['Line'];
        break;

      // QRCodeOptions
      case 'QRCodeOptions':
        allowed = ['Barcode'];
        break;

      // PaintingOptions
      case 'PaintingOptions':
        allowed = ['Painting'];
        break;

      // MarkFieldOptions
      case 'MarkFieldOptions':
        allowed = ['MarkField'];
        break;

      // AssignmentDateRange
      case 'AssignmentDateRange':
        propertycheck = [{ DataKind: 'length>0' }];
        break;
    }

    // property check
    if (propertycheck.length > 0) {
      let propCheck = true;
      for (const prop of propertycheck) {
        const key = Object.keys(prop)[0];
        const val = Object.values(prop)[0];
        const propCheckType = typeof val;

        if (propCheck) {
          switch (propCheckType) {
            case 'boolean':
              propCheck =
                this.activeElement[key] && this.activeElement[key] === val;
              break;
            case 'string':
              if (val === 'length>0') {
                propCheck =
                  this.activeElement[key] && this.activeElement[key].length > 0;
                break;
              } else {
                Utils.warn('unknown propcheckable string');
                propCheck = false;
                break;
              }
            default:
              Utils.warn('unknown propertycheck type', propCheckType);
          }
        }
      }

      return propCheck;
    }

    // check if type in allowed
    if (allowed.length > 0 && allowed.includes(type)) {
      return true;
    }

    // check if in disallowed
    if (disallowed.length > 0 && disallowed.includes(type)) {
      return false;
    }

    return allowed.length === 0;
  }

  /**
   * get caption for assignmentsdaterange
   *
   * @param {*} value
   * @returns
   * @memberof SidebarComponent
   */
  public getAssignmentRangeCaption(value: any) {
    if (value) {
      let n: number = +value;
      return `Nur Aufträge der letzten ${n} Tage beachten`;
    } else {
      return 'Alle Aufträge beachten';
    }
  }

  /**
   * checks whether active element is a page
   *
   * @public
   * @returns {boolean}
   * @memberof SidebarComponent
   */
  public isPage(): boolean {
    if (
      this.activeElement.Type === 'Container' &&
      typeof this.activeElement.isPage === 'function'
    ) {
      return this.activeElement.isPage();
    } else {
      return false;
    }
  }

  /**
   * emit chosen object file and reset the input value
   *
   * @public
   * @memberof SidebarComponent
   */
  public importObject() {
    this.import.emit({
      File: this.objectInput.nativeElement.files[0],
      Type: 'Object',
    });
    this.objectInput.nativeElement.value = null;
  }
}
