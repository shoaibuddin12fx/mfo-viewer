import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

/**
 * modal dialog for text input
 *
 * @export
 * @class InputModalComponent
 */
@Component({
  selector: 'app-input-modal',
  templateUrl: './input-modal.component.html',
  styleUrls: ['./input-modal.component.scss'],
})
export class InputModalComponent {
  /**
   * title/header of the modal
   *
   * @type {string}
   * @memberof InputModalComponent
   */
  @Input() title: string;
  /**
   * value binding
   *
   * @type {string}
   * @memberof InputModalComponent
   */
  @Input() value: string;

  /**
   * Creates an instance of InputModalComponent.
   * @param {ModalController} modalController
   * @memberof InputModalComponent
   */
  constructor(public modalController: ModalController) {}

  /**
   *
   * close dialog
   * @public
   * @memberof InputModalComponent
   */
  public close() {
    this.modalController.dismiss();
  }

  /**
   *
   * close dialog and pass entered text
   * @public
   * @memberof InputModalComponent
   */
  public confirm() {
    this.modalController.dismiss({
      newValue: this.value,
    });
  }
}
