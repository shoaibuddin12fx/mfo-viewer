import { HostListener, Input, ElementRef, ViewChild } from '@angular/core';
import { HistoryService } from '@services/history.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { AppConfig } from '../../app.config';
import { MfoActiveComponent } from '../mfo-active/mfo-active.component';

/**
 * base class for all mfo objects
 *
 * @export
 * @class MfoBase
 */
export class MfoBase {
  /**
   * html element reference of active component
   *
   * @memberof MfoBase
   */
  @ViewChild(MfoActiveComponent, { read: ElementRef }) activeComponent;

  /**
   * base model element
   *
   * @type {*}
   * @memberof MfoBase
   */
  public element: any;

  /**
   * Path of current object
   *
   * @private
   * @type {string}
   * @memberof MfoBase
   */
  private _path: string;

  /**
   * setter for path
   *
   * @memberof MfoBase
   */
  @Input() public set path(val: string) {
    this._path = val;
    this.element.Path = val;
    this.element.uiElement = this;
  }

  /**
   * getter for path
   *
   * @readonly
   * @type {string}
   * @memberof MfoBase
   */
  public get path(): string {
    return this._path;
  }

  /**
   * Mode of the editor
   *
   * @type {ViewMode}
   * @memberof MfoContainerComponent
   */
  @Input() Mode: ViewMode;

  /**
   * temporary cache variable
   *
   * @private
   * @memberof MfoBase
   */
  private oldVal = '';

  /**
   * Creates an instance of MfoBase.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @memberof MfoBase
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {}

  /**
   * event handler for click
   *
   * @param {MouseEvent} event
   * @memberof MfoBase
   */
  @HostListener('click', ['$event'])
  public click(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();

    // editor is not in editview
    if (this.Mode == ViewMode.Interactive || this.Mode == ViewMode.Preview) {
      // call dblclick function on single click for Inputs
      if (this.element.Type === 'Input') {
        this.dblclick(event);
      }

      // close possible open calendar
      (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
        new CustomEvent('closecalendar', {
          bubbles: true,
        })
      );
    }

    this.hostRef.nativeElement.focus();
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('activate', {
        bubbles: true,
        detail: { path: this.path },
      })
    );
  }

  /**
   * event handler for context menu activities
   *
   * @param {MouseEvent} event
   * @memberof MfoBase
   */
  @HostListener('contextmenu', ['$event'])
  public contextmenu(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.hostRef.nativeElement.focus();
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('activate', {
        bubbles: true,
        detail: { path: this.path },
      })
    );

    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('context', {
        bubbles: true,
        detail: { path: this.path, pageX: event.pageX, pageY: event.pageY },
      })
    );

    // editor is not in editview
    if (this.Mode == ViewMode.Interactive || this.Mode == ViewMode.Preview) {
      // date input
      if (this.element.Type === 'Input' && this.element.IsDateInput) {
        (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
          new CustomEvent('showcalendar', {
            bubbles: true,
            detail: { element: this.element },
          })
        );
      } else {
        new CustomEvent('context', {
          bubbles: true,
          detail: {
            path: this.path,
            pageX: event.pageX,
            pageY: event.pageY,
            element: this.element,
          },
        });
      }
    }
  }

  /**
   * event handler for double click activities
   *
   * @param {MouseEvent} event
   * @memberof MfoBase
   */
  @HostListener('dblclick', ['$event'])
  dblclick(event: MouseEvent) {
    this.element.IsDoubleClicked = true;
    this.hostRef.nativeElement.focus();
  }

  /**
   * event handler for blurring
   * adds history entry
   * @param {MouseEvent} event
   * @memberof MfoBase
   */
  @HostListener('blur', ['$event'])
  public blur(event: MouseEvent) {
    this.element.IsDoubleClicked = false;

    if (this.oldVal !== this.element.Text) {
      this.history.addPropertyChange(this.element, [
        {
          propertyName: 'Text',
          propertyValueNew: this.element.Text,
          propertyValueOld: this.oldVal,
        },
      ]);
    }
  }

  /**
   * event handler for focus
   * sets oldvalue to element.Text
   * @param {MouseEvent} event
   * @memberof MfoBase
   */
  @HostListener('focus', ['$event'])
  public focus(event: MouseEvent) {
    this.oldVal = this.element.Text;
  }
}
