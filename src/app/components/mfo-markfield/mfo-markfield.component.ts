import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  ViewChild,
} from '@angular/core';
import { HistoryService } from '@services/history.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { AppConfig } from '../../app.config';
import { MarkField } from '../../dataformat/models/index';
import { MfoBase } from '../mfo-base/mfo-base';
import { HistoryAction } from 'src/app/dataformat/enums/historyactions.enum';

/**
 * markfield component to let the user input marks on a canvas
 *
 * @export
 * @class MfoSignfieldComponent
 * @extends {MfoBase}
 * @implements {AfterViewInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-markfield',
  templateUrl: './mfo-markfield.component.html',
})
export class MfoMarkfieldComponent extends MfoBase implements AfterViewInit {
  /**
   * element context
   *
   * @type {MarkField}
   * @memberof MfoMarkfieldComponent
   */
  @Input() element: MarkField;

  /**
   * canvas reference
   *
   * @type {ElementRef}
   * @memberof MfoMarkfieldComponent
   */
  @ViewChild('Canvas', { read: ElementRef }) canvas: ElementRef;

  /**
   * context reference for canvas
   *
   * @private
   * @type {CanvasRenderingContext2D}
   * @memberof MfoMarkfieldComponent
   */
  private canvasContext: CanvasRenderingContext2D;

  /**
   *
   * x-coordinate of last mark
   * @private
   * @type {number}
   * @memberof MfoMarkfieldComponent
   */
  private lastX: number;

  /**
   *
   * y-coordinate of last mark
   * @private
   * @type {number}
   * @memberof MfoMarkfieldComponent
   */
  private lastY: number;

  /**
   * position binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoMarkfieldComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * display binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoMarkfieldComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * width binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoMarkfieldComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * border-style binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoMarkfieldComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    let borderStyle = 'none';

    if (this.Mode === 0) {
      borderStyle = 'dotted';
    }

    if (this.element.BorderWidth) {
      borderStyle = 'inset';
    }

    return borderStyle;
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === 0
      ? 'grey'
      : 'transparent';
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === 0
      ? 1
      : 0;
  }

  /**
   * background-image binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoMarkfieldComponent
   */
  @HostBinding('style.backgroundImage') get backgroundImage(): string {
    if (this.element.Image) {
      return 'url(' + this.element.Image + ')';
    }
  }

  /**
   * background-size binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoMarkfieldComponent
   */
  @HostBinding('style.backgroundSize') get beckgroundImageSize(): string {
    return '100% 100%';
  }

  /**
   * background-color binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoMarkfieldComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return this.element.Color ? this.element.Color : 'white';
  }

  /**
   * Creates an instance of MfoMarkfieldComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @memberof MfoMarkfieldComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }

  /**
   * trigger updateImage method in editor
   *
   * @param {MouseEvent} event
   * @memberof MfoMarkfieldComponent
   */
  @HostListener('dblclick', ['$event'])
  private loadImage(event: MouseEvent) {
    if (this.Mode > 0) {
      return;
    }
    event.preventDefault();
    event.stopPropagation();
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('loadImage', {
        bubbles: true,
      })
    );
    this.lastX = null;
    this.lastY = null;
  }

  /**
   * reset canvas and draw all marks
   *
   * @private
   * @param {string} [path]
   * @returns
   * @memberof MfoMarkfieldComponent
   */
  @HostListener('window:dataLoaded', ['$event'])
  private drawMarks(event?: CustomEvent) {
    if (event && event.detail.path !== this.path) {
      return;
    }
    event.stopPropagation();

    this.canvasContext.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );

    this.element.Marks.forEach((mark) => {
      this.drawMark(mark.X, mark.Y);
    });
  }

  /**
   * add new Mark at cursor position
   *
   * @param {MouseEvent} event
   * @memberof MfoMarkfieldComponent
   */
  @HostListener('click', ['$event'])
  private newMark(event: MouseEvent) {
    if (
      !(this.Mode === ViewMode.Interactive || this.Mode === ViewMode.Preview)
    ) {
      return;
    }
    this.drawMark(event.offsetX, event.offsetY);

    let oldval = [...this.element.Marks];
    this.element.Marks.push({ X: this.lastX, Y: this.lastY });

    this.history.addUndo({
      action: HistoryAction['PropertyChange'],
      element: this.element,
      changes: [
        {
          propertyName: 'Marks',
          propertyValueOld: oldval,
          propertyValueNew: [...this.element.Marks],
        },
      ],
    });
  }

  /**
   * draw new mark on canvas
   *
   * @private
   * @param {number} X
   * @param {number} Y
   * @memberof MfoMarkfieldComponent
   */
  private drawMark(X: number, Y: number) {
    this.canvasContext.strokeStyle = this.element.FontColor;

    this.canvasContext.beginPath();
    this.canvasContext.moveTo(X - 5, Y - 5);
    this.canvasContext.lineTo(X + 5, Y + 5);
    this.canvasContext.moveTo(X - 5, Y + 5);
    this.canvasContext.lineTo(X + 5, Y - 5);

    if (this.element.Connect && this.lastX) {
      this.canvasContext.moveTo(this.lastX, this.lastY);
      this.canvasContext.lineTo(X, Y);
    }
    this.canvasContext.stroke();
    this.canvasContext.closePath();

    this.lastX = X;
    this.lastY = Y;
  }

  /**
   * set canvas context
   *
   * @memberof MfoMarkfieldComponent
   */
  ngAfterViewInit(): void {
    this.canvasContext = (
      this.canvas.nativeElement as HTMLCanvasElement
    ).getContext('2d');
  }
}
