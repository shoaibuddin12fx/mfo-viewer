import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  ViewChild,
} from '@angular/core';
import { MfoBase } from '@components/mfo-base/mfo-base';
import { IPoint } from '@interfaces/point';
import { HistoryService } from '@services/history.service';
import { AppConfig } from 'src/app/app.config';
import { HistoryAction } from 'src/app/dataformat/enums/historyactions.enum';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { SignField } from 'src/app/dataformat/models';
import { Painting } from 'src/app/dataformat/models/paint.model';

/**
 * component to let the user paint strokes on a canvas
 *
 * @export
 * @class MfoPaintingComponent
 * @extends {MfoBase}
 * @implements {AfterViewInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-paint',
  templateUrl: './mfo-paint.component.html',
  styleUrls: ['./mfo-paint.component.scss'],
})
export class MfoPaintingComponent extends MfoBase implements AfterViewInit {
  /**
   * element context
   *
   * @type {SignField}
   * @memberof MfoPaintingComponent
   */
  @Input() element: Painting;

  /**
   * canvas reference
   *
   * @type {ElementRef}
   * @memberof MfoPaintingComponent
   */
  @ViewChild('Canvas', { read: ElementRef }) canvas: ElementRef;

  /**
   *
   * canvas context
   * @private
   * @type {CanvasRenderingContext2D}
   * @memberof MfoPaintingComponent
   */
  private canvasContext: CanvasRenderingContext2D;

  /**
   * position binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoPaintingComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * display binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoPaintingComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * width binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoPaintingComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * border-style binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoPaintingComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    let borderStyle = 'none';

    if (this.Mode === 0) {
      borderStyle = 'dotted';
    }

    if (this.element.BorderWidth) {
      borderStyle = 'inset';
    }

    return borderStyle;
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === 0
      ? 'grey'
      : 'transparent';
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === 0
      ? 1
      : 0;
  }

  /**
   * background-color binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoPaintingComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return this.element.Color ? this.element.Color : 'white';
  }

  /**
   * background-image binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoPaintingComponent
   */
  @HostBinding('style.backgroundImage') get backgroundImage(): string {
    if (this.element.Image) {
      return 'url(' + this.element.Image + ')';
    }
  }

  /**
   * background-size binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoPaintingComponent
   */
  @HostBinding('style.backgroundSize') get beckgroundImageSize(): string {
    return '100% 100%';
  }

  /**
   * Creates an instance of MfoPaintingComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @memberof MfoPaintingComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }

  /**
   * set hold to true
   *
   * @private
   * @memberof MfoPaintingComponent
   */
  @HostListener('mousedown', ['$event'])
  public mouseDown(event: MouseEvent) {
    if (this.Mode === ViewMode.Interactive || this.Mode === ViewMode.Preview) {
      this.canvasContext.lineWidth =
        this.element.PenSize > 0 ? this.element.PenSize + 2 : 3;
      this.canvasContext.strokeStyle = this.element.FontColor;
      this.element.Strokes.push({
        Coordinates: [{ X: event.offsetX, Y: event.offsetY }],
      });
      this.canvasContext.moveTo(event.offsetX * 2, event.offsetY * 2);
    }
  }

  /**
   * draw line between last 2 points
   *
   * @private
   * @memberof MfoPaintingComponent
   */
  @HostListener('mousemove', ['$event'])
  public mouseMove(event: MouseEvent) {
    if (
      (this.Mode === ViewMode.Interactive || this.Mode == ViewMode.Preview) &&
      event.buttons
    ) {
      this.element.Strokes[this.element.Strokes.length - 1].Coordinates.push({
        X: event.offsetX,
        Y: event.offsetY,
      });
      this.canvasContext.lineTo(event.offsetX * 2, event.offsetY * 2);

      this.canvasContext.stroke();
    }
  }

  /**
   * pushes new undo into the history
   *
   * @private
   * @memberof MfoPaintingComponent
   */
  @HostListener('mouseup')
  private mouseUp() {
    let oldval = [...this.element.Strokes];
    oldval.pop();

    this.history.addUndo({
      action: HistoryAction['PropertyChange'],
      element: this.element,
      changes: [
        {
          propertyName: 'Strokes',
          propertyValueOld: oldval,
          propertyValueNew: [...this.element.Strokes],
        },
      ],
    });
  }

  /**
   * trigger updateImage method in editor
   *
   * @param {MouseEvent} event
   * @memberof MfoPaintingComponent
   */
  @HostListener('dblclick', ['$event'])
  public loadImage(event: MouseEvent) {
    if (this.Mode > 0) {
      return;
    }
    event.preventDefault();
    event.stopPropagation();
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('loadImage', {
        bubbles: true,
      })
    );
    this.element.Strokes = [];
  }

  /**
   * resets the canvas
   *
   * @memberof MfoPaintingComponent
   */
  public clearCanvas() {
    this.canvasContext.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
    this.canvasContext.beginPath();
    this.element.Strokes = [];
  }

  /**
   * set canvas context
   *
   * @memberof MfoPaintingComponent
   */
  ngAfterViewInit(): void {
    this.canvasContext = (
      this.canvas.nativeElement as HTMLCanvasElement
    ).getContext('2d');
  }

  /**
   * raised after a data package was received and properties in model has been refreshed and to paint all paths to canvas
   *
   * @memberof MfoPaintingComponent
   */
  @HostListener('window:dataLoaded', ['$event'])
  public paintPath(event: CustomEvent) {
    if (event && event.detail.path !== this.path) {
      return;
    }
    console.log('DATA LOADED', this);
    let strokeDemo = [
      {
        Coordinates: [
          { X: 1, Y: 1 },
          { X: 1, Y: 10 },
          { X: 20, Y: 10 },
          { X: 20, Y: 20 },
        ],
      },
      {
        Coordinates: [
          { X: 50, Y: 1 },
          { X: 50, Y: 10 },
          { X: 70, Y: 10 },
          { X: 70, Y: 20 },
        ],
      },
    ];
    if (!event) {
      this.element.Strokes = strokeDemo;
    }
    this.canvasContext.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
    this.canvasContext.beginPath();

    this.canvasContext.strokeStyle = this.element.FontColor;
    this.canvasContext.lineWidth =
      this.element.PenSize > 0 ? this.element.PenSize + 2 : 3;

    // loop through strokes
    for (let s = 0; s < this.element.Strokes.length; s++) {
      for (let p = 0; p < this.element.Strokes[s].Coordinates.length; p++) {
        // starting point
        if (p == 0) {
          this.canvasContext.moveTo(
            this.element.Strokes[s].Coordinates[p].X * 2,
            this.element.Strokes[s].Coordinates[p].Y * 2
          );
        }
        // line to every other point
        else {
          this.canvasContext.lineTo(
            this.element.Strokes[s].Coordinates[p].X * 2,
            this.element.Strokes[s].Coordinates[p].Y * 2
          );
          this.canvasContext.stroke();
        }
      }
    }
  }
}
