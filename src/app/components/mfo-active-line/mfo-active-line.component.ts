import { Component, Input, HostListener, HostBinding } from '@angular/core';
import { Line } from '../../dataformat/models';
import { AppConfig } from '../../app.config';
import { HistoryService } from '@services/history.service';

/**
 * active line component
 *
 * @export
 * @class MfoActiveLineComponent
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-active-line',
  templateUrl: './mfo-active-line.component.html',
})
export class MfoActiveLineComponent {
  /**
   * element context
   *
   * @type {Line}
   * @memberof MfoActiveLineComponent
   */
  @Input() element: Line;
  /**
   * current object path
   *
   * @type {string}
   * @memberof MfoActiveLineComponent
   */
  @Input() path: string;
  /**
   * width of parent line element
   *
   * @type {number}
   * @memberof MfoActiveLineComponent
   */
  @Input() width: number;
  /**
   * height of parent line element
   *
   * @type {number}
   * @memberof MfoActiveLineComponent
   */
  @Input() height: number;

  /**
   * indicates whether left mouse button is up or down
   *
   * @private
   * @memberof MfoActiveLineComponent
   */
  private hold = false;
  /**
   * indicates which dot the mouse is on
   *
   * @private
   * @memberof MfoActiveLineComponent
   */
  private dot = 0;

  /**
   * host binding boxSizing
   *
   * @readonly
   * @type {string}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   * host binding styleWidth
   *
   * @readonly
   * @type {number}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.width.px') get styleWidth(): number {
    return this.width;
  }

  /**
   * host binding styleHeight
   *
   * @readonly
   * @type {number}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.height.px') get styleHeight(): number {
    return this.height;
  }

  /**
   * host binding top
   *
   * @readonly
   * @type {number}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return (
      (this.element.Y < this.element.Y2 ? this.element.Y : this.element.Y2) * -1
    );
  }

  /**
   * host binding left
   *
   * @readonly
   * @type {number}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return (
      (this.element.X < this.element.X2 ? this.element.X : this.element.X2) * -1
    );
  }

  /**
   * host binding position
   *
   * @readonly
   * @type {string}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * host binding zindex
   *
   * @readonly
   * @type {number}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.z-index') get zIndex(): number {
    return 9999;
  }

  /**
   * host binding display
   *
   * @readonly
   * @type {string}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * host binding pointer-events
   *
   * @readonly
   * @type {string}
   * @memberof MfoActiveLineComponent
   */
  @HostBinding('style.pointer-events') get pointereEvents(): string {
    return 'none';
  }

  /**
   * Creates an instance of MfoActiveLineComponent.
   * @param {AppConfig} config
   * @memberof MfoActiveLineComponent
   */
  constructor(public config: AppConfig, private history: HistoryService) {}

  /**
   * set dot and hold properties
   *
   * @public
   * @param {*} dot
   * @memberof MfoActiveLineComponent
   */
  public onMouseDown(dot) {
    this.hold = true;
    this.dot = dot;
  }

  /**
   * called after mouse button is released
   *
   * @public
   * @memberof MfoActiveLineComponent
   */
  @HostListener('window:mouseup', ['$event'])
  public onMouseUp() {
    this.hold = false;
  }

  /**
   * recalculate element properties
   *
   * @private
   * @param {MouseEvent} event
   * @memberof MfoActiveLineComponent
   */
  @HostListener('window:mousemove', ['$event'])
  private move(event: MouseEvent) {
    if (this.hold) {
      if (this.dot === 1) {
        const oldX = this.element.X;
        const oldY = this.element.Y;

        this.element.X += event.movementX / this.config.editor.Zoom;
        this.element.Y += event.movementY / this.config.editor.Zoom;
        if (this.element.X < 0) {
          this.element.X = 0;
        }
        if (this.element.Y < 0) {
          this.element.Y = 0;
        }

        // history
        const changeArr = [
          {
            propertyName: 'X',
            propertyValueOld: oldX,
            propertyValueNew: this.element.X,
          },
          {
            propertyName: 'Y',
            propertyValueOld: oldY,
            propertyValueNew: this.element.Y,
          },
        ];

        this.activeElementChange(changeArr);
      } else if (this.dot === 2) {
        const oldX2 = this.element.X2;
        const oldY2 = this.element.Y2;

        this.element.X2 += event.movementX / this.config.editor.Zoom;
        this.element.Y2 += event.movementY / this.config.editor.Zoom;
        if (this.element.X2 < 0) {
          this.element.X2 = 0;
        }
        if (this.element.Y2 < 0) {
          this.element.Y2 = 0;
        }

        // history
        const changeArr = [
          {
            propertyName: 'X2',
            propertyValueOld: oldX2,
            propertyValueNew: this.element.X2,
          },
          {
            propertyName: 'Y2',
            propertyValueOld: oldY2,
            propertyValueNew: this.element.Y2,
          },
        ];

        this.activeElementChange(changeArr);
      }
    }
  }

  /**
   * add history change for line pos change
   *
   * @private
   * @param {*} changes
   * @memberof MfoActiveLineComponent
   */
  private activeElementChange(...changes) {
    const changesResult = [];

    [...changes].forEach((changeArr) => {
      changeArr.forEach((change) => {
        changesResult.push(change);
      });
    });

    this.history.addPropertyChange(this.element, changesResult);
  }
}
