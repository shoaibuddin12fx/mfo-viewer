import { Component, ElementRef, Input } from '@angular/core';
import { SidebarComponent } from '@components/sidebar/sidebar.component';
import { MFO } from '../../dataformat/models/mfo.model';

/**
 * a component to show the raw data of a MFO Document in a treeview
 *
 * @export
 * @class TreeviewComponent
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'treeview',
  templateUrl: './treeview.component.html',
  styleUrls: ['./treeview.component.scss'],
})
export class TreeviewComponent {
  /**
   * Sidebar reference
   *
   * @private
   * @type {SidebarComponent}
   * @memberof TreeviewComponent
   */
  public sidebar: SidebarComponent;

  /**
   * The MFO File Object
   *
   * @type {MFO}
   * @memberof TreeviewComponent
   */
  public mfo: MFO;

  constructor(public hostRef: ElementRef) {}

  /**
   * click event for elements in treeview
   *
   * @param {*} path
   * @memberof TreeviewComponent
   */
  public selectElement(path: any): void {
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('setActiveElement', {
        bubbles: true,
        detail: { path },
      })
    );
  }
}
