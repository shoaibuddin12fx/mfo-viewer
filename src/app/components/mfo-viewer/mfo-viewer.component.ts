import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { IContainer } from '@interfaces/container';
import { HistoryService } from '@services/history.service';
import { HistoryAction } from 'src/app/dataformat/enums/historyactions.enum';
import { UiBaseSelectable } from 'src/app/dataformat/models/uibaseselectable';
import { Utils } from 'src/app/dataformat/util';
import { AppConfig } from '../../app.config';
import { MFO } from '../../dataformat/models/mfo.model';
import { ViewMode } from '../../dataformat/enums/viewmode.enum';
import { Font } from 'src/app/dataformat/models';

declare global {
  interface Window {
    mfo: any;
  }
}

/**
 * displays the MFO document at a page
 *
 * @export
 * @class MfoViewerComponent
 * @implements {AfterViewInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-viewer',
  templateUrl: './mfo-viewer.component.html',
  styleUrls: ['./mfo-viewer.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MfoViewerComponent implements AfterViewInit {
  /**
   * container element reference where the viewer is generated in
   *
   * @type {ElementRef}
   * @memberof MfoViewerComponent
   */
  @ViewChild('area', { read: ElementRef }) area: ElementRef;

  /**
   * event emitter called, when selected object changed
   *
   * @memberof MfoViewerComponent
   */
  @Output() activate = new EventEmitter<string>();

  /**
   * view mode of editor
   *
   * @type {ViewMode}
   * @memberof MfoViewerComponent
   */
  @Input() Mode: ViewMode;

  /**
   * mfo object
   *
   * @type {MFO}
   * @memberof MfoViewerComponent
   */
  public mfo: MFO;

  /**
   * actual zoom
   *
   * @type {number}
   * @memberof MfoViewerComponent
   */
  public zoom: number;

  /**
   * x position of cursor
   *
   * @type {number}
   * @memberof MfoViewerComponent
   */
  public mouseX: number;

  /**
   * y position of cursor
   *
   * @type {number}
   * @memberof MfoViewerComponent
   */
  public mouseY: number;

  /**
   * style node for font injection
   *
   * @private
   * @memberof MfoViewerComponent
   */
  private fontSheet;

  /**
   * new element to add to viewer
   *
   * @type {UiBaseSelectable}
   * @memberof MfoViewerComponent
   */
  public newElement: UiBaseSelectable;

  /**
   * Creates an instance of MfoViewerComponent.
   */
  constructor(
    private elementRef: ElementRef,
    private config: AppConfig,
    private history: HistoryService
  ) {}

  /**
   * injects font, angular keeps removing <style> tags since they are "evil" if we
   * ngfor them in the HTML
   * so we build a new node and inject it directly!
   *
   * @memberof MfoViewerComponent
   */
  public injectFonts() {
    let fontString = ``;
    this.mfo.Fonts.forEach((font) => {
      if (!font) {
        return;
      }

      fontString += `
        @font-face {
          font-family: '${font.Family}';
          src: url(${font.File}) format("woff2");
        }`;
    });

    this.fontSheet.innerHTML = fontString;
  }

  /**
   * injects a single font into the DOM
   *
   * @param {Font} font
   * @memberof MfoViewerComponent
   */
  injectFont(font: Font) {
    this.fontSheet.innerHTML += `
          @font-face {
            font-family: '${font.Family}';
            src: url(${font.File}) format("woff2");
          }`;
  }

  /**
   * angular lifecycle hook after init
   *
   * @memberof MfoViewerComponent
   */
  async ngAfterViewInit() {
    this.zoom = this.config.editor.Zoom;
    const node = document.createElement('style');
    this.elementRef.nativeElement.appendChild(node);
    this.fontSheet = node;
  }

  /**
   * sets mfo object
   *
   * @param {*} mfo
   * @memberof MfoViewerComponent
   */
  public setMfo(mfo) {
    this.mfo = mfo;
    this.injectFonts();
  }

  /**
   * creates new Element on page
   *
   * @param {*} data
   * @return {*}
   * @memberof MfoViewerComponent
   */
  @HostListener('mousedown', ['$event'])
  public async addNewElement(data: any) {
    if (this.newElement === undefined || data.button !== 0) return;
    data.path[0].click(data);

    let containerEl: HTMLElement = data.target;

    while (containerEl.tagName !== 'MFO-CONTAINER') {
      containerEl = containerEl.parentElement;
    }

    setTimeout(() => {
      let con: IContainer = Utils.getActiveContainer(
        this.mfo.activeElement,
        this.mfo
      );

      this.newElement.X = data.pageX - containerEl.getBoundingClientRect().x;
      this.newElement.Y = data.pageY - containerEl.getBoundingClientRect().y;

      this.newElement.Width = 1;
      this.newElement.Height = 1;
      con.Children = con.Children ?? [];

      con.Children.push(this.newElement);
      this.history.addUndo({
        element: this.newElement,
        action: HistoryAction['Add'],
        parent: con.Children,
      });
    }, 10);
  }

  /**
   * resizes newly created element
   *
   * @param {*} data
   * @return {*}
   * @memberof MfoViewerComponent
   */
  @HostListener('mousemove', ['$event'])
  public async newElementResize(data: any) {
    // save mousepos
    this.mouseX = data.offsetX;
    this.mouseY = data.offsetY;

    if (this.newElement === undefined || data.button !== 0) return;

    this.newElement.Width += data.movementX;
    this.newElement.Height += data.movementY;
  }

  /**
   * finalizes object creation
   *
   * @param {*} data
   * @return {*}
   * @memberof MfoViewerComponent
   */
  @HostListener('mouseup', ['$event'])
  public async finalizeNewElement(data: any) {
    if (this.newElement === undefined) return;
    this.newElement.setAsActiveElement();
    // sometimes newElement gets reset before addNewElement method is finished
    setTimeout(() => {
      this.newElement = undefined;
    }, 50);

    window.dispatchEvent(
      new CustomEvent('elementCreated', {
        bubbles: true,
      })
    );
  }

  /**
   * event listener for custom commands
   *
   * @param {*} data
   * @return {*}
   * @memberof MfoViewerComponent
   */
  @HostListener('mfo-command', ['$event.detail'])
  async command(data: any) {
    switch (data.name) {
      case 'Click':
        break;
      case 'ContextMenu':
        //Utils.log(data.element.test());
        //(data.element as MfoTextComponent).element.IsContext = true;
        break;
      case 'Zoom':
        if (data.direction === 'reset') {
          this.zoom = 1;
          this.config.editor.Zoom = this.zoom;
          this.config.save();
          break;
        }
        if (this.zoom <= 0.5 && data.direction === 'out') {
          this.zoom = 0.5;
          return;
        } else {
          const oldzoom = this.zoom;
          const area = this.area.nativeElement as HTMLElement;
          if (data.direction === 'out') {
            this.zoom -= 0.1;
          } else if (data.direction === 'in') {
            this.zoom += 0.1;
          }
          const scrollX =
            (area.getBoundingClientRect().left + area.scrollLeft) *
              (this.zoom / oldzoom) -
            area.clientWidth / 2;
          const scrollY =
            (area.getBoundingClientRect().top + area.scrollTop) *
              (this.zoom / oldzoom) -
            area.clientHeight / 2;
          await new Promise<void>((r) => {
            window.setTimeout(() => {
              r();
            }, 0);
          });
          this.area.nativeElement.scroll(scrollX, scrollY);
          this.config.editor.Zoom = this.zoom;
          this.config.save();
        }
        // Utils.log(this.zoom);
        break;
    }
  }
}
