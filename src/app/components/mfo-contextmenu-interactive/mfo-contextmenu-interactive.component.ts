import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
} from '@angular/core';
import { AutotextService } from '@services/autotext.service';
import { TabIndexDissolverService } from '@services/tabindexdissolver.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { Utils } from 'src/app/dataformat/util';
import { AppConfig } from '../../app.config';
import { MFO } from '../../dataformat/models/mfo.model';

/**
 * Contextmenu for interactive view
 *
 * @export
 * @class MfoContextMenuInteractiveComponent
 * @implements {AfterViewInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-contextmenu-interactive',
  templateUrl: './mfo-contextmenu-interactive.component.html',
  styleUrls: ['./mfo-contextmenu-interactive.component.scss'],
})
export class MfoContextMenuInteractiveComponent implements AfterViewInit {
  /**
   * interactive element
   *
   * @type {IInteractiveContextMenu}
   * @memberof MfoContextMenuInteractiveComponent
   */
  public element: any;

  /**
   * MFO Object if there are some things to be done with mfo object
   *
   * @type {MFO}
   * @memberof MfoContextMenuInteractiveComponent
   */
  public mfo: MFO;

  /**
   * List of Labels from element to display
   *
   * @type {string[]}
   * @memberof MfoContextMenuInteractiveComponent
   */
  public labels: string[];

  /**
   * coordinates to display the menu in
   *
   * @private
   * @memberof MfoContextMenuInteractiveComponent
   */
  private _left = 0;

  /**
   * coordinates to display the menu in
   *
   * @private
   * @memberof MfoContextMenuInteractiveComponent
   */
  private _top = 0;

  /**
   * indicator if zooming is enabled
   *
   * @private
   * @memberof MfoContextMenuInteractiveComponent
   */
  private _showing = true;

  /**
   * zoom scale
   *
   * @private
   * @memberof MfoContextMenuInteractiveComponent
   */
  private _zoom = 1;

  /**
   * Mode of the editor
   *
   * @type {Mode}
   * @memberof MfoContainerComponent
   */
  @Input() Mode: ViewMode;

  /**
   * host binding top
   *
   * @readonly
   * @type {number}
   * @memberof MfoContextMenuInteractiveComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this._top;
  }

  /**
   * host binding left
   *
   * @readonly
   * @type {number}
   * @memberof MfoContextMenuInteractiveComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this._left;
  }

  /**
   * host binding display
   *
   * @readonly
   * @type {string}
   * @memberof MfoContextMenuInteractiveComponent
   */
  @HostBinding('style.display') get display(): string {
    return this._showing && this.element ? 'block' : 'none';
  }

  /**
   * host binding zoom
   *
   * @readonly
   * @type {number}
   * @memberof MfoContextMenuInteractiveComponent
   */
  @HostBinding('style.zoom') get zoom(): number {
    return this._zoom;
  }

  /**
   * Creates an instance of MfoContextMenuInteractiveComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {TabIndexDissolverService} tiresolveService
   * @memberof MfoContextMenuInteractiveComponent
   */
  constructor(
    public hostRef: ElementRef,
    private autotextService: AutotextService
  ) {}

  /**
   * default implementation for interface AfterViewInit
   *
   * @memberof MfoContextMenuInteractiveComponent
   */
  ngAfterViewInit(): void {}

  /**
   * shows the contextmenu at given coordinates
   *
   * @param {*} element
   * @param {*} x
   * @param {*} y
   * @param {*} relativeX
   * @param {*} relativeY
   * @memberof MfoContextMenuInteractiveComponent
   */
  public show(element, x, y) {
    Utils.log('show interactive contextmenu', this);
    this.element = element;
    this._showing = true;
    this._top = y;
    this._left = x;
    this.labels = this.element.getItemLabel();
  }

  /**
   * bunch of window events which will cause the menu to close
   *
   * @memberof MfoContextMenuInteractiveComponent
   */
  @HostListener('window:click', ['$event'])
  @HostListener('window:dblclick', ['$event'])
  @HostListener('window:contextmenu', ['$event'])
  @HostListener('window:keydown', ['$event'])
  @HostListener('window:scrollY', ['$event'])
  public hide() {
    this.element = undefined;
    this._showing = false;
  }

  /**
   * opens autotext selection dialog
   *
   * @param {Event} event
   * @memberof MfoContextMenuInteractiveComponent
   */
  public showAutotextDlg(event: Event) {
    this.element.IsDoubleClicked = true;
    this.autotextService.checkForAutotext(this.element);
  }
}
