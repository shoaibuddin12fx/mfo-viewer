import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { FontService } from '@services/font.service';
import { AppConfig } from '../../app.config';
import { DatabindingService } from '@services/databinding.service';
import { DateformaterService } from '@services/dateformater.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { HistoryService } from '@services/history.service';
import { environment } from 'src/environments/environment';
import { SettingsComponent } from '@components/settings/settings.component';
import { DataService } from '@services/data.service';
import { Utils } from 'src/app/dataformat/util';
import { MFO } from 'src/app/dataformat/models/mfo.model';
import { ApiService } from '@services/api.service';

/**
 * a component that displays and enables to change the properties of a mfo element
 *
 * @export
 * @class MenuBarComponent
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss'],
})
export class MenuBarComponent {
  public environment;

  /**
   * Mode of the editor
   *
   * @type {ViewMode}
   * @memberof MfoContainerComponent
   */
  @Input() Mode: ViewMode;

  /**
   * formular options of document
   *
   * @type {*}
   * @memberof MenuBarComponent
   */
  @Input() FormularOptions: any;

  /**
   * export event emitter for saving container of whole document
   *
   * @memberof MenuBarComponent
   */
  @Output() export = new EventEmitter<string>();

  /**
   * import event emitter, raised when user wants to import/open a document from local pc
   *
   * @memberof MenuBarComponent
   */
  @Output() import = new EventEmitter();

  /**
   * import HTML element for filtering files by extension
   *
   * @type {ElementRef}
   * @memberof MenuBarComponent
   */
  @ViewChild('importInput', { read: ElementRef, static: false })
  importInput: ElementRef;

  /**
   * mfo object
   *
   * @private
   * @type {MFO}
   * @memberof MenuBarComponent
   */
  private mfo: MFO;

   /**
   * reference to the currently selected element in the treeview
   *
   * @type {object}
   * @memberof MenuBarComponent
   */
@Input() activeElement: any;



  /**
   * Creates an instance of MenuBarComponent.
   * @param {AppConfig} config
   * @param {AlertController} alertController
   * @param {TranslationService} translation
   * @param {FontService} fontService
   * @param {DatabindingService} dataBinding
   * @param {DateformaterService} dateFormater
   * @param {HistoryService} history
   * @param {ElementRef} hostRef
   * @memberof MenuBarComponent
   */
  constructor(
    public config: AppConfig,
    public alertController: AlertController,
    public fontService: FontService,
    public dataBinding: DatabindingService,
    public dateFormater: DateformaterService,
    public history: HistoryService,
    private dataService: DataService,
    private api: ApiService,
    public hostRef: ElementRef,
    public modalController: ModalController
  ) {
    this.environment = environment;
    
  }

 
  /**
   * using it prevent angular to recreate your DOM and it will keep track of changes
   *
   * @param {*} index
   * @return {*}
   * @memberof MenuBarComponent
   */
  public trackByFn(index) {
    return index;
  }

  /**
   * sets mfo object
   *
   * @param {MFO} mfo
   * @memberof MenuBarComponent
   */
  public setMfo(mfo: MFO) {
    this.mfo = mfo;
  }

  /**
   * trigger for changing view mode
   *
   * @memberof MenuBarComponent
   */
  public changeViewModeTrigger() {
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('changeViewMode', {
        bubbles: true,
      })
    );
  }

  /**
   * trigger for open settings
   *
   * @memberof MenuBarComponent
   */
  public async openSettings() {
   let font = this.mfo.Fonts
    const modal = await this.modalController.create({
      component: SettingsComponent,
      cssClass: 'settingsModal',
      componentProps : {activeElement : this.mfo.activeElement,FontFamily: font[font.length-1].Family}
    });
    return await modal.present();
  }

  /**
   * method for testing purposes, bound to help button
   * TODO: remove in productive build
   * @memberof MenuBarComponent
   */
  public demo() {
    let ds = new DataService();
    let demo = {
      data: [
        { path: 'Pages.0.Children.65', value: 'Child 65' },
        { path: 'Pages.0.Children.66', value: 'Child 66' },
        { path: 'Pages.0.Children.69', value: 'Child 69' },
        {
          path: 'Pages.1.Children.1.Children.15',
          propertyName: 'IsChecked',
          value: false,
        },
      ],
    };

    ds.updateMfoWithPackageData(demo, this.mfo);
  }

  /**
   * called to save document or to store user input and push it to MO-Server
   *
   * @return {*}
   * @memberof MenuBarComponent
   */
  public save() {
    switch (this.Mode) {
      case ViewMode.Preview:
      case ViewMode.Edit:
        Utils.logPrefixed(
          'menubar - save',
          (this.Mode == ViewMode.Edit ? 'edit view' : 'preview') +
            ', push document to MOServer'
        );
        // denoise document
        let mfoCopy = this.mfo.getDenoisedAnonymousCopy();
        let dt = this.dataService.createDataPackage(this.mfo);
        console.log('PACKAGE', JSON.stringify(dt));
        this.api.saveMFO(mfoCopy);
        break;
      case ViewMode.Interactive:
        // if viewer is in interactive view, we should create DataPackage and call API
        Utils.logPrefixed(
          'menubar - save',
          'interactive view, push form data to MOServer'
        );

        // let dt = this.dataService.createDataPackage(this.mfo);
        // TODO: Integration, push package to MO Server via API
        break;
      case ViewMode.Print:
        // TODO: maybe convert to PDF and Save?!
        break;
    }
  }
}
