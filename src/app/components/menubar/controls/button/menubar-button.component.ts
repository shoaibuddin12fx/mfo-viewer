import { Component, Input } from '@angular/core';

/**
 * button component for a menu bar button
 *
 * @export
 * @class MBbutton
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mb-button',
  templateUrl: './menubar-button.component.html',
  styleUrls: ['./menubar-button.component.scss'],
})
export class MBbutton {
  /**
   * caption (headline) of item
   *
   * @type {string}
   * @memberof MBbutton
   */
  @Input() caption: string;

  /**
   * path to image
   *
   * @type {string}
   * @memberof MBbutton
   */
  @Input() imgsrc: string;
}
