import { Component } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

/**
 * special Select component to select a Medical Office Databinding item
 *
 * @export
 * @class DatabindingSelectItemComponent
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'databinding-element',
  templateUrl: './databinding-select-item.component.html',
  styleUrls: ['./databinding-select-item.component.scss'],
})
export class DatabindingSelectItemComponent {
  /**
   * available databinding items
   *
   * @type {Array<{ name: string; default: string }>}
   * @memberof DatabindingSelectItemComponent
   */
  public items: Array<{ name: string; default: string }>;

  /**
   * available databinding extras
   *
   * @type {Array<{ name: string; default: string }>}
   * @memberof DatabindingSelectItemComponent
   */
  public extras: Array<{ name: string; default: string }>;

  /**
   * actual chosen item
   *
   * @type {string}
   * @memberof DatabindingSelectItemComponent
   */
  public choosenItem: string;

  /**
   * determines if a topic is chosen
   *
   * @type {boolean}
   * @memberof DatabindingSelectItemComponent
   */
  public isChoosenTopic: boolean;

  /**
   * Creates an instance of DatabindingSelectItemComponent.
   * @param {NavParams} navParams
   * @param {PopoverController} popoverCtrl
   * @memberof DatabindingSelectItemComponent
   */
  constructor(
    private navParams: NavParams,
    private popoverCtrl: PopoverController
  ) {
    this.items = this.navParams.get('items');
    this.extras = this.navParams.get('extras');
    this.isChoosenTopic = this.navParams.get('isChoosenTopic');
    this.choosenItem = this.navParams.get('choosenItem');

    // retrieve position of parent
    const x = this.navParams.get('x');
    const y = this.navParams.get('y');
    this.setCSSVariables(x, y);
  }

  /**
   * set own css variables
   *
   * @private
   * @param {*} x
   * @param {*} y
   * @memberof DatabindingSelectItemComponent
   */
  private setCSSVariables(x, y) {
    document.body.style.setProperty(
      '--databinding-select-item-popover-x',
      x + 'px'
    );
    document.body.style.setProperty(
      '--databinding-select-item-popover-y',
      y + 'px'
    );
  }

  /**
   * on select item
   *
   * @param {*} item
   * @memberof DatabindingSelectItemComponent
   */
  public chooseItem(item) {
    this.popoverCtrl.dismiss({ item });
  }
}
