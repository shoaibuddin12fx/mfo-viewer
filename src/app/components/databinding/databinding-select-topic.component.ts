import { Component } from '@angular/core';
import { IDatabindingdata } from '@interfaces/databinding/databindingdata';
import { NavParams, PopoverController } from '@ionic/angular';
import { DatabindingSelectItemComponent } from './databinding-select-item.component';

/**
 * special Select component to select a Medical Office Databinding Topic
 *
 * @export
 * @class DatabindingSelectTopicComponent
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'databinding-element',
  templateUrl: './databinding-select-topic.component.html',
  styleUrls: ['./databinding-select-topic.component.scss'],
})
export class DatabindingSelectTopicComponent {
  /**
   * all available Keys
   *
   * @type {Array<string>}
   * @memberof DatabindingSelectTopicComponent
   */
  public keys: Array<string>;

  /**
   * all available data
   *
   * @type {IDatabindingdata}
   * @memberof DatabindingSelectTopicComponent
   */
  public data: IDatabindingdata;

  /**
   * chosen topic value
   *
   * @type {string}
   * @memberof DatabindingSelectTopicComponent
   */
  public choosenTopic: string;

  /**
   * chosen item value
   *
   * @type {{ name: string; default: string }}
   * @memberof DatabindingSelectTopicComponent
   */
  public choosenItem: { name: string; default: string };

  /**
   * timer for delaying ui response
   *
   * @private
   * @memberof DatabindingSelectTopicComponent
   */
  private execTimer;

  /**
   * Creates an instance of DatabindingSelectTopicComponent.
   * @param {NavParams} navParams
   * @param {PopoverController} popoverCtrl
   * @memberof DatabindingSelectTopicComponent
   */
  constructor(
    private navParams: NavParams,
    private popoverCtrl: PopoverController
  ) {
    this.keys = this.navParams.get('keys');
    this.data = this.navParams.get('data');
    this.choosenTopic = this.navParams.get('choosenTopic');
    this.choosenItem = this.navParams.get('choosenItem');
  }

  /**
   * on mouseover
   *
   * @param {*} event
   * @param {*} key
   * @returns
   * @memberof DatabindingSelectTopicComponent
   */
  public async openMousePopover(event, key) {
    // TODO check if hover is needed or click is enough
    return;

    this.execTimer = setTimeout(() => {
      this.openPopover(event, key);
    }, 300);
  }

  /**
   * clear timeout on mouse leave
   *
   * @memberof DatabindingSelectTopicComponent
   */
  public onMouseLeave() {
    clearTimeout(this.execTimer);
  }

  /**
   * open item popover
   *
   * @param {*} event
   * @param {*} key
   * @memberof DatabindingSelectTopicComponent
   */
  public async openPopover(event, key) {
    // get clicked parent
    const clickedElement = event.target;
    const rects = clickedElement.getBoundingClientRect();

    // get items and extras for key
    const items = this.data[key].items;
    const extras = this.data[key].extras ? this.data[key].extras : [];

    const popover = await this.popoverCtrl.create({
      component: DatabindingSelectItemComponent,
      componentProps: {
        items,
        extras,
        choosenItem: this.choosenItem,
        isChoosenTopic: key === this.choosenTopic,
        x: rects.x,
        y: rects.y,
      },
      backdropDismiss: true,
      showBackdrop: false,
      event,
      cssClass: 'databindingItemPopover',
      animated: false,
    });

    // display popover
    await popover.present();

    // on close
    await popover.onDidDismiss().then((d) => {
      if (d.data) {
        // !Timeout is needed
        setTimeout(() => {
          this.popoverCtrl.dismiss({ topic: key, item: d.data.item });
        }, 1);
      }
    });
  }

  /**
   * if none is choosen
   *
   * @memberof DatabindingSelectTopicComponent
   */
  public chooseNone() {
    this.popoverCtrl.dismiss('none');
  }
}
