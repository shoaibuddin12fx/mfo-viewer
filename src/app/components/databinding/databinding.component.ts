import {
  Component,
  HostListener,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { IDatabindingdata } from '@interfaces/databinding/databindingdata';
import { PopoverController } from '@ionic/angular';
import { DatabindingSelectTopicComponent } from './databinding-select-topic.component';

/**
 * special Select component to select a Medical Office Databinding
 *
 * @export
 * @class DatabindingComponent
 * @implements {OnInit}
 * @implements {OnChanges}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'databinding-select',
  templateUrl: './databinding.component.html',
  styleUrls: ['./databinding.component.scss'],
})
export class DatabindingComponent implements OnInit, OnChanges {
  /**
   * actual Databinding value
   *
   * @type {IDatabindingdata}
   * @memberof DatabindingComponent
   */
  @Input() data: IDatabindingdata;
  /**
   * selected MFO model element
   *
   * @type {*}
   * @memberof DatabindingComponent
   */
  @Input() activeElement: any;

  /**
   * actual selected topic
   *
   * @type {string}
   * @memberof DatabindingComponent
   */
  public topic: string;

  /**
   * actual selected item
   *
   * @type {string}
   * @memberof DatabindingComponent
   */

  public item: string;
  /**
   * actual selected value
   *
   * @type {string}
   * @memberof DatabindingComponent
   */

  public value: string;
  /**
   * actual selected key
   *
   * @type {Array<string>}
   * @memberof DatabindingComponent
   */
  public keys: Array<string>;

  /**
   * Creates an instance of DatabindingComponent.
   * @param {PopoverController} popoverCtrl
   * @memberof DatabindingComponent
   */
  constructor(private popoverCtrl: PopoverController) {}

  /**
   * lifecycle hook after init
   *
   * @memberof DatabindingComponent
   */
  ngOnInit() {
    this.keys = Object.keys(this.data);
    this.setValue();
  }

  /**
   * event raised, when value changed
   *
   * @memberof DatabindingComponent
   */
  ngOnChanges() {
    if (this.activeElement.Topic && this.activeElement.Item) {
      this.topic = this.activeElement.Topic;
      this.item = this.activeElement.Item;
    } else {
      this.topic = '';
      this.item = '';
    }
    this.setValue();
  }

  /**
   * sets displayed value
   *
   * @private
   * @memberof DatabindingComponent
   */
  private setValue() {
    if (this.topic && this.item) {
      this.value = this.topic + '.' + this.item;
    } else {
      this.value = '<ohne>';
    }
  }

  /**
   * sets activeElement text based on choosen item default
   *
   * @private
   * @param {string} value
   * @memberof DatabindingComponent
   */
  private setActiveElementValue(value: string) {
    this.activeElement.Text = value;
    this.activeElement.Topic = this.topic;
    this.activeElement.Item = this.item;
  }

  /**
   * opens select Topic popover
   *
   * @param {Event} event
   * @memberof DatabindingComponent
   */
  @HostListener('click', ['$event'])
  public async open(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: DatabindingSelectTopicComponent,
      componentProps: {
        keys: this.keys,
        data: this.data,
        choosenTopic: this.topic,
        choosenItem: this.item,
      },
      backdropDismiss: true,
      showBackdrop: false,
      event,
      cssClass: 'databindingTopicPopover',
    });

    await popover.present();

    // popover was closed, check for data
    popover.onDidDismiss().then((d) => {
      if (d.data) {
        if (d.data.topic && d.data.item) {
          this.topic = d.data.topic;
          this.item = d.data.item.name;
          this.setValue();
          this.setActiveElementValue(d.data.item.default);
        } else {
          this.topic = '';
          this.item = '';
          this.setValue();
          this.setActiveElementValue('');
        }
      }
    });
  }
}
