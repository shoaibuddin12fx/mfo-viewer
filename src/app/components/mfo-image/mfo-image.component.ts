import {
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
} from '@angular/core';
import { HistoryService } from '@services/history.service';
import { AppConfig } from '../../app.config';
import { Image } from '../../dataformat/models/image.model';
import { MfoBase } from '../mfo-base/mfo-base';

/**
 * Image component to display a image on a page
 *
 * @export
 * @class MfoImageComponent
 * @extends {MfoBase}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-image',
  templateUrl: './mfo-image.component.html',
  styleUrls: ['./mfo-image.component.scss'],
})
export class MfoImageComponent extends MfoBase {
  /**
   * element context
   *
   * @type {Image}
   * @memberof MfoImageComponent
   */
  @Input() element: Image;

  /**
   * position binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * display binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * width binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * borderstyle binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    let borderStyle = 'none';

    if (this.Mode === 0) {
      borderStyle = 'dotted';
    }

    if (this.element.BorderWidth) {
      borderStyle = 'inset';
    }

    return borderStyle;
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === 0
      ? 'grey'
      : 'transparent';
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === 0
      ? 1
      : 0;
  }

  /**
   * background-image binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.backgroundImage') get backgroundImage(): string {
    if (this.element.Image) {
      return 'url(' + this.element.Image + ')';
    }
  }

  /**
   * background-size binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.backgroundSize') get beckgroundImageSize(): string {
    return '100% 100%';
  }

  /**
   * background-color binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return 'white';
  }

  /**
   * line-height binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoImageComponent
   */
  @HostBinding('style.lineHeight') get lineHeight(): string {
    return this.element.Height + 'px';
  }

  /**
   * Creates an instance of MfoImageComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @memberof MfoImageComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }

  /**
   * trigger updateImage method in editor
   *
   * @param {MouseEvent} event
   * @memberof MfoImageComponent
   */
  @HostListener('dblclick', ['$event'])
  private loadImage(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    (this.hostRef.nativeElement as HTMLElement).dispatchEvent(
      new CustomEvent('loadImage', {
        bubbles: true,
      })
    );
  }
}
