import { Component } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

/**
 * popover component for autotext selection
 *
 * @export
 * @class AutotextSelectPopoverComponent
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'autotext-select-popover',
  templateUrl: './autotext-select-popover.component.html',
  styleUrls: ['./autotext-select-popover.component.scss'],
})
export class AutotextSelectPopoverComponent {
  /**
   * predefined items which can be selected
   *
   * @memberof AutotextSelectPopoverComponent
   */
  public selectableItems = [];

  /**
   * Creates an instance of AutotextSelectPopoverComponent.
   * @param {PopoverController} popoverCtrl
   * @param {NavParams} navParams
   * @memberof AutotextSelectPopoverComponent
   */
  constructor(
    private popoverCtrl: PopoverController,
    private navParams: NavParams
  ) {
    this.selectableItems = this.navParams.get('selectableItems');
  }

  /**
   * eventhandler called when item was selected to dismiss popover
   *
   * @param {*} item
   * @memberof AutotextSelectPopoverComponent
   */
  public selectItem(item) {
    this.popoverCtrl.dismiss(item);
  }
}
