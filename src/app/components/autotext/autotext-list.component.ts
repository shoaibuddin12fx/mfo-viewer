import { Component } from '@angular/core';
import { IAutotextEntry } from '@interfaces/api/autotextResponse';
import { ModalController, NavParams } from '@ionic/angular';
import { Globals } from '@services/global.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'autotext-list',
  templateUrl: './autotext-list.component.html',
  styleUrls: ['./autotext-list.component.scss'],
})
export class AutotextListComponent {
  /**
   * boolean focused
   *
   * @memberof AutotextListComponent
   */
  public focused = false;

  /**
   * search string
   *
   * @type {string}
   * @memberof AutotextListComponent
   */
  public searchString: string = '';

  /**
   * array of filtered autotexts
   *
   * @type {Array<IAutotextEntry>}
   * @memberof AutotextListComponent
   */
  public filteredAutotexts: Array<IAutotextEntry> = [];

  /**
   * Creates an instance of AutotextListComponent.
   * @param {ModalController} modalCtrl
   * @param {NavParams} navParams
   * @memberof AutotextListComponent
   */
  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams
  ) {
    const search = this.navParams.get('search');

    if (search) {
      this.searchString = search;
    }
    this.onChange();
  }

  /**
   * dismiss modal
   *
   * @memberof AutotextListComponent
   */
  public close() {
    this.modalCtrl.dismiss();
  }

  /**
   * onfocus event
   *
   * @memberof AutotextListComponent
   */
  public onFocus() {
    this.focused = true;
  }

  /**
   * on blur event
   *
   * @memberof AutotextListComponent
   */
  public onBlur() {
    this.focused = false;
  }

  /**
   * clear search function
   *
   * @memberof AutotextListComponent
   */
  public clearSearch() {
    this.searchString = '';
  }

  /**
   * on change function for searchbar
   *
   * @memberof AutotextListComponent
   */
  public onChange() {
    this.filteredAutotexts = Globals.autotexts.filter((autotext) => {
      return autotext.kuerzel.indexOf(this.searchString.toUpperCase()) > -1;
    });
  }

  /**
   * on click for item
   *
   * @param {IAutotextEntry} item
   * @memberof AutotextListComponent
   */
  public select(item: IAutotextEntry) {
    this.modalCtrl.dismiss(item);
  }
}
