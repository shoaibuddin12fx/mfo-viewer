import { Component, HostListener, Input } from '@angular/core';
import { IPropChangeItem } from '@interfaces/historyobject';
import { HistoryService } from '@services/history.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { AppConfig } from '../../app.config';

/**
 * active and selected MFO element
 *
 * @export
 * @class MfoActiveComponent
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-active',
  templateUrl: './mfo-active.component.html',
  styleUrls: ['./mfo-active.component.scss'],
})
export class MfoActiveComponent {
  /**
   * actual MFO model element
   *
   * @type {*}
   * @memberof MfoActiveComponent
   */
  @Input() element: any;

  /**
   * Mode of the editor
   *
   * @type {ViewMode}
   * @memberof MfoContainerComponent
   */
  @Input() Mode: ViewMode;

  /**
   * Creates an instance of MfoActiveComponent.
   * @param {AppConfig} config
   * @memberof MfoActiveComponent
   */
  constructor(private config: AppConfig, private history: HistoryService) {}

  /**
   * actual x coordinate of mouse cursor
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private mouseX = 0;

  /**
   * actual y coordinate of mouse cursor
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private mouseY = 0;

  /**
   * actual x coordinate of current element
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private positionX = 0;

  /**
   * actual y coordinate of current element
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private positionY = 0;

  /**
   * width of element
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private width = 0;

  /**
   * height of element
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private height = 0;

  /**
   * determines if mousebutton is hold
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private hold = false;

  /**
   * actual mode: size/position
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private mode = '';

  /**
   * determines if border should be dotted
   *
   * @private
   * @memberof MfoActiveComponent
   */
  private dot = '';

  /**
   * raised when MouseButton is pressed
   *
   * @param {MouseEvent} event
   * @param {string} mode
   * @param {string} dot
   * @return {*}
   * @memberof MfoActiveComponent
   */
  public onMouseDown(event: MouseEvent, mode: string, dot: string) {
    if (this.Mode !== 0 || this.isPage()) {
      return;
    }

    this.hold = true;
    this.mode = mode;

    //! TODO: @Michael, why a dot param when its never used?
    this.dot = dot;

    this.mouseX = event.screenX;
    this.mouseY = event.screenY;

    this.width = this.element.Width;
    this.height = this.element.Height;

    this.positionX = this.element.X;
    this.positionY = this.element.Y;
  }

  /**
   * raised when mousebutton is released
   *
   * @param {MouseEvent} event
   * @memberof MfoActiveComponent
   */
  @HostListener('window:mouseup', ['$event'])
  public onMouseUp(event: MouseEvent) {
    this.hold = false;
  }

  /**
   * event emitter for mouse move
   *
   * @param {MouseEvent} event
   * @memberof MfoActiveComponent
   */
  @HostListener('window:mousemove', ['$event'])
  public move(event: MouseEvent) {
    let a, b;

    if (this.hold) {
      if (this.mode === 'size') {
        switch (this.dot) {
          case 'bottom-right':
            a = this.size_bottom(event);
            b = this.size_right(event);
            this.activeElementChange(a, b);
            break;
          case 'right':
            a = this.size_right(event);
            this.activeElementChange(a);
            break;
          case 'bottom':
            a = this.size_bottom(event);
            this.activeElementChange(a);
            break;
          case 'top':
            a = this.size_top(event);
            this.activeElementChange(a);
            break;
          case 'top-right':
            a = this.size_right(event);
            b = this.size_top(event);
            this.activeElementChange(a, b);
            break;
          case 'bottom-left':
            a = this.size_bottom(event);
            b = this.size_left(event);
            this.activeElementChange(a, b);
            break;
          case 'left':
            a = this.size_left(event);
            this.activeElementChange(a);
            break;
          case 'top-left':
            a = this.size_top(event);
            b = this.size_left(event);
            this.activeElementChange(a, b);
            break;
        }
      } else if (this.mode === 'position') {
        const oldX = this.element.X;
        const oldY = this.element.Y;
        this.element.X =
          this.positionX +
          (event.screenX - this.mouseX) / this.config.editor.Zoom;
        this.element.Y =
          this.positionY +
          (event.screenY - this.mouseY) / this.config.editor.Zoom;

        // history
        const changeArr = [
          {
            propertyName: 'X',
            propertyValueOld: oldX,
            propertyValueNew: this.element.X,
          },
          {
            propertyName: 'Y',
            propertyValueOld: oldY,
            propertyValueNew: this.element.Y,
          },
        ];
        this.activeElementChange(changeArr);
      }
    }
  }

  /**
   * checks whether element is a page
   *
   * @returns {boolean}
   * @memberof MfoActiveComponent
   */
  public isPage(): boolean {
    if (
      this.element.Type === 'Container' &&
      typeof this.element.isPage === 'function'
    ) {
      return this.element.isPage();
    } else {
      return false;
    }
  }

  /**
   * sets new y position property for element
   *
   * @private
   * @param {MouseEvent} event
   * @returns {Array<IPropChangeItem>}
   * @memberof MfoActiveComponent
   */
  private size_top(event: MouseEvent): Array<IPropChangeItem> {
    const oldY = this.element.Y;
    const oldH = this.element.Height;

    this.element.Y =
      this.positionY + (event.screenY - this.mouseY) / this.config.editor.Zoom;
    this.element.Height =
      this.height - (event.screenY - this.mouseY) / this.config.editor.Zoom;

    // history
    const changeArr = [
      {
        propertyName: 'Y',
        propertyValueOld: oldY,
        propertyValueNew: this.element.Y,
      },
      {
        propertyName: 'Height',
        propertyValueOld: oldH,
        propertyValueNew: this.element.Height,
      },
    ];
    return changeArr;
  }

  /**
   * sets new width property for element
   *
   * @private
   * @param {MouseEvent} event
   * @returns {Array<IPropChangeItem>}
   * @memberof MfoActiveComponent
   */
  private size_right(event: MouseEvent): Array<IPropChangeItem> {
    const oldW = this.element.Width;

    this.element.Width =
      this.width + (event.screenX - this.mouseX) / this.config.editor.Zoom;

    // history
    const changeArr = [
      {
        propertyName: 'Width',
        propertyValueOld: oldW,
        propertyValueNew: this.element.Width,
      },
    ];
    return changeArr;
  }

  /**
   * sets new height property
   *
   * @private
   * @param {MouseEvent} event
   * @returns {Array<IPropChangeItem>}
   * @memberof MfoActiveComponent
   */
  private size_bottom(event: MouseEvent): Array<IPropChangeItem> {
    const oldH = this.element.Height;

    this.element.Height =
      this.height + (event.screenY - this.mouseY) / this.config.editor.Zoom;

    // history
    const changeArr = [
      {
        propertyName: 'Height',
        propertyValueOld: oldH,
        propertyValueNew: this.element.Height,
      },
    ];
    return changeArr;
  }

  /**
   * sets new x position property
   *
   * @private
   * @param {MouseEvent} event
   * @returns {Array<IPropChangeItem>}
   * @memberof MfoActiveComponent
   */
  private size_left(event: MouseEvent): Array<IPropChangeItem> {
    const oldX = this.element.X;
    const oldW = this.element.Width;

    this.element.X =
      this.positionX + (event.screenX - this.mouseX) / this.config.editor.Zoom;
    this.element.Width =
      this.width - (event.screenX - this.mouseX) / this.config.editor.Zoom;

    // history
    const changeArr = [
      {
        propertyName: 'X',
        propertyValueOld: oldX,
        propertyValueNew: this.element.X,
      },
      {
        propertyName: 'Width',
        propertyValueOld: oldW,
        propertyValueNew: this.element.Width,
      },
    ];
    return changeArr;
  }

  /**
   * history change for activeElement
   *
   * @private
   * @param {*} changes
   * @memberof MfoActiveComponent
   */
  private activeElementChange(...changes) {
    const changesResult = [];

    [...changes].forEach((changeArr) => {
      changeArr.forEach((change) => {
        changesResult.push(change);
      });
    });

    this.history.addPropertyChange(this.element, changesResult);
  }
}
