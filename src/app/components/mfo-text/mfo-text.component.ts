import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MfoPrint } from '@services/mfoPrint.service';
import { AppConfig } from '../../app.config';
import { Text } from '../../dataformat/models';
import { MfoBase } from '../mfo-base/mfo-base';
import * as objectPath from 'object-path';
import { HistoryService } from '@services/history.service';

/**
 * Simple text component to display fixed text on a page
 *
 * @export
 * @class MfoTextComponent
 * @extends {MfoBase}
 * @implements {OnInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-text',
  templateUrl: './mfo-text.component.html',
})
export class MfoTextComponent extends MfoBase implements OnInit {
  /**
   * reference to paragraph element
   *
   * @memberof MfoTextComponent
   */
  @ViewChild('P') set p(pRef: ElementRef) {
    if (!pRef) {
      return;
    }
    const native = pRef.nativeElement as HTMLParagraphElement;

    native.style.position = 'absolute';
    native.style.margin = '0';
    native.style.resize = 'none';
    native.style.wordBreak = 'break-all';
    native.style.overflow = 'hidden';
    native.style.width = '100%';
    native.style.top = '0';
    native.style.left = '0';
    native.style.bottom = '0';
    native.style.right = '0';
  }

  /**
   * reference to textarea
   *
   * @memberof MfoTextComponent
   */
  @ViewChild('EditText') set input(inputRef: ElementRef) {
    if (!inputRef) {
      return;
    }

    const native = inputRef.nativeElement as HTMLInputElement;
    const host = this.hostRef.nativeElement as HTMLElement;

    // set some properties exclusive for input/textarea
    native.style.backgroundColor = host.style.backgroundColor;
    native.style.resize = 'none';
    native.style.overflow = 'hidden';
    native.style.width = '100%';
    native.style.height = this.element.Height + 'px';
    native.style.top = '0';
    native.style.left = '0';
    native.style.bottom = '0';
    native.style.right = '0';
    native.style.padding = '0';
    native.style.border = 'none';
    native.style.outline = 'none';

    native.focus();
  }

  /**
   * Element Context.
   *
   * @type {Container}
   * @memberof MfoContainerComponent
   */
  @Input() element: Text;

  /**
   * FontFamily binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontFamily') get fontFamily(): string {
    return this.element.FontFamily;
  }

  /**
   * FontSize binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontSize.px') get fontSize(): number {
    return this.element.FontSize;
  }

  /**
   * line height host binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoTextComponent
   */
  @HostBinding('style.lineHeight') get lineHeight(): number {
    return this.element.LineSpacing;
  }

  /**
   * LetterSpacing binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoEditComponent
   */
  @HostBinding('style.letterSpacing.px') get letterSpacing(): number {
    return this.element.LetterSpacing - 0.5;
  }

  /**
   * BorderStyle binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    return this.element.BorderWidth
      ? 'inset'
      : this.Mode === 0
      ? 'dotted'
      : 'none';
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === 0
      ? 'grey'
      : 'transparent';
  }

  /**
   * Color binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.color') get color(): string {
    return this.element.FontColor;
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === 0
      ? 1
      : 0;
  }

  /**
   * border radius host binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoTextComponent
   */
  @HostBinding('style.borderRadius.px') get borderRadius(): number {
    return this.Mode === 0 && !this.element.BorderWidth ? 4 : 0;
  }

  /**
   * BoxSizing binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   * BackgroundColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return this.element.Color;
  }

  /**
   * Width binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * Position binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * Display binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * Weight binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontWeight') get fontWeight(): string {
    return this.element.FontStyleBold ? 'bold' : 'normal';
  }

  /**
   * Weight binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.textAlign') get textAlign(): string {
    return this.element.Align;
  }

  /**
   * Weight binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.whiteSpace') get whiteSpace(): string {
    return 'pre-wrap';
  }

  /**
   * textDecoration binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoTextComponent
   */
  @HostBinding('style.textDecoration') get textDecoration(): string {
    return this.element.FontStyleUnderline ? 'underline' : 'none';
  }

  /**
   * fontStyle binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoTextComponent
   */
  @HostBinding('style.fontStyle') get fontStyle(): string {
    return this.element.FontStyleItalic ? 'italic' : '';
  }

  /**
   * Creates an instance of MfoTextComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {MfoPrint} mfoPrint
   * @param {HistoryService} history
   * @memberof MfoTextComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public mfoPrint: MfoPrint,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }

  /**
   * lifecycle hook OnInit
   *
   * @memberof MfoTextComponent
   */
  ngOnInit(): void {
    this.element.FontStyleBold = this.element.FontStyleBold || false;
    this.element.FontStyleItalic = this.element.FontStyleItalic || false;
    this.element.FontStyleStrikeOut = this.element.FontStyleItalic || false;
    this.element.FontStyleUnderline = this.element.FontStyleItalic || false;
  }

  /**
   * get Text value from mfoPrint object
   *
   * @public
   * @returns
   * @memberof MfoTextComponent
   */
  public getText(): string {
    return objectPath.get(this.mfoPrint.mfo, this.path + '.Text');
  }
}
