import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Input,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { HistoryService } from '@services/history.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { AppConfig } from '../../app.config';
import { Container } from '../../dataformat/models';
import { MfoBase } from '../mfo-base/mfo-base';

/**
 * mfo container component
 *
 * @export
 * @class MfoContainerComponent
 * @implements {AfterViewInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-container',
  templateUrl: './mfo-container.component.html',
})
export class MfoContainerComponent extends MfoBase implements AfterViewInit {
  /**
   * Template Reference for Input/Text/Label fields.
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Input') Input: TemplateRef<any>;

  /**
   * Template Reference for Subcontainer.
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Container') Container: TemplateRef<any>;

  /**
   * Template Reference for Shapes.
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Shape') Shape: TemplateRef<any>;

  /**
   * template reference for Lines
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Line') Line: TemplateRef<any>;

  /**
   * template reference for Textfields
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Text') Text: TemplateRef<any>;

  /**
   * template reference for Barcodes
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Barcode') Barcode: TemplateRef<any>;

  /**
   * template reference for Signfields
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('SignField') SignField: TemplateRef<any>;

  /**
   * template reference for Images
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Image') Image: TemplateRef<any>;

  /**
   * Template Reference for MarkFields.
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('MarkField') MarkField: TemplateRef<any>;

  /**
   * Template Reference for Checkboxes
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('CheckBox') CheckBox: TemplateRef<any>;

  /**
   * Template Reference for Painting
   *
   * @type {TemplateRef<any>}
   * @memberof MfoContainerComponent
   */
  @ViewChild('Painting') Painting: TemplateRef<any>;

  /**
   * Element Context.
   *
   * @type {Container}
   * @memberof MfoContainerComponent
   */
  @Input() element: Container;

  /**
   * determines if container is page
   *
   * @type {boolean}
   * @memberof MfoContainerComponent
   */
  @Input() isPage: boolean;

  /**
   * FontFamily binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontFamily') get fontFamily(): string {
    return this.element.FontFamily;
  }

  /**
   * FontSize binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontSize.px') get fontSize(): number {
    return this.element.FontSize;
  }

  /**
   * Margin binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.margin') get margin(): string {
    return this.isPage ? '0 auto' : '0';
  }

  /**
   * BorderStyle binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    let borderStyle = 'none';

    if (this.Mode === ViewMode.Edit) {
      borderStyle = 'dotted';
    }

    if (this.element.BorderWidth) {
      borderStyle = 'inset';
    }

    return borderStyle;
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === ViewMode.Edit
      ? 'grey'
      : 'transparent';
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === ViewMode.Edit
      ? 1
      : 0;
  }

  /**
   * BoxSizing binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   * TextOverflow binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.textOverflow') get textOverflow(): string {
    return 'clip';
  }

  /**
   * BackgroundColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return this.element.Color || 'white';
  }

  /**
   * Width binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * Position binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.position') get position(): string {
    return this.isPage ? 'relative' : 'absolute';
  }

  /**
   * Display binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.display') get display(): string {
    return this.Mode !== ViewMode.Print && this.isPage && !this.element.IsActive
      ? 'none'
      : 'block';
  }

  /**
   * TextDecoration binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.textDecoration') get textDecoration(): string {
    return `${this.element.FontStyleUnderline ? 'underline' : ''} ${
      this.element.FontStyleStrikeOut ? 'line-through' : ''
    }`;
  }

  /**
   * BoxShadow binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.boxShadow') get boxShadow(): string {
    return this.isPage ? '0px 0px 8px 1px rgba(0,0,0,0.75)' : '';
  }

  /**
   * host binding for background image
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.backgroundImage') get backgroundImage(): string {
    if (this.element.Image) {
      return 'url(' + this.element.Image + ')';
    }
  }

  /**
   * host binding for background image size
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.backgroundSize') get beckgroundImageSize(): string {
    return '100% 100%';
  }

  /**
   *Creates an instance of MfoContainerComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @param {ChangeDetectorRef} changeDetectorRef
   * @memberof MfoContainerComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super(hostRef, config, history);
  }

  /**
   * ngAfterViewInit
   *
   * @memberof MfoContainerComponent
   */
  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  /**
   * angular lifecycle hook after init
   *
   * @memberof MfoContainerComponent
   */
  ngOnInit(): void {
    this.element.Image = this.element.Image || '';
  }
}
