import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MenuBarComponent } from '@components/menubar/menubar.component';
import { MfoContextMenuInteractiveComponent } from '@components/mfo-contextmenu-interactive/mfo-contextmenu-interactive.component';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
import {
  AlertController,
  ModalController,
  PopoverController,
} from '@ionic/angular';
import { DateformaterService } from '@services/dateformater.service';
import { ExportService } from '@services/export.service';
import { FontService } from '@services/font.service';
import { GuidService } from '@services/guid.service';
import { ImportService } from '@services/import.service';
import { TabIndexDissolverService } from '@services/tabindexdissolver.service';
import datepicker from 'js-datepicker';
import * as objectPath from 'object-path';
import * as Observable from 'observable-slim';
import { MFO } from 'src/app/dataformat/models/mfo.model';
import { AppConfig } from '../../app.config';
import { Zoom } from '../../commands/zoom.command';
import { SidebarComponent } from '../../components/sidebar/sidebar.component';
import { TreeviewComponent } from '../../components/treeview/treeview.component';
import { ViewMode as ViewMode } from '../../dataformat/enums/viewmode.enum';
import * as models from '../../dataformat/models';
import { MfoContextMenuComponent } from '../mfo-contextmenu/mfo-contextmenu.component';
import { MfoViewerComponent } from '../mfo-viewer/mfo-viewer.component';
import { HistoryService } from '@services/history.service';
import { HistoryAction } from 'src/app/dataformat/enums/historyactions.enum';
import { MfoPrint } from '@services/mfoPrint.service';
import { Utils } from 'src/app/dataformat/util';
import { UiBaseSelectable } from 'src/app/dataformat/models/uibaseselectable';
import { ApiService } from '@services/api.service';
import { IAssignmentResponse } from '@interfaces/api/assignmentResponse';
import { AutotextService } from '@services/autotext.service';
import { DataService } from '@services/data.service';

/**
 * Basis Component for whole project
 *
 * @export
 * @class MfoEditorComponent
 * @implements {OnInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-editor',
  templateUrl: './mfo-editor.component.html',
  styleUrls: ['./mfo-editor.component.scss'],
})
export class MfoEditorComponent implements OnInit {
  /**
   * HTML element reference for viewer component
   *
   * @type {ElementRef}
   * @memberof MfoEditorComponent
   */
  @ViewChild(MfoViewerComponent, { read: ElementRef }) viewerRef: ElementRef;

  /**
   * context menu reference
   *
   * @memberof MfoEditorComponent
   */
  @ViewChild(MfoContextMenuComponent)
  public contextMenuComponent;

  /**
   * interactive contextmenu reference
   *
   * @memberof MfoEditorComponent
   */
  @ViewChild(MfoContextMenuInteractiveComponent)
  public contextMenuComponenteInteractive;

  /**
   * toolbar reference
   *
   * @memberof MfoEditorComponent
   */
  @ViewChild(ToolbarComponent)
  public toolbarComponent;

  /**
   * element reference of viewer component
   *
   * @type {MfoViewerComponent}
   * @memberof MfoEditorComponent
   */
  @ViewChild(MfoViewerComponent) viewer: MfoViewerComponent;

  /**
   * treeview componenent reference
   *
   * @type {TreeviewComponent}
   * @memberof MfoEditorComponent
   */
  @ViewChild(TreeviewComponent) treeview: TreeviewComponent;

  /**
   * sidebar componenent reference
   *
   * @type {SidebarComponent}
   * @memberof MfoEditorComponent
   */
  @ViewChild(SidebarComponent) sidebar: SidebarComponent;

  /**
   * menubar compononent reference
   *
   * @type {MenuBarComponent}
   * @memberof MfoEditorComponent
   */
  @ViewChild(MenuBarComponent) menubar: MenuBarComponent;

  /**
   * page tab bar reference
   *
   * @type {ElementRef}
   * @memberof MfoEditorComponent
   */
  @ViewChild('tabs', { read: ElementRef }) tabs: ElementRef;

  /**
   * alert controller
   *
   * @private
   * @memberof MfoEditorComponent
   */
  private alert;

  /**
   * mfo object
   *
   * @type {MFO}
   * @memberof MfoEditorComponent
   */
  public mfo: MFO;

  /**
   * current view mode of editor
   *
   * @type {ViewMode}
   * @memberof MfoEditorComponent
   */
  public Mode: ViewMode = ViewMode.Edit;

  /**
   * cache for toggling view modes
   *
   * @private
   * @type {ViewMode}
   * @memberof MfoEditorComponent
   */
  private previousMode: ViewMode;

  /**
   * datepicker instance
   *
   * @type {datepicker}
   * @memberof MfoEditorComponent
   */
  public mfoDatepicker: datepicker;

  /**
   * showtreeview boolean
   *
   * @type {boolean}
   * @memberof MfoEditorComponent
   */
  public showTreeview: boolean = false;

  /**
   * Creates an instance of MfoEditorComponent.
   * @param {ImportService} importService
   * @param {ExportService} exportService
   * @param {AlertController} alertController
   * @param {AppConfig} config
   * @param {TabIndexDissolverService} _tiDissolver
   * @param {FontService} fontService
   * @param {DateformaterService} dateformater
   * @memberof MfoEditorComponent
   */
  constructor(
    private importService: ImportService,
    private exportService: ExportService,
    private alertController: AlertController,
    public config: AppConfig,
    public _tiDissolver: TabIndexDissolverService,
    private _history: HistoryService,
    private fontService: FontService,
    private dateformater: DateformaterService,
    private mfoPrint: MfoPrint,
    private modalCtrl: ModalController,
    private popoverCtrl: PopoverController,
    private api: ApiService,
    private autotextService: AutotextService
  ) {}

  /**
   * default initialize method, loads a demo MFO file
   *
   * @memberof MfoEditorComponent
   */
  private async init() {
    const response = await fetch('/assets/demo.MFW');

    this.mfo = Observable.create(
      await this.importService.from.file(
        response,
        response.url.split('.').pop()
      ),
      true,
      (changes) => {
        if (changes[0].property === 'FontFamily') {
          this.removeFont(changes[0].previousValue);
          this.addFont(changes[0].newValue);
        }
      }
    );

    this.viewer.setMfo(this.mfo);
    this.menubar.setMfo(this.mfo);
    this._tiDissolver.setMfo(this.mfo);
    this._history.setMfo(this.mfo);
    this.contextMenuComponent.mfo = this.mfo;
    this.contextMenuComponenteInteractive.mfo = this.mfo;

    window.mfo = this.mfo;
    if (this.treeview) {
      this.treeview.mfo = this.viewer.mfo;
      this.treeview.sidebar = this.sidebar;
    }

    // set active element after load
    this.setActiveElement('Pages.0');

    // ! Utils logging examples
    // Utils.logPrefixed('mfo-editor', 'simple log', 1, 2, 3, 4, 5, 6, [239580, 4657384]);
    // Utils.logPrefixed('prefixed log', 1, 2, 3, 4, 5, 6, [239580, 4657384]);

    // Utils.warn('simple warn', 1, 2, 3, 4, 5, 6, [239580, 4657384]);
    // Utils.warnPrefixed('prefixed warn', 1, 2, 3, 4, 5, 6, [239580, 4657384]);

    // Utils.fail('simple fail', 1, 2, 3, 4, 5, 6, [239580, 4657384]);
    // Utils.failPrefixed('prefixed fail', 1, 2, 3, 4, 5, 6, [239580, 4657384]);

    Utils.logPrefixed('mfo-editor', 'MFO', this.mfo);
  }

  /**
   * angular lifecycle hook for after init
   *
   * @memberof MfoEditorComponent
   */
  public async ngOnInit() {
    await this.init();
    this.setFormularOptions();
    this.createCalendarInstance();
    await this.setAutotexts();
    
  }

  /**
   * sets formularoptions
   *
   * @private
   * @memberof MfoEditorComponent
   */
  private setFormularOptions(): void {
    if (this.mfo.Pages.length > 0) {
      // kblatt bezeichnung
      if (
        this.mfo.FormularOptions &&
        !this.mfo.FormularOptions.krankenblattbezeichnung
      ) {
        const krankenblattbezeichnung = (this.mfo.Pages[0] as models.Container)
          .DataKind
          ? (this.mfo.Pages[0] as models.Container).DataKind
          : '';

        this.mfo.FormularOptions.krankenblattbezeichnung =
          krankenblattbezeichnung;
      }
    } else {
      // ...
    }
  }

  /**
   * creates a calendar instance for future use
   *
   * @private
   * @memberof MfoEditorComponent
   */
  private createCalendarInstance(): void {
    this.mfoDatepicker = this.dateformater.createDatePicker(
      'calendar_input',
      null
    );
  }

  /**
   * sets global autotexts
   *
   * @private
   * @memberof MfoEditorComponent
   */
  private async setAutotexts() {
    // set timeout because waiting for login
    setTimeout(async () => {
      await this.api.getAutoTextList();

      // TODO REMOVE TESTS
      // assignmentstest
      try {
        const assignmentResp: IAssignmentResponse =
          await this.api.getAssignment('');
        Utils.logPrefixed('mfo-editor', 'assignmentResp', assignmentResp);
      } catch (error) {
        Utils.failPrefixed('API-Service', 'getAssignment catched err', error);
        // TODO errorhandler
      }
    }, 1000);
  }

  /**
   * adds simple propchange to history
   *
   * @private
   * @param {string} propname
   * @param {*} oldVal
   * @param {*} newVal
   * @memberof MfoEditorComponent
   */
  private addHistorySimplePropChange(
    propname: string,
    oldVal: any,
    newVal: any
  ) {
    this._history.addPropertyChange(this.sidebar.activeElement, [
      {
        propertyName: propname,
        propertyValueNew: newVal,
        propertyValueOld: oldVal,
      },
    ]);
  }

  /**
   * checks if a modal or popover is open
   *
   * @private
   * @returns {Promise<boolean>}
   * @memberof MfoEditorComponent
   */
  private async isPopoverOrModalOpen(): Promise<boolean> {
    if ((await this.popoverCtrl.getTop()) || (await this.modalCtrl.getTop())) {
      return true;
    }

    return false;
  }

  /**
   * sets active element
   *
   * @param {*} path
   * @returns
   * @memberof MfoEditorComponent
   */
  public setActiveElement(path: any) {
    if (path instanceof CustomEvent) {
      // ToDo: unbedingt aufruf angleichen damit
      // dieses "if" verschwinden kann
      path = path.detail.path;
    }
    let element = objectPath.get(this.mfo, path);
    let direction = path.split('.');

    // check if view is interactive view and element has property interactive
    if (this.Mode != ViewMode.Edit && !element.Interactive) {
      return;
    }

    // element was not active yet
    this.deletePropertyByName(this.mfo, 'IsActive');
    this.deletePropertyByName(this.mfo, 'IsSelected');
    this.activateElement(direction);
    this.sidebar.activeElement = element;
    this.toolbarComponent.activeElement = element;
    this.mfo.activeElement = element;
    return true;
  }

  /**
   *  set the 'IsActive' property by direction array
   *
   * @private
   * @param {string[]} direction
   * @param {boolean} [first=true]
   * @returns {void}
   * @memberof MfoEditorComponent
   */
  private activateElement(direction: string[], first = true): void {
    if (!direction?.length) {
      return;
    }

    if (first) {
      objectPath.set(this.mfo, [...direction, 'IsSelected'], true);
    }

    objectPath.set(this.mfo, [...direction, 'IsActive'], true);
    direction.pop(); // remove index
    direction.pop(); // remove element
    this.activateElement(direction, false);
  }

  /**
   * removes properties by name
   *
   * @private
   * @param {*} obj
   * @param {*} propertyName
   * @returns
   * @memberof MfoEditorComponent
   */
  private deletePropertyByName(obj, propertyName) {
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;
      if (i == propertyName) {
        delete obj[propertyName];
      } else if (typeof obj[i] == 'object' && i != 'uiElement') {
        this.deletePropertyByName(obj[i], propertyName);
      }
    }
    return obj;
  }

  /**
   * event listener for zoom events
   *
   * @param {*} direction
   * @memberof MfoEditorComponent
   */
  @HostListener('window:mfo:zoom', ['$event.detail.direction'])
  private zoomListener(direction) {
    if (this.contextMenuComponent != undefined)
      this.contextMenuComponent.hide();
    if (this.contextMenuComponenteInteractive != undefined)
      this.contextMenuComponenteInteractive.hide();
    // DO NOT REMOVE!! Object.setPrototypeOf({ a: 1 }, models['Container'].prototype);

    this.execute<Zoom>({
      name: Zoom.name,
      direction,
    });
  }

  /**
   * event handler for image upload
   *
   * @memberof MfoEditorComponent
   */
  @HostListener('window:loadImage', ['$event'])
  private loadImage() {
    this.imageUpload('upload');
  }

  /**
   * event handler for checkbox group toggle
   *
   * @param {*} $event
   * @memberof MfoEditorComponent
   */
  @HostListener('window:checkBoxChecked', ['$event'])
  public checkboxChecked($event) {
    this.uncheckCheckboxesOfGroup($event.detail);
  }

  /**
   * event handler to dissolve tab index references
   *
   * @param {*} $event
   * @memberof MfoEditorComponent
   */
  @HostListener('window:resolveTabIndexes', ['$event'])
  public dissolveTabIndexReferences($event) {
    this.dissolveTIReferences();
  }

  /**
   * handles context menu open behaviour
   *
   * @param {MouseEvent} event
   * @memberof MfoEditorComponent
   */
  public contextMenu(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();

    if (
      event['path'].some((element: HTMLElement) =>
        ['MFO-TEXT', 'MFO-CONTAINER'].includes(element?.tagName?.toUpperCase())
      )
    ) {
      let rect = (event.target as HTMLElement).getBoundingClientRect();
      if (this.Mode != ViewMode.Edit) {
        // element has to implement getItemLabel function for interactive context menu, otherwise, suppress contextmenu
        if (typeof this.sidebar.activeElement.getItemLabel === 'function')
          this.contextMenuComponenteInteractive.show(
            this.sidebar.activeElement,
            event.detail['pageX'],
            event.detail['pageY']
          );
      } else {
        this.contextMenuComponent.show(
          this.sidebar.activeElement,
          event.detail['pageX'],
          event.detail['pageY'],
          event.detail['pageX'] - rect.x,
          event.detail['pageY'] - rect.y
        );
      }
    }
  }

  /**
   * handles a mouse click event and closes any open context menues
   *
   * @param {MouseEvent} event
   * @memberof MfoEditorComponent
   */
  @HostListener('click', ['$event'])
  private click(event: MouseEvent) {
    if (this.contextMenuComponent != undefined)
      this.contextMenuComponent.hide();
    if (this.contextMenuComponenteInteractive != undefined)
      this.contextMenuComponenteInteractive.hide();
  }

  /**
   * handles key down events for keyboard shortcuts
   *
   * @private
   * @param {KeyboardEvent} event
   * @return {*}
   * @memberof MfoEditorComponent
   */
  @HostListener('window:keydown', ['$event'])
  private async keydownListener(event: KeyboardEvent) {
    if (this.contextMenuComponent != undefined)
      this.contextMenuComponent.hide();
    if (this.contextMenuComponenteInteractive != undefined)
      this.contextMenuComponenteInteractive.hide();
    if (event.key === 'Escape') {
      event.preventDefault();
      Utils.logPrefixed('mfo-editor', 'esc');
    }

    // alerts
    if (this.alert && event.key === 'j') {
      event.preventDefault();
      Utils.logPrefixed('mfo-editor', 'confirm pressed');

      this.alert.buttons[0].handler();
      this.alert.dismiss();
      this.alert = undefined;
    }

    if (this.alert && event.key === 'n') {
      event.preventDefault();
      Utils.logPrefixed('mfo-editor', 'decline pressed');

      this.alert.dismiss();
    }

    // if popover or modal open ignore actions
    if (await this.isPopoverOrModalOpen()) {
      return;
    }

    // ctrl + "+"
    if (event.ctrlKey && event.key === '+') {
      event.preventDefault();
      Utils.logPrefixed('mfo-editor', 'zoom ctrl +');

      this.execute<Zoom>({
        name: Zoom.name,
        direction: 'in',
      });
    }

    // ctrl + "-"
    if (event.ctrlKey && event.key === '-') {
      event.preventDefault();
      Utils.logPrefixed('mfo-editor', 'zoom ctrl -');

      this.execute<Zoom>({
        name: Zoom.name,
        direction: 'out',
      });
    }

    // ctrl + "=" || "0"
    if (event.ctrlKey && (event.key === '=' || event.key === '0')) {
      event.preventDefault();

      this.execute<Zoom>({
        name: Zoom.name,
        direction: 'reset',
      });
    }

    let oldVal;

    // ctrlkey pressed
    if (event.ctrlKey) {
      // shortcut available just in edit view
      if (this.Mode == ViewMode.Edit) {
        switch (event.key) {
          case 'ArrowUp':
            Utils.logPrefixed('mfo-editor', 'ctrl ArrowUp');
            oldVal = this.sidebar.activeElement.Y;
            this.sidebar.activeElement.Y -= 10;
            this.addHistorySimplePropChange(
              'Y',
              oldVal,
              this.sidebar.activeElement.Y
            );
            break;
          case 'ArrowDown':
            Utils.logPrefixed('mfo-editor', 'ctrl ArrowDown');
            oldVal = this.sidebar.activeElement.Y;
            this.sidebar.activeElement.Y += 10;
            this.addHistorySimplePropChange(
              'Y',
              oldVal,
              this.sidebar.activeElement.Y
            );
            break;
          case 'ArrowLeft':
            Utils.logPrefixed('mfo-editor', 'ctrl ArrowLeft');
            oldVal = this.sidebar.activeElement.X;
            this.sidebar.activeElement.X -= 10;
            this.addHistorySimplePropChange(
              'X',
              oldVal,
              this.sidebar.activeElement.X
            );
            break;
          case 'ArrowRight':
            Utils.logPrefixed('mfo-editor', 'ctrl ArrowRight');
            oldVal = this.sidebar.activeElement.X;
            this.sidebar.activeElement.X += 10;
            this.addHistorySimplePropChange(
              'X',
              oldVal,
              this.sidebar.activeElement.X
            );
            break;
          case 'i':
          case 'I':
            Utils.logPrefixed('mfo-editor', 'ctrl I = import action');
            this.menubar.importInput.nativeElement.click();
            break;
          case 'j':
          case 'J':
            event.preventDefault();
            Utils.logPrefixed('mfo-editor', 'ctrl J = export action');
            this.exportMFW();
            break;
          case 'c':
          case 'C':
            Utils.logPrefixed('mfo-editor', 'ctrl C = copy action');
            this.contextMenuComponent.copy(this.sidebar.activeElement);
            break;
          case 'x':
          case 'X':
            Utils.logPrefixed('mfo-editor', 'ctrl X = cut action');
            this.contextMenuComponent.cut(this.sidebar.activeElement);
            break;
          case 'v':
          case 'V':
            Utils.logPrefixed('mfo-editor', 'ctrl V = paste action');
            let newElementPaste = this.contextMenuComponent.paste(
              this.sidebar.activeElement,
              this.viewer.mouseX,
              this.viewer.mouseY
            );
            if (newElementPaste) {
              this.setActiveElement(newElementPaste.__getPath);
            }
            break;
        }
      }
      // generally available shortcuts
      switch (event.key) {
        case 'z':
        case 'Z':
          Utils.logPrefixed('mfo-editor', 'ctrl Z = undo action');
          this._history.undo();
          break;
        case 'y':
        case 'Y':
          Utils.logPrefixed('mfo-editor', 'ctrl Y = redo action');
          this._history.redo();
          break;
        case 's':
        case 'S':
          Utils.logPrefixed('mfo-editor', 'ctrl S = save action');
          // TODO implement Save function?
          break;
        case 'o':
        case 'O':
          event.preventDefault();
          Utils.logPrefixed('mfo-editor', 'ctrl O = change view action');
          this.Mode =
            this.Mode === ViewMode.Edit ? ViewMode.Interactive : ViewMode.Edit;
          break;
        case 'e':
        case 'E':
          event.preventDefault();
          Utils.logPrefixed('mfo-editor', 'ctrl E = open menu action');
          // TODO implement menu
          break;
        case 'F1':
          Utils.logPrefixed('mfo-editor', 'ctrl f1 = open help');
          // TODO implement help
          break;
      }
    }

    // shiftkey pressed
    if (event.shiftKey) {
      // shortcut available just in edit view
      if (this.Mode == ViewMode.Edit) {
        switch (event.key) {
          case 'ArrowUp':
            // flacher
            Utils.logPrefixed('mfo-editor', 'shift ArrowUp');
            oldVal = this.sidebar.activeElement.Height;
            this.sidebar.activeElement.Height -= 10;
            this.addHistorySimplePropChange(
              'Height',
              oldVal,
              this.sidebar.activeElement.Height
            );
            break;
          case 'ArrowDown':
            // höher
            Utils.logPrefixed('mfo-editor', 'shift ArrowDown');
            oldVal = this.sidebar.activeElement.Height;
            this.sidebar.activeElement.Height += 10;
            this.addHistorySimplePropChange(
              'Height',
              oldVal,
              this.sidebar.activeElement.Height
            );
            break;
          case 'ArrowLeft':
            // schmaler
            Utils.logPrefixed('mfo-editor', 'shift ArrowLeft');
            oldVal = this.sidebar.activeElement.Width;
            this.sidebar.activeElement.Width -= 10;
            this.addHistorySimplePropChange(
              'Width',
              oldVal,
              this.sidebar.activeElement.Width
            );
            break;
          case 'ArrowRight':
            // breiter
            Utils.logPrefixed('mfo-editor', 'shift ArrowRight');
            oldVal = this.sidebar.activeElement.Width;
            this.sidebar.activeElement.Width += 10;
            this.addHistorySimplePropChange(
              'Width',
              oldVal,
              this.sidebar.activeElement.Width
            );
            break;
          case 'H':
          case 'h':
            Utils.logPrefixed('mfo-editor', 'shift H');
            this.contextMenuComponent.toBack(this.sidebar.activeElement);
            break;
          case 'V':
          case 'v':
            Utils.logPrefixed('mfo-editor', 'shift V');
            this.contextMenuComponent.toFront(this.sidebar.activeElement);
            break;
          case 'Delete':
            Utils.logPrefixed('mfo-editor', 'shift Delete');
            this.contextMenuComponent.delete();

            // set activeElement to activePage otherwise we can delete unlimited times
            // with wrong elements!
            const activeContainer = this.mfo.Pages.filter((p) => {
              return p.IsActive;
            })[0];

            this.sidebar.activeElement = activeContainer;

            // ! do not remove return here!
            // ! otherwise element will get removed AND text will be cleared!
            return;
        }
      }
      // generally available shortcuts
      switch (event.key) {
        case 'Tab':
          Utils.logPrefixed('mfo-editor', 'shift Tab');
          // TODO remove warning after function implementation
          Utils.warn('jumping back to last element is not implemented yet!');

          // ! do not remove return here!
          return;
      }
    }

    // altkey pressed and not in edit view
    if (event.altKey && this.Mode == ViewMode.Edit) {
      switch (event.key.toLowerCase()) {
        case 't':
          Utils.logPrefixed('mfo-editor', 'alt T -> create new TextElement');
          this.contextMenuComponent.createText(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'e':
          event.preventDefault();
          Utils.logPrefixed('mfo-editor', 'alt E -> create new InputElement');
          this.contextMenuComponent.createEdit(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'b':
          Utils.logPrefixed('mfo-editor', 'alt B -> create new ImageElement');
          this.contextMenuComponent.createImage(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'r':
          Utils.logPrefixed('mfo-editor', 'alt R -> create new ShapeElement');
          this.contextMenuComponent.createShape(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'l':
          Utils.logPrefixed('mfo-editor', 'alt L -> create new LineElement');
          this.contextMenuComponent.createLine(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'c':
          event.preventDefault();
          Utils.logPrefixed(
            'mfo-editor',
            'alt C -> create new CheckboxElement'
          );
          this.contextMenuComponent.createCheckBox(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'm':
          Utils.logPrefixed(
            'mfo-editor',
            'alt M -> create new MarkfieldElement'
          );
          this.contextMenuComponent.createMarkField(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'k':
          Utils.logPrefixed(
            'mfo-editor',
            'alt K -> create new ContainerElement'
          );
          this.contextMenuComponent.createContainer(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'q':
          Utils.logPrefixed('mfo-editor', 'alt Q -> create new QRElement');
          this.contextMenuComponent.createBarcode(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
        case 'u':
          Utils.logPrefixed('mfo-editor', 'alt U -> create new SignElement');
          this.contextMenuComponent.createSignField(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
        case 'z':
          Utils.logPrefixed(
            'mfo-editor',
            'alt Z -> create new PaintingElement'
          );
          this.contextMenuComponent.createPainting(
            this.viewer.mouseX,
            this.viewer.mouseY
          );
          break;
      }
    }

    // single shortcuts
    switch (event.key) {
      case ' ':
        // checkbox
        if (this.sidebar.activeElement instanceof models.CheckBox) {
          event.preventDefault();
          Utils.logPrefixed('mfo-editor', 'whitespace -> checkboxAction');
          oldVal = this.sidebar.activeElement.IsChecked;
          this.sidebar.activeElement.IsChecked = !oldVal;
          this.addHistorySimplePropChange('IsChecked', oldVal, !oldVal);
        }

        // datefield
        if (
          this.sidebar.activeElement.IsDateInput &&
          this.sidebar.activeElement.DateFormat !== ''
        ) {
          event.preventDefault();
          Utils.logPrefixed('mfo-editor', 'whitespace -> dateFieldAction');
          this.sidebar.activeElement.Text = this.dateformater.formatDate(
            this.sidebar.activeElement.DateFormat
          );
        }
        break;
      case 'Tab':
        event.preventDefault();
        Utils.logPrefixed('mfo-editor', 'Tab');
        // autotext jumping to next action
        if (
          this.Mode === ViewMode.Interactive &&
          this.sidebar.activeElement instanceof models.Edit &&
          this.sidebar.activeElement.Text.includes('{...')
        ) {
          Utils.logPrefixed(
            'mfo-editor',
            'activeElement contains autotext action, setSelection for action'
          );

          this.autotextService.checkForAutotextAction(
            this.sidebar.activeElement
          );

          break;
        }

        // TODO remove warning after funtion implementation
        Utils.warn('jumping to next element is not implemented yet!');
        break;
      case 'Delete':
        Utils.logPrefixed('mfo-editor', 'Remove');
        if (
          this.sidebar.activeElement.hasOwnProperty('Text') &&
          this.sidebar.activeElement.Text.length > 0
        ) {
          Utils.logPrefixed('mfo-editor', 'cleared text for element!');
          oldVal = this.sidebar.activeElement.Text;
          this.sidebar.activeElement.Text = '';
          this.addHistorySimplePropChange(
            'Text',
            oldVal,
            this.sidebar.activeElement.Text
          );
        }
        break;
      case 'ArrowUp':
        // datefields
        if (
          this.sidebar.activeElement.IsDateInput &&
          this.sidebar.activeElement.DateFormat !== ''
        ) {
          event.preventDefault();
          Utils.logPrefixed('mfo-editor', 'ArrowUp -> IncrementDateAction');
          const compatibleFormats = this.dateformater.possibleGermanFormats;
          const choosenFormat = this.sidebar.activeElement.DateFormat;

          if (compatibleFormats.includes(choosenFormat)) {
            let dateObj = new Date();

            // convert str to date
            const dayjsDate = this.dateformater.getDateObjFromString(
              this.sidebar.activeElement.Text,
              choosenFormat
            );

            if (dayjsDate !== '') {
              dateObj = dayjsDate.toDate();
            }

            this.sidebar.activeElement.Text = this.dateformater.changeDate(
              'forwards',
              dateObj,
              choosenFormat
            );
          }
        }
        break;
      case 'ArrowDown':
        if (
          this.sidebar.activeElement.IsDateInput &&
          this.sidebar.activeElement.DateFormat !== ''
        ) {
          event.preventDefault();
          Utils.logPrefixed('mfo-editor', 'ArrowUp -> DecrementDateAction');
          const compatibleFormats = this.dateformater.possibleGermanFormats;
          const choosenFormat = this.sidebar.activeElement.DateFormat;

          if (compatibleFormats.includes(choosenFormat)) {
            let dateObj = new Date();

            // convert str to date
            const dayjsDate = this.dateformater.getDateObjFromString(
              this.sidebar.activeElement.Text,
              choosenFormat
            );

            if (dayjsDate !== '') {
              dateObj = dayjsDate.toDate();
            }

            this.sidebar.activeElement.Text = this.dateformater.changeDate(
              'backwards',
              dateObj,
              choosenFormat
            );
          }
        }
        break;
      case 'Insert':
        Utils.logPrefixed('mfo-editor', 'Insert -> Check for autotext');
        this.autotextService.checkForAutotext(this.sidebar.activeElement);
        break;
    }
  }

  /**
   * executes a custom command
   *
   * @private
   * @template T
   * @param {T} data
   * @memberof MfoEditorComponent
   */
  private execute<T>(data: T) {
    this.viewerRef.nativeElement.dispatchEvent(
      new CustomEvent<T>('mfo-command', {
        detail: data,
      })
    );
  }

  /**
   * handles click event on page tabs for setting as active or remove by middle mouse button click
   *
   * @param {*} clickedpage
   * @param {*} event
   * @memberof MfoEditorComponent
   */
  public handleTabClick(clickedpage, event) {
    if (event.which === 2) {
      // middle mouse button
      event.preventDefault();
      this.removePage(clickedpage);
    }

    if (event.which === 1) {
      // left mouse button
      this.mfo.Pages.forEach((page) => {
        page.IsActive = false;
      });
      clickedpage.IsActive = true;
      clickedpage.setAsActiveElement();
    }
  }

  /**
   * removes page from document
   *
   * @param {*} [clickedpage=undefined]
   * @return {*}
   * @memberof MfoEditorComponent
   */
  public removePage(clickedpage = undefined) {
    // return if just one page is left or editor is not in edit view
    if (this.mfo.Pages.length === 1 || this.Mode != ViewMode.Edit) {
      return;
    }

    // clickedpage is undefined because call from button
    if (clickedpage === undefined) {
      clickedpage = this.mfo.getActivePage();
    }

    this.alertController
      .create({
        message: 'Möchten Sie die gewählte Seite wirklich löschen?',
        cssClass: 'invisiblebackdrop',
        backdropDismiss: false,
        keyboardClose: true,
        mode: 'md',
        buttons: [
          {
            text: 'Ja',
            cssClass: 'primary',
            handler: () => {
              let neighbour;

              // find index
              const index = this.mfo.Pages.findIndex(
                (f: models.Container) => f.Id === clickedpage.Id
              );

              // add history entry
              const pageElement = this.mfo.Pages[index];
              pageElement.IsActive = false;
              this._history.addUndo({
                element: pageElement,
                action: HistoryAction['Remove'],
              });

              // delete page;
              this.mfo.Pages.splice(
                this.mfo.Pages.findIndex(
                  (f: models.Container) => f.Id === clickedpage.Id
                ),
                1
              );

              // identify neighbour
              if (this.mfo.Pages.length >= index + 1) {
                neighbour = this.mfo.Pages[index];
              } else {
                neighbour = this.mfo.Pages[0];
              }

              // set neighbour to active
              neighbour.IsActive = true;
            },
          },
          {
            text: 'Nein',
            role: 'cancel',
          },
        ],
      })
      .then((alert) => {
        // this.alert = alert;
        alert.present().then(() => {
          const button: any = document.querySelector(
            'ion-alert button:last-of-type'
          );
          button.focus();
          return;
        });
      });
  }

  /**
   * adds new page to mfo object
   *
   * @memberof MfoEditorComponent
   */
  public addPage() {
    const newPage = new models.Container();
    newPage.Id = GuidService.generateGuid();
    newPage.Type = 'Container';
    newPage.Children = [];
    newPage.Height = 1122;
    newPage.Width = 793;

    this.mfo.Pages.push(newPage);

    this._history.addUndo({
      element: newPage,
      action: HistoryAction['Add'],
    });
    newPage.setAsActiveElement();

    // Utils.logPrefixed('mfo-editor', this.mfo);
  }

  /**
   * handles image upload
   *
   * @param {*} event
   * @memberof MfoEditorComponent
   */
  public imageUpload(event) {
    if (event === 'upload') {
      this.sidebar.imageInput.nativeElement.click();
    } else if (event === 'delete') {
      this.sidebar.deleteImg();
    }
  }

  /**
   * uncheckes all Checkboxes of one given group
   *
   * @private
   * @param {*} group
   * @memberof MfoEditorComponent
   */
  private uncheckCheckboxesOfGroup(group) {
    this.viewer.mfo.Pages.forEach((container) => {
      this.uncheckByGroupRecoursive(container, group);
    });
  }

  /**
   * unchecks all Checkboxes of one give Group recoursively
   *
   * @private
   * @param {*} container
   * @param {*} group
   * @memberof MfoEditorComponent
   */
  private uncheckByGroupRecoursive(container, group) {
    container.Children.forEach((child) => {
      if (child.Type == 'CheckBox') {
        if (child.GroupIndex == group) child.IsChecked = false;
      } else {
        child.Children.forEach((c) => {
          this.uncheckByGroupRecoursive(c, group);
        });
      }
    });
  }

  /**
   * exports document as MFW file
   *
   * @memberof MfoEditorComponent
   */
  public exportMFW() {
    this.exportService.toMFW(this.mfo);
  }

  /**
   * export Container as .obj
   *
   * @public
   * @memberof MfoEditorComponent
   */
  public exportObject() {
    const fontArray = [];
    const object = { Fonts: [], Container: {} };
    this.mfo.Fonts.forEach((font) => {
      if (this.parseFonts([this.sidebar.activeElement], font.Family)) {
        fontArray.push(font);
      }
    });
    object.Fonts = fontArray;
    object.Container = this.sidebar.activeElement;
    Utils.logPrefixed('mfo-editor', 'export', object);
    this.exportService.toObject(object);
  }

  /**
   * calls the export service toPDF function
   *
   * @public
   * @memberof MfoEditorComponent
   */
  public exportPDF() {
    this.exportService.toPDF(
      document.querySelectorAll('#page'),
      this.mfo.FormularOptions.krankenblattbezeichnung
    );
  }

  /**
   * export eventhandler
   *
   * @public
   * @param {string} format
   * @memberof MfoEditorComponent
   */
  public export(format: string) {
    switch (format) {
      case 'MFW':
        this.exportMFW();
        break;
      case 'Object':
        this.exportObject();
        break;
      case 'PDF':
        this.exportPDF();
        break;
    }
  }

  /**
   * import MFO,MFW or obj File
   *
   * @public
   * @param {*} event
   * @memberof MfoEditorComponent
   */
  public async import(event) {
    if (event.Type === 'Object') {
      const object = await this.importService.from.file(event.File, 'obj');
      const mfoFonts = [];
      this.mfo.Fonts.forEach((font) => {
        mfoFonts.push(font.Family);
      });
      object.Fonts.forEach((font) => {
        if (!mfoFonts.includes(font.Family)) {
          this.addFont(font.Family);
        }
      });
      this.sidebar.activeElement.Children.push(object['Container']);
      this.setActiveElement(
        this.sidebar.activeElement.Children.slice(-1)[0].__getPath
      );
    } else if (event.Type === 'MFO') {
      this.mfo = Observable.create(
        await this.importService.from.file(
          event.File,
          event.File.name.split('.').pop()
        ),
        true,
        (changes) => {
          if (changes[0].property === 'FontFamily') {
            this.removeFont(changes[0].previousValue);
            this.addFont(changes[0].newValue);
          }
        }
      );
      this.viewer.setMfo(this.mfo);
      this.menubar.setMfo(this.mfo);
      this._tiDissolver.setMfo(this.mfo);
      this._history.setMfo(this.mfo);
      this.contextMenuComponent.mfo = this.mfo;
      this.contextMenuComponenteInteractive.mfo = this.mfo;

      if (this.treeview) {
        this.treeview.mfo = this.viewer.mfo;
      }

      window.mfo = this.mfo;

      this.setActiveElement('Pages.0');
      this.setFormularOptions();
      // Utils.logPrefixed('mfo-editor', this.mfo);
      this.menubar.importInput.nativeElement.value = null;
    }
  }

  /**
   * resolves all TabIndex reference texts in all objects
   *
   * @public
   * @memberof MfoEditorComponent
   */
  public dissolveTIReferences() {
    this._tiDissolver.updateTIfromObjects();
    this._tiDissolver.resolveAllTabIndexes();
  }

  /**
   * remove font (if not used anymore) from mfo Object and stylesheet
   *
   * @private
   * @param {*} font
   * @returns
   * @memberof MfoEditorComponent
   */
  private removeFont(font) {
    for (let i = 0; i < this.mfo.Pages.length; i++) {
      if (
        this.parseFonts((this.mfo.Pages[i] as models.Container).Children, font)
      ) {
        return;
      }
    }
    this.mfo.Fonts.splice(
      this.mfo.Fonts.indexOf(
        this.mfo.Fonts.find((element) => element.Family === font)
      ),
      1
    );
    this.viewer.injectFonts();
  }

  /**
   * add new font to the mfo Object and inject into style
   *
   * @private
   * @param {*} font
   * @returns
   * @memberof MfoEditorComponent
   */
  private async addFont(font: string) {
    for (let i = 0; i < this.mfo.Fonts.length; i++) {
      if (this.mfo.Fonts[i].Family === font) {
        return;
      }
    }
    const fontObject = await this.fontService.getFont(font);
    this.mfo.Fonts.push(fontObject);
    this.viewer.injectFont(fontObject);
  }

  /**
   * check if array of elements contains a certain font
   *
   * @private
   * @param {*} children
   * @param {*} font
   * @returns
   * @memberof MfoEditorComponent
   */
  private parseFonts(children, font) {
    for (let i = 0; i < children.length; i++) {
      if (children[i].FontFamily === font) {
        return true;
      }
      if (children[i].Children) {
        if (this.parseFonts(children[i].Children, font)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * called, when toolbar has activated creation mode
   *
   * @param {UiBaseSelectable} data
   * @memberof MfoEditorComponent
   */
  public async toolbarActivated(data: UiBaseSelectable) {
    this.viewer.newElement = data;
  }

  /**
   * changes view mode
   *
   * @memberof MfoEditorComponent
   */
  @HostListener('window:changeViewMode', ['$event.detail'])
  public changeViewMode(mode: ViewMode): void {
    if (mode === null || mode === undefined) {
      mode = ViewMode.Preview;
    }

    if (this.Mode !== mode) {
      this.previousMode = this.Mode;
      this.Mode = mode;
    } else {
      this.Mode = this.previousMode;
      this.previousMode = mode;
    }

    if (this.Mode === ViewMode.Print && this.previousMode !== ViewMode.Print) {
      let mfoCopy = this.mfo.getDenoisedAnonymousCopy();
      this.mfoPrint.setMfo(JSON.stringify(mfoCopy));

      this._tiDissolver.setMfo(this.mfoPrint.mfo);
      this._tiDissolver.resolveAllTabIndexes();

      document.documentElement.style.height =
        this.mfo.Pages.length * 1000 + 'px';
    } else if (
      this.Mode !== ViewMode.Print &&
      this.previousMode === ViewMode.Print
    ) {
      document.documentElement.style.height = '100%';
      this._tiDissolver.setMfo(this.mfo);
    }
  }

  /**
   * shows calendar
   *
   * @param {CustomEvent} event
   * @memberof MfoEditorComponent
   */
  @HostListener('window:showcalendar', ['$event'])
  public showCalendar(event: CustomEvent): void {
    const element = event.detail.element;

    this.mfoDatepicker.onSelect = (instance) => {
      const dateSelected = instance.dateSelected.toLocaleDateString(
        'de-DE',
        this.dateformater.dayFormatOptions
      );
      element.Text = this.dateformater.formatDate(
        element.DateFormat,
        dateSelected
      );
    };

    setTimeout(() => {
      this.mfoDatepicker.show();
    }, 1);
  }

  /**
   * closes possible open calendar
   *
   * @memberof MfoEditorComponent
   */
  @HostListener('window:closecalendar')
  public closeCalendar(): void {
    const isVisible =
      !this.mfoDatepicker.calendarContainer.classList.contains('qs-hidden');

    if (isVisible) {
      this.mfoDatepicker.hide();
    }
  }

  /**
   * sets active Element based on an event for example from the treeview
   *
   * @param {CustomEvent} event
   * @memberof MfoEditorComponent
   */
  @HostListener('window:setActiveElement', ['$event'])
  public setActiveElementEvent(event: CustomEvent): void {
    this.setActiveElement(event.detail.path);
  }
}
