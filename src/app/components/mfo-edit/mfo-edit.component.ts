import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AppConfig } from '../../app.config';
import { Edit } from '../../dataformat/models';
import { MfoBase } from '../mfo-base/mfo-base';
import { MfoPrint } from '@services/mfoPrint.service';
import * as objectPath from 'object-path';
import { HistoryService } from '@services/history.service';
import { ModalController } from '@ionic/angular';
import { AutotextService } from '@services/autotext.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';

/**
 * Input component for fetching user input
 *
 * @export
 * @class MfoEditComponent
 * @extends {MfoBase}
 * @implements {OnInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-edit',
  templateUrl: './mfo-edit.component.html',
  styleUrls: ['./mfo-edit.component.scss'],
})
export class MfoEditComponent extends MfoBase implements OnInit {
  /**
   * HTML element reference to textarea
   *
   * @private
   * @type {HTMLInputElement}
   * @memberof MfoEditComponent
   */
  private nativeInput: HTMLInputElement;

  /**
   * set properties for p element
   *
   * @memberof MfoEditComponent
   */
  @ViewChild('P') set p(pRef: ElementRef) {
    if (!pRef) {
      return;
    }
    const native = pRef.nativeElement as HTMLParagraphElement;

    native.style.position = 'absolute';
    native.style.margin = '0';
    native.style.resize = 'none';
    native.style.wordBreak = 'break-all';
    native.style.overflow = 'hidden';
    native.style.width = '100%';
    native.style.top = '0';
    native.style.left = '0';
    native.style.bottom = '0';
    native.style.right = '0';
  }

  /**
   * set properties of textarea/input element
   *
   * @memberof MfoEditComponent
   */
  @ViewChild('EditText') set edittext(inputRef: ElementRef) {
    if (!inputRef) {
      return;
    }

    const native = inputRef.nativeElement as HTMLInputElement;
    this.nativeInput = native;
    const host = this.hostRef.nativeElement as HTMLElement;

    native.style.backgroundColor = host.style.backgroundColor;
    native.style.resize = 'none';
    native.style.overflow = 'hidden';
    native.style.width = '100%';
    native.style.height = this.element.Height + 'px';
    native.style.top = '0';
    native.style.left = '0';
    native.style.bottom = '0';
    native.style.right = '0';
    native.style.padding = '0';
    native.style.border = 'none';
    native.style.outline = 'none';

    native.focus();
  }

  /**
   * Element Context.
   *
   * @type {Container}
   * @memberof MfoContainerComponent
   */
  @Input() element: Edit;

  /**
   * FontFamily binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontFamily') get fontFamily(): string {
    return this.element.FontFamily;
  }

  /**
   * LineHeight binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoEditComponent
   */
  @HostBinding('style.lineHeight') get lineHeight(): number {
    return this.element.LineSpacing;
  }

  /**
   * LetterSpacing binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoEditComponent
   */
  @HostBinding('style.letterSpacing.px') get letterSpacing(): number {
    return this.element.LetterSpacing - 0.5;
  }

  /**
   * FontSize binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontSize.px') get fontSize(): number {
    return this.element.FontSize;
  }

  /**
   * BorderStyle binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    return this.element.BorderWidth
      ? 'inset'
      : this.Mode === ViewMode.Edit
      ? 'dotted'
      : 'none';
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === ViewMode.Edit
      ? 'grey'
      : 'transparent';
  }

  /**
   * Color binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.color') get color(): string {
    return this.element.FontColor;
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === ViewMode.Edit
      ? 1
      : 0;
  }

  /**
   * border radius hostbinding
   *
   * @readonly
   * @type {number}
   * @memberof MfoEditComponent
   */
  @HostBinding('style.borderRadius.px') get borderRadius(): number {
    return this.Mode === ViewMode.Edit && !this.element.BorderWidth ? 4 : 0;
  }

  /**
   * BoxSizing binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   * BackgroundColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.background') get background(): string {
    return this.Mode == ViewMode.Interactive || this.Mode == ViewMode.Preview
      ? 'var(--ion-color-light)'
      : this.element.Color;
  }

  /**
   * Width binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * Position binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * Display binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * Weight binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.fontWeight') get fontWeight(): string {
    return this.element.FontStyleBold ? 'bold' : 'normal';
  }

  /**
   * Weight binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.textAlign') get textAlign(): string {
    return this.element.Align;
  }

  /**
   * Weight binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.whiteSpace') get whiteSpace(): string {
    return 'pre-wrap';
  }

  /**
   * TextDecoration binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.textDecoration') get textDecoration(): string {
    return this.element.FontStyleUnderline ? 'underline' : 'none';
  }

  /**
   * fontStyle binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoEditComponent
   */
  @HostBinding('style.fontStyle') get fontStyle(): string {
    return this.element.FontStyleItalic ? 'italic' : '';
  }

  /**
   * Creates an instance of MfoEditComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {MfoPrint} mfoPrint
   * @param {HistoryService} history
   * @memberof MfoEditComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public mfoPrint: MfoPrint,
    public history: HistoryService,
    private modalCtrl: ModalController,
    private autotextService: AutotextService
  ) {
    super(hostRef, config, history);
  }

  /**
   * set properties in case they are missing (after import from old mfo)
   *
   * @memberof MfoEditComponent
   */
  ngOnInit(): void {
    this.element.Number = this.element.Number || false;
    this.element.Multiline = this.element.Multiline || true;
    this.element.Text = this.element.Text || '';
    this.element.FontStyleBold = this.element.FontStyleBold || false;
    this.element.FontStyleItalic = this.element.FontStyleItalic || false;
    this.element.FontStyleStrikeOut = this.element.FontStyleItalic || false;
    this.element.FontStyleUnderline = this.element.FontStyleItalic || false;
  }

  /**
   * get Text value from mfoPrint object
   *
   * @public
   * @returns
   * @memberof MfoEditComponent
   */
  public getText(): string {
    return objectPath.get(this.mfoPrint.mfo, this.path + '.Text');
  }

  /**
   * blur event override
   *
   * @param {MouseEvent} event
   * @returns
   * @memberof MfoEditComponent
   */
  public async blur(event: MouseEvent) {
    const modal = await this.modalCtrl.getTop();

    // autotextlist was open
    if (modal) {
      return;
    }

    super.blur(event);
  }
}
