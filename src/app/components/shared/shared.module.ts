import { Pdf417BarcodeModule } from 'pdf417-barcode';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DatabindingSelectItemComponent } from '@components/databinding/databinding-select-item.component';
import { DatabindingSelectTopicComponent } from '@components/databinding/databinding-select-topic.component';
import { DatabindingComponent } from '@components/databinding/databinding.component';
import { MBbutton } from '@components/menubar/controls/button/menubar-button.component';
import { MenuBarComponent } from '@components/menubar/menubar.component';
import { MfoActiveLineComponent } from '@components/mfo-active-line/mfo-active-line.component';
import { MfoActiveComponent } from '@components/mfo-active/mfo-active.component';
import { MfoBarcodeComponent } from '@components/mfo-barcode/mfo-barcode.component';
import { MfoCheckBoxComponent } from '@components/mfo-checkbox/mfo-checkbox.component';
import { MfoContainerComponent } from '@components/mfo-container/mfo-container.component';
import { MfoContextMenuInteractiveComponent } from '@components/mfo-contextmenu-interactive/mfo-contextmenu-interactive.component';
import { MfoContextMenuComponent } from '@components/mfo-contextmenu/mfo-contextmenu.component';
import { MfoEditComponent } from '@components/mfo-edit/mfo-edit.component';
import { MfoEditorComponent } from '@components/mfo-editor/mfo-editor.component';
import { MfoImageComponent } from '@components/mfo-image/mfo-image.component';
import { MfoLineComponent } from '@components/mfo-line/mfo-line.component';
import { MfoMarkfieldComponent } from '@components/mfo-markfield/mfo-markfield.component';
import { MfoShapeComponent } from '@components/mfo-shape/mfo-shape.component';
import { MfoSignfieldComponent } from '@components/mfo-signfield/mfo-signfield.component';
import { MfoTextComponent } from '@components/mfo-text/mfo-text.component';
import { MfoViewerComponent } from '@components/mfo-viewer/mfo-viewer.component';
import { SbComboboxPopover } from '@components/sidebar/combobox/sb-combobox-popover.component';
import { SbCombobox } from '@components/sidebar/combobox/sb-combobox.component';
import { SbComboboxPopoverSettings } from '@components/sidebar/combobox-settings/sb-combobox-popover-settings.component';
import { SbComboboxSettings } from '@components/sidebar/combobox-settings/sb-combobox-settings.component';
import { SbCheckbox } from '@components/sidebar/controls/checkbox/sb-checkbox.component';
import { SbColorPicker } from '@components/sidebar/controls/colorpicker/sb-color-picker.component';
import { SbItem } from '@components/sidebar/controls/item/sb-item.component';
import { SbNumberInput } from '@components/sidebar/controls/number-input/sb-number-input.component';
import { SbTextInput } from '@components/sidebar/controls/text-input/sb-text-input.component';
import { SbText } from '@components/sidebar/controls/text/sb-text.component';
import { SbToggleButton } from '@components/sidebar/controls/toggle-button/sb-toggle-button.component';
import { InputModalComponent } from '@components/sidebar/input-modal/input-modal.component';
import { SidebarComponent } from '@components/sidebar/sidebar.component';
import { TabComponent } from '@components/tabbar/controls/tab.component';
import { TabbarComponent } from '@components/tabbar/tabbar.component';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
import { TreeviewComponent } from '@components/treeview/treeview.component';
import { IonicModule } from '@ionic/angular';
import { SbDataBinding } from '@components/sidebar/controls/data-binding/data-binding.component';
import { DbDialogComponent } from '@components/sidebar/controls/data-binding/db-dialog.component';
import { AutotextListComponent } from '@components/autotext/autotext-list.component';
import { AutotextSelectPopoverComponent } from '@components/autotext/autotext-select-popover.component';
import { SettingsComponent } from '@components/settings/settings.component';
import { MfoPaintingComponent } from '@components/mfo-paint/mfo-paint.component';
import {SettingsPopoverComponent} from '@components/settings/settings-popover.component'

@NgModule({
  declarations: [
    MfoEditorComponent,
    MfoViewerComponent,
    MfoContainerComponent,
    MfoActiveComponent,
    MfoTextComponent,
    MfoContextMenuComponent,
    MfoContextMenuInteractiveComponent,
    TreeviewComponent,
    SidebarComponent,
    MenuBarComponent,
    ToolbarComponent,
    TabbarComponent,
    MfoShapeComponent,
    MfoImageComponent,
    MfoEditComponent,
    MfoMarkfieldComponent,
    MfoCheckBoxComponent,
    MfoLineComponent,
    MfoActiveLineComponent,
    MfoBarcodeComponent,
    DatabindingComponent,
    DatabindingSelectItemComponent,
    DatabindingSelectTopicComponent,
    MfoSignfieldComponent,
    MfoPaintingComponent,
    SbItem,
    SbNumberInput,
    SbTextInput,
    SbText,
    SbCheckbox,
    SbToggleButton,
    SbColorPicker,
    SbCombobox,
    SbComboboxPopover,
    SbComboboxSettings,
    SbComboboxPopoverSettings,
    MBbutton,
    TabComponent,
    InputModalComponent,
    SbDataBinding,
    DbDialogComponent,
    AutotextSelectPopoverComponent,
    AutotextListComponent,
    SettingsComponent,
    SettingsPopoverComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxQRCodeModule,
    Pdf417BarcodeModule,
  ],
  exports: [
    MfoEditorComponent,
    MfoViewerComponent,
    MfoContainerComponent,
    MfoActiveComponent,
    MfoTextComponent,
    MfoContextMenuComponent,
    MfoContextMenuInteractiveComponent,
    TreeviewComponent,
    SidebarComponent,
    MenuBarComponent,
    ToolbarComponent,
    TabbarComponent,
    MfoShapeComponent,
    MfoImageComponent,
    MfoEditComponent,
    MfoMarkfieldComponent,
    MfoCheckBoxComponent,
    MfoLineComponent,
    MfoActiveLineComponent,
    MfoBarcodeComponent,
    DatabindingComponent,
    DatabindingSelectItemComponent,
    DatabindingSelectTopicComponent,
    MfoSignfieldComponent,
    MfoPaintingComponent,
    SbItem,
    SbNumberInput,
    SbTextInput,
    SbText,
    SbCheckbox,
    SbToggleButton,
    SbColorPicker,
    SbCombobox,
    SbComboboxPopover,
    MBbutton,
    TabComponent,
    InputModalComponent,
    SbDataBinding,
    DbDialogComponent,
    AutotextSelectPopoverComponent,
    AutotextListComponent,
    SettingsComponent,
    SettingsPopoverComponent
  ],
})
export class SharedModule {}
