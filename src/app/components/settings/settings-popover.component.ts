import { Component } from "@angular/core";
import { NavParams, PopoverController } from "@ionic/angular";

@Component({
  selector: "app-settings-popover",
  templateUrl: "./settings-popover.component.html",
  styleUrls: ["./settings-popover.component.scss"],
})
export class SettingsPopoverComponent {
  /**
   * items to choose from
   *
   * @type {Array<any>}
   * @memberof SettingsPopoverComponent
   */
  public items: Array<any>;

  /**
   * propertyname of value to display of object
   *
   * @type {string}
   * @memberof SettingsPopoverComponent
   */
  public captionvalue: string;
  /**
   * Creates an instance of SettingsPopoverComponent.
   * @param {PopoverController} popoverCtrl
   * @param {NavParams} navparams
   * @memberof SettingsPopoverComponent
   */
  constructor(
    private popoverCtrl: PopoverController,
    private navparams: NavParams
  ) {
    this.items = this.navparams.get("items");
    this.captionvalue = this.navparams.get("captionvalue");
  }

  /**
   * item on click
   *
   * @param {*} item
   * @memberof SettingsPopoverComponent
   */
  public selectItem(item) {
    this.popoverCtrl.dismiss({ item });
  }
}
