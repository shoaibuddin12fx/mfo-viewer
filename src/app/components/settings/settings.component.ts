import { Component } from "@angular/core";
import { ModalController, NavParams, PopoverController } from "@ionic/angular";
import { SettingsPopoverComponent } from "@components/settings/settings-popover.component";
import { InputModalComponent } from "@components/sidebar/input-modal/input-modal.component";
import { FontService } from "@services/font.service";
import { Utils } from "src/app/dataformat/util";

/**
 *
 *
 * @export
 * @class SettingsComponent
 */
@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent {
  /**
   * focused property
   *
   * @type {boolean}
   * @memberof SettingsComponent
   */
  public focused: boolean = false;

  /**
   * document name property
   *
   * @type {String}
   * @memberof SettingsComponent
   */
  public documentValue: String;

  /**
   * For adding a new class in SbCombox
   *
   * @type {boolean}
   * @memberof SettingsComponent
   */
  BorderClass: boolean = false;

  /**
   * propertyname of value to display of object
   *
   * @type {object}
   * @memberof SettingsComponent
   */
  public activeElement: any;

   /**
   * Default Font
   *
   * @type {String}
   * @memberof SettingsComponent
   */
   public SettingsFontFamily: any;

  /**
   * Creates an instance of SettingsComponent.
   * @param {ModalController} modalController
   *  @param {NavParams} navparams
   * @memberof SettingsComponent
   */
  constructor(
    public modalController: ModalController,
    public popoverCtrl: PopoverController,
    public fontService: FontService,
    private navparams: NavParams
  ) {
    this.activeElement = this.navparams.get("activeElement");
    if (this.activeElement != undefined) {
      this.BorderClass = true;
    }
  }

  /**
   *
   * close dialog
   * @public
   * @memberof SettingsComponent
   */
  public close() {
    this.modalController.dismiss();
  }

  /**
   * onfocus function
   *
   * @memberof SettingsComponent
   */
  public onFocus() {
    this.focused = true;
  }

  /**
   * onblur function
   *
   * @memberof SettingsComponent
   */
  public onBlur() {
    this.focused = false;
  }

  /**
   * onClear function to clear Document name from input
   *
   * @memberof SettingsComponent
   */
  public onClear() {
    this.documentValue = "";
  }

  /**
   * openInputDialog function for edit Document name
   *
   * @memberof SettingsComponent
   */

  public async openInputDialog() {
    const modal = await this.modalController.create({
      component: InputModalComponent,
      componentProps: {
        title: "FORMULARNAME",
        value: this.documentValue,
      },
      cssClass: "inputModal",
      showBackdrop: false,
    });

    modal.onDidDismiss().then((data) => {
      if (data.data) {
        this.documentValue = data.data.newValue;
      }
    });
    
    return await modal.present();
  }
}
