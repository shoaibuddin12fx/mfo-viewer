import { MfoPrint } from '@services/mfoPrint.service';
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppConfig } from '../../app.config';
import { Barcode } from '../../dataformat/models';
import { MfoBase } from '../mfo-base/mfo-base';
import * as objectPath from 'object-path';
import { InputModalComponent } from '@components/sidebar/input-modal/input-modal.component';
import { HistoryService } from '@services/history.service';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';

/**
 * Barcode component
 *
 * @export
 * @class MfoBarcodeComponent
 * @extends {MfoBase}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-barcode',
  templateUrl: './mfo-barcode.component.html',
})
export class MfoBarcodeComponent extends MfoBase {
  /**
   * element context
   *
   * @type {Barcode}
   * @memberof MfoBarcodeComponent
   */
  @Input() element: Barcode;

  /**
   * qr template ref
   *
   * @type {TemplateRef<any>}
   * @memberof MfoBarcodeComponent
   */
  @ViewChild('QR') QR: TemplateRef<any>;
  /**
   *
   * pdf417 template ref
   * @type {TemplateRef<any>}
   * @memberof MfoBarcodeComponent
   */
  @ViewChild('PDF417') PDF417: TemplateRef<any>;

  /**
   * borderstyle binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    return this.element.BorderWidth
      ? 'solid'
      : this.Mode === ViewMode.Edit
      ? 'dotted'
      : 'none';
  }

  /**
   * bordercolor binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === ViewMode.Edit
      ? 'grey'
      : 'transparent';
  }

  /**
   * borderwidth binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === ViewMode.Edit
      ? 1
      : 0;
  }

  /**
   * boxsizing binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.boxSizing') get boxSizing(): string {
    return 'border-box';
  }

  /**
   * width binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * height binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * top position binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * left position binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * position binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * display binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * backgroundcolor binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoBarcodeComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return 'white';
  }

  /**
   * Creates an instance of MfoBarcodeComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {MfoPrint} mfoPrint
   * @param {HistoryService} history
   * @param {TranslationService} translation
   * @param {ModalController} modalController
   * @memberof MfoBarcodeComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public mfoPrint: MfoPrint,
    public history: HistoryService,
    private modalController: ModalController,
    private changeDetector: ChangeDetectorRef
  ) {
    super(hostRef, config, history);
  }

  /**
   * present input dialog for text input
   *
   * @memberof MfoBarcodeComponent
   */
  @HostListener('dblclick', ['$event'])
  public async InputDialog() {
    const modal = await this.modalController.create({
      component: InputModalComponent,
      componentProps: {
        title: 'Text',
        value: this.element.Text,
      },
      cssClass: 'inputModal',
      showBackdrop: false,
    });

    modal.onDidDismiss().then((data) => {
      if (data.data) {
        const oldVal = this.element.Text;
        this.element.Text = data.data.newValue;
        this.history.addPropertyChange(this.element, [
          {
            propertyValueOld: oldVal,
            propertyValueNew: this.element.Text,
            propertyName: 'Text',
          },
        ]);
      }
    });
    return await modal.present();
  }

  /**
   * get Text value from mfoPrint object
   *
   * @public
   * @returns
   * @memberof MfoBarcodeComponent
   */
  public getText(): string {
    return objectPath.get(this.mfoPrint.mfo, this.path + '.Text');
  }

  /**
   *
   *
   * @memberof MfoBarcodeComponent
   */
  ngAfterViewChecked(): void {
    this.changeDetector.detectChanges();
  }
}
