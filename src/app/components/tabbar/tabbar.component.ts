import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tabbar',
  templateUrl: './tabbar.component.html',
  styleUrls: ['./tabbar.component.scss'],
})
export class TabbarComponent {
  /**
   * actual ViewMode of Editor
   *
   * @type {ViewMode}
   * @memberof TabbarComponent
   */
  @Input() Mode: ViewMode = ViewMode.Edit;

  /**
   * addPage Emitter
   *
   * @memberof TabbarComponent
   */
  @Output() addPageFunction = new EventEmitter<string>();

  /**
   * remove Page emitter
   *
   * @memberof TabbarComponent
   */
  @Output() removePageFunction = new EventEmitter<string>();

  /**
   * tabs
   *
   * @type {ElementRef}
   * @memberof TabbarComponent
   */
  @ViewChild('tabs', { read: ElementRef }) tabs: ElementRef;

  /**
   * dragging tabs enabled
   *
   * @type {boolean}
   * @memberof TabbarComponent
   */
  public TabDragEnabled: boolean = false;

  /**
   * position as number [dont set number as type!]
   *
   * @type {any}
   * @memberof TabbarComponent
   */
  public TabDragPosition: any = 0;

  /**
   * emits addPage function
   *
   * @memberof TabbarComponent
   */
  public addPage() {
    this.addPageFunction.emit();
  }

  /**
   * removes active selected page
   *
   * @memberof TabbarComponent
   */
  public removePage() {
    this.removePageFunction.emit();
  }

  /**
   * try dragging tabs
   *
   * @param {*} event
   * @returns
   * @memberof TabbarComponent
   */
  public tryDragging(event) {
    if (!this.TabDragEnabled) {
      return;
    }
    event.preventDefault();

    const walk =
      event.pageX - this.tabs.nativeElement.getBoundingClientRect().x;
    this.tabs.nativeElement.scrollLeft = this.TabDragPosition - walk;
  }

  /**
   * drag start
   *
   * @param {*} event
   * @memberof TabbarComponent
   */
  public dragStart(event) {
    this.TabDragEnabled = true;
    this.TabDragPosition =
      this.tabs.nativeElement.scrollLeft +
      event.pageX -
      this.tabs.nativeElement.getBoundingClientRect().x;
  }

  /**
   * disabled tab dragging
   *
   * @memberof TabbarComponent
   */
  public dragStop() {
    this.TabDragEnabled = false;
  }

  /**
   * scrolls tabs
   *
   * @param {*} delta
   * @memberof TabbarComponent
   */
  public scrollTabs(delta) {
    this.tabs.nativeElement.scrollLeft += delta;
  }
}
