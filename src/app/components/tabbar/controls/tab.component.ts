import { Component, Input } from '@angular/core';

/**
 * tab component for displaying a page tab
 *
 * @export
 * @class TabComponent
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
})
export class TabComponent {
  /**
   * tab caption
   *
   * @type {string}
   * @memberof TabComponent
   */
  @Input() caption: string;

  /**
   * indicator for active page
   *
   * @type {boolean}
   * @memberof TabComponent
   */
  @Input() active: boolean = false;
}
