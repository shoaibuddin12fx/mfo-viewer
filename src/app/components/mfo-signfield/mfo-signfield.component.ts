import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  ViewChild,
} from '@angular/core';
import { MfoBase } from '@components/mfo-base/mfo-base';
import { HistoryService } from '@services/history.service';
import { AppConfig } from 'src/app/app.config';
import { ViewMode } from 'src/app/dataformat/enums/viewmode.enum';
import { SignField } from 'src/app/dataformat/models';

/**
 * component to let the user sign a document
 *
 * @export
 * @class MfoSignfieldComponent
 * @extends {MfoBase}
 * @implements {AfterViewInit}
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'mfo-signfield',
  templateUrl: './mfo-signfield.component.html',
  styleUrls: ['./mfo-signfield.component.scss'],
})
export class MfoSignfieldComponent extends MfoBase implements AfterViewInit {
  /**
   * element context
   *
   * @type {SignField}
   * @memberof MfoSignfieldComponent
   */
  @Input() element: SignField;

  /**
   * canvas reference
   *
   * @type {ElementRef}
   * @memberof MfoSignfieldComponent
   */
  @ViewChild('Canvas', { read: ElementRef }) canvas: ElementRef;

  /**
   *
   * canvas context
   * @private
   * @type {CanvasRenderingContext2D}
   * @memberof MfoSignfieldComponent
   */
  private canvasContext: CanvasRenderingContext2D;

  /**
   * position binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoSignfieldComponent
   */
  @HostBinding('style.position') get position(): string {
    return 'absolute';
  }

  /**
   * display binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoSignfieldComponent
   */
  @HostBinding('style.display') get display(): string {
    return 'block';
  }

  /**
   * width binding
   *
   * @readonly
   * @type {number}
   * @memberof MfoSignfieldComponent
   */
  @HostBinding('style.width.px') get width(): number {
    return this.element.Width;
  }

  /**
   * Height binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.height.px') get height(): number {
    return this.element.Height;
  }

  /**
   * Top binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.top.px') get top(): number {
    return this.element.Y;
  }

  /**
   * Left binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.left.px') get left(): number {
    return this.element.X;
  }

  /**
   * border-style binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoSignfieldComponent
   */
  @HostBinding('style.borderStyle') get borderStyle(): string {
    let borderStyle = 'none';

    if (this.Mode === 0) {
      borderStyle = 'dotted';
    }

    if (this.element.BorderWidth) {
      borderStyle = 'inset';
    }

    return borderStyle;
  }

  /**
   * BorderColor binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderColor') get borderColor(): string {
    return this.element.BorderColor
      ? this.element.BorderColor
      : this.Mode === 0
      ? 'grey'
      : 'transparent';
  }

  /**
   * BorderWidth binding.
   *
   * @readonly
   * @type {string}
   * @memberof MfoContainerComponent
   */
  @HostBinding('style.borderWidth.px') get borderWidth(): number {
    return this.element.BorderWidth
      ? this.element.BorderWidth
      : this.Mode === 0
      ? 1
      : 0;
  }

  /**
   * background-color binding
   *
   * @readonly
   * @type {string}
   * @memberof MfoSignfieldComponent
   */
  @HostBinding('style.backgroundColor') get backgroundColor(): string {
    return this.element.Color ? this.element.Color : 'white';
  }

  /**
   * Creates an instance of MfoSignfieldComponent.
   * @param {ElementRef} hostRef
   * @param {AppConfig} config
   * @param {HistoryService} history
   * @memberof MfoSignfieldComponent
   */
  constructor(
    public hostRef: ElementRef,
    public config: AppConfig,
    public history: HistoryService
  ) {
    super(hostRef, config, history);
  }

  /**
   * set hold to true
   *
   * @private
   * @memberof MfoSignfieldComponent
   */
  @HostListener('mousedown', ['$event'])
  private mouseDown(event: MouseEvent) {
    if (this.Mode === 1) {
      this.canvasContext.moveTo(event.offsetX * 2, event.offsetY * 2);
      this.canvasContext.strokeStyle = this.element.FontColor;
    }
  }

  /**
   * draw line between last 2 points
   *
   * @private
   * @memberof MfoSignfieldComponent
   */
  @HostListener('mousemove', ['$event'])
  private mouseMove(event: MouseEvent) {
    if (
      !(this.Mode == ViewMode.Interactive || this.Mode == ViewMode.Preview) &&
      event.buttons
    ) {
      this.canvasContext.lineTo(event.offsetX * 2, event.offsetY * 2);
      this.canvasContext.stroke();
    }
  }

  /**
   * resets the canvas
   *
   * @memberof MfoSignfieldComponent
   */
  public clearCanvas() {
    this.canvasContext.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
    this.canvasContext.beginPath();
  }

  /**
   * set canvas context
   *
   * @memberof MfoSignfieldComponent
   */
  ngAfterViewInit(): void {
    this.canvasContext = (
      this.canvas.nativeElement as HTMLCanvasElement
    ).getContext('2d');
  }
}
