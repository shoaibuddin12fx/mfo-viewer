import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { EditorConfig } from './dataformat/models/editorconfig.model';

@Injectable()
export class AppConfig {
  public editor = new EditorConfig();
  private _experimental = 0;

  constructor(private storage: Storage) {}

  /**
   * Save editorconfig to storage
   *
   * @memberof AppConfig
   */
  public save() {
    this.storage.set('editorconfig', this.editor);
  }

  /**
   * Load all configuration and resolve only when all are loaded!
   *
   * @return {*}
   * @memberof AppConfig
   */
  public load() {
    return new Promise(async (resolve) => {
      this._experimental = parseInt(
        (await window.localStorage.getItem('\x01\x06\x76\x33\x00\x51\x51')) ||
          '0'
      );

      Object.assign(this.editor, await this.storage.get('editorconfig'));

      resolve(true);
    });
  }

  /**
   * Do not rely on this function. This is for internal devs only.
   *
   * @param {*} bit
   * @return {*}
   * @memberof AppConfig
   */
  public has(bit) {
    return this._experimental & bit;
  }
}
